(function($) {
	$.fn.validCampo = function(cadena) {
		$(this).on({
			keypress : function(e){
				var key = e.which,
					keye = e.keyCode,
					tecla = String.fromCharCode(key).toLowerCase(),
					letras = cadena;
				if(letras.indexOf(tecla)==-1 && keye!=9&& (key==37 || keye!=37)&& (keye!=39 || key==39) && keye!=8 && (keye!=46 || key==46) || key==161){
					e.preventDefault();
				}
			}
		});
	};
	$(".text").validCampo("abcdefghijklmnñopqrstuvwxyzáÁéÉíÍóÓúÚ ");
	$(".number").validCampo("1234567890");
	$(".number-2").validCampo("1234567890.");
	$(".number-1").validCampo("1234567890-");
  	$(".text-number-").validCampo("1234567890abcdefghijklmnñopqrstuvwxyz");
  	$(".text-number-1").validCampo("1234567890abcdefghijklmnñopqrstuvwxyz- ");

})( jQuery );