$(document).ready(function(){
    
    (function($) {
        "use strict";


    // validate contactExp form
    $("input:radio[name=estrellas]").click(function () {  
        $(".lab_est").hide();
    });
    $(function() {
        $('#contactExp').validate({
            rules: {
                name_e: {
                    required: true,
                    minlength: 2
                },
                apellido_e: {
                    required: true,
                    minlength: 2
                },
                email_e: {
                    required: true,
                    email: true
                },
                message_e: {
                    required: true,
                    minlength: 10
                }
            },
            messages: {
                name_e: {
                    required: "Ingresa tu nombre",
                    minlength: "Su nombre debe tener al menos 2 caracteres"
                },
                apellido_e: {
                    required: "Ingresa tu apellido",
                    minlength: "Su apellido debe constar de al menos 2 caracteres"
                },
                email_e: {
                    required: "Ingresa un correo electrónico valido",
                    email: "Ingresa un correo electrónico valido"
                },
                message_e: {
                    required: "Ingresa el comentario.",
                    minlength: "Debes escribir mas de 10 caracteres"
                }
            },
            submitHandler: function(form) {
                var val_est = $('input:radio[name=estrellas]:checked').val();
                if(val_est==undefined || val_est==null){
                    $(".lab_est").show();
                }else{
                    $(".lab_est").hide();
                    $(".btn_e").attr("disabled",true);
                    $(".msj-loading-cont-e").show();
                    var data = $('#contactExp').serialize();
                    //console.log(data);
                    $.ajax({
                        type: 'post',
                        url: 'modelo/experiencia.php   ',
                        data: data
                    }).done(function (r) {
                        $(".btn_e").attr("disabled",false);
                        $(".msj-loading-cont-e").hide();
                        if(r==1){
                            $('#md-comentarios').modal('hide');
                            $('#contactExp')[0].reset();
                            $('#md-enviado-exp').modal('show');
                        }else
                            $('#md-error-contact').modal('show');
                    });
                }
                
                return;

                
            }
        })
    })
        
 })(jQuery)
})