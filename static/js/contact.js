$(document).ready(function(){
    
    (function($) {
        "use strict";


    // validate contactForm form
    $(function() {
        $('#contactForm').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                subject: {
                    required: true,
                    minlength: 4
                },
                email: {
                    required: true,
                    email: true
                },
                message: {
                    required: true,
                    minlength: 10
                }
            },
            messages: {
                name: {
                    required: "Ingresa tu nombre",
                    minlength: "Su nombre debe tener al menos 2 caracteres"
                },
                subject: {
                    required: "Ingresa el asunto",
                    minlength: "El asunto debe constar de al menos 4 caracteres"
                },
                email: {
                    required: "Ingresa un correo electrónico valido",
                    email: "Ingresa un correo electrónico valido"
                },
                message: {
                    required: "Ingresa un mensaje.",
                    minlength: "Debes escribir mas de 10 caracteres"
                }
            },
            submitHandler: function(form) {
                $(".btn_1").attr("disabled",true);
                $(".msj-loading-cont").show();
                var data = $('#contactForm').serialize();
                $.ajax({
                    type: 'post',
                    url: 'modelo/contacto.php   ',
                    data: data
                }).done(function (r) {
                    $(".btn_1").attr("disabled",false);
                    $(".msj-loading-cont").hide();
                    if(r==1){
                        $('#contactForm')[0].reset();
                        $('#md-enviado-contact').modal('show');
                    }else
                        $('#md-error-contact').modal('show');
                });
                return;

                
            }
        })
    })
        
 })(jQuery)
})