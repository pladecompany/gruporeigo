$(document).ready(function(){
    
    (function($) {
        "use strict";
    $("#tlf").inputmask({placeholder:"",mask:"9999-9999999"});
    $(document).on('change', '#edad', function(){
        var valor = $('#edad').val();
        if(parseFloat(valor)>=15){
            if(parseFloat(valor)>14 && parseFloat(valor)<18){
                $("#cat").val('Juvenil 15-17').trigger("change");
                $("#tip_cat").val('Juvenil 15-17');
            }else if(parseFloat(valor)>17 && parseFloat(valor)<40){
                $("#cat").val('Libre 18-39').trigger("change");
                $("#tip_cat").val('Libre 18-39');
            }else if(parseFloat(valor)>39 && parseFloat(valor)<55){
                $("#cat").val('+40').trigger("change");
                $("#tip_cat").val('+40');
            }else{
                $("#cat").val('+55').trigger("change");
                $("#tip_cat").val('+55');
            }
        }else{
            $("#cat").val('').trigger("change");
            $("#tip_cat").val('');
        }
    });
    // validate contactRun form
    $(function() {
        $('#contactRun').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
                ec: {
                    required: true
                },
                edad: {
                    required: true
                },
                gen: {
                    required: true
                },
                club: {
                    required: true,
                    minlength: 2
                },
                tlf: {
                    required: true,
                    minlength: 12
                },
                email: {
                    required: true,
                    email: true
                },
                tipo: {
                    required: true
                },
                pag: {
                    required: true
                },
                talla: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Ingresa tu nombre",
                    minlength: "Su nombre debe tener al menos 2 caracteres"
                },
                ec: {
                    required: "Selecciona el estado civil"
                },
                edad: {
                    required: "Ingresa la edad",
                    min: "Debes ingresar una edad igual o mayor a 15 años",
                    max: "Debes ingresar una edad igual o menor a 90 años"
                },
                gen: {
                    required: "Selecciona el género"
                },
                club: {
                    required: "Ingresa el club",
                    minlength: "Club debe tener al menos 2 caracteres"
                },
                tlf: {
                    required: "Ingresa el teléfono",
                    minlength: "Ingresa un numero de teléfono valido"
                },
                email: {
                    required: "Ingresa un correo electrónico valido",
                    email: "Ingresa un correo electrónico valido"
                },
                tipo: {
                    required: "Selecciona el tipo"
                },
                pag: {
                    required: "Selecciona el método de pago"
                },
                talla: {
                    required: "Ingresa la talla"
                },
                
            },
            submitHandler: function(form) {
                $(".btn_r").attr("disabled",true);
                $(".msj-loading-cont-r").show();
                var data = $('#contactRun').serialize();
                $.ajax({
                    type: 'post',
                    url: 'modelo/run.php',
                    data: data
                }).done(function (r) {
                    $(".btn_r").attr("disabled",false);
                    $(".msj-loading-cont-r").hide();
                    $('#md-inscripcion').modal('hide');
                    if(r==1){
                        $('#contactRun')[0].reset();
                        $('#md-enviado-run').modal('show');
                    }else
                        $('#md-error-contact').modal('show');
                });
                return;

                
            }
        })
    })
        
 })(jQuery)
})
