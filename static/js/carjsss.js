$(document).on('ready', function(){
  var dolar = parseFloat($("#dolar").val());
  var descuento_pag = $("#descuento_pag").val();
  
  class Session extends Map {
    set(id, value) {
      if (typeof value === 'object') value = JSON.stringify(value);
      localStorage.setItem(id, value);
    }
    get(id) {
      const value = localStorage.getItem(id);
      try {
        return JSON.parse(value);
      } catch (e) {
        return value;
      }
    }
  }

  $(document).on('click', '.bt_abrir_carrito', function(){
    $("#md-carrito").modal('show');
  });

  const session = new Session();
  var data = session.get('data');
  if(data==null){
    session.set('data', {"carrito": []});
    carrito=session.get('data').carrito;
  }else{
    carrito=session.get('data').carrito;
  }

  $(document).on('click', '.bt_agregar_carrito', function(){
    $("#md-carrito").modal('show');
    var idp = this.id;
    var obj = { 
        modulo: 'productos',
        tipo: 'buscarProducto',
        idc: idp
    }
    $(".img_cargando").show();
    $.post('modelo/ajax_php.php', obj, function(data){
        $(".img_cargando").hide();
        if(data.r == true){
          data.producto.cantidad = 1;
          agregarProducto(data, 1);
          var bq = buscarEnCarrito(data.producto.id);
          if(bq.index == '-1') //si no existe se agrega a la sesion del carrito
            agregarAlCarrito(data.producto);
        }else{
          alert("Hay un problema con el producto que intenta agregar al carrito, intente con otro");
        }
    });
  });


  function agregarProducto(data, cantidad_agregar){
      id_almacen = $("#id_almacen").val();
      data = data.producto;

      var tipo_precio = "";

      // Validamos si ya existe en la tabla
      var preci = parseFloat(data.pre_ven_inv);
      var preciof = preci * dolar;
      data.monto_iva = (preci * data.ivap) / 100;
      
      var monto_iva_bs = (preciof * data.ivap) / 100;
      var costo_con_iva = parseFloat(preciof) + parseFloat(monto_iva_bs);
      var descuento_pro = data.descuento_web;
      if(descuento_pro && descuento_pro>0){
        descuen = (parseFloat(preciof) * parseFloat(descuento_pro)) / 100;
        preciof = preciof - descuen;
      }
      preciof = preciof.toFixed(2);

      if($("#fila_" + data.id).length == 1){
          $("#can_" + data.id).attr('disp', data.can_inv);
          $("#monto_disponible_" + data.id).text(data.can_inv + " /  ");
          $("#can_" + data.id).attr('reservado', data.reservado);
          var cantidad = $("#can_" + data.id);

          $("#can_" + data.id).select();
          $("#subtotal_" + data.id).html(formato.precio(cantidad.val() * preciof)); 
          calcularTotal();
          cantidad.change();
          return false;
      }

      //console.log(data);
      var html ="<tr id='fila_"+data.id+"' style='text-align:center;'>";
      html+= "<td class='fila_numero'></td>" ;
      html+= "<td colspan='2' class='text-left'>";
      html+= "<button type='button' class='btn btn-catalogo bt_quitar' id='"+data.id+"'><i class='fa fa-trash'></i></button>&nbsp;";
      //html+= "<a href='#!' class='bt_resumen_producto' id='"+data.id+"'>#" + data.cod_inv + " - " + data.nom_inv + "</a></td>";
      html+= "#" + data.cod_inv + " - " + data.nom_inv + "</td>";
      html+= "<td colspan='1'><span  id='monto_disponible_"+data.id+"'>"+((data.compuesto==0)? data.can_inv+" / ":'N/A')+"</span>";
      html+= " <input type='text' reservado='"+data.reservado+"' disp='"+data.can_inv+"' id_p='"+data.id+"' compuesto='"+data.compuesto+"'";
      html+= " dolar='"+dolar+"' precio_p='"+data.pre_ven_inv+"' tipo_precio='"+tipo_precio+"' pre_ven_bs='"+preciof+"' class='cantidades number-2' pedido='"+data.pedido+"' iva='"+(data.monto_iva*dolar)+"' por_iva='"+data.ivap+"' id_iva='"+data.id_iva+"' id='can_"+data.id+"' style='width:50px;text-align:center;' value='" + data.cantidad + "' min='1' elemento='cantidad'></td>";
      html+= "<td id='subtotal_"+data.id+"'>" + formato.precio(preciof) + "</td>";
      html+= "<td id=''>" + data.iva + "</td>";
      html+= "<td id='total_iva_"+data.id+"' total_con_iva="+(preciof+data.monto_iva*dolar)+" style='cursor:pointer;' cod_pro='"+data.cod_inv+"' nom_pro='"+data.nom_inv+"' id_p='"+data.id+"' class=''>" +  formato.precio((preciof+(data.monto_iva*dolar)))+ "</td>";
      //html+= "<td title='' id='total_dolar_fila_"+data.id+"'></td>";
      html+= "</tr>";
      $("#contenedor_productos").append(html);
      $(".number").validCampo("1234567890");
      $("#can_" + data.id).select().change();
      contarFilas();
  }

    $(document).on('change', '.cantidades', function(){
        var cann = parseFloat($(this).val());
        var cand = parseFloat($(this).attr('disp'));
        var rese = parseFloat($(this).attr('reservado'));

        if(cann > cand && parseFloat($(this).attr('compuesto'))==0){
          if(cann > rese){
            alert("Cantidad no disponible en el inventario.");
            $(this).val('1').trigger("change");
            cann = cand;
            return false;
          }
        }else if(cann <= 0){
          cann =cand;
          alert("Cantidad no permitida.");
          $(this).val('1').trigger("change");
          return false;
        }

        var id = $(this).attr('id_p');
        var objEditar = buscarEnCarrito(id);

        if(objEditar.index>-1){
            objEditar.data.cantidad = cann;
            editarProductoCarrito(objEditar.data, objEditar.index);
        }

        var pre_ven_bs = parseFloat($(this).attr('pre_ven_bs'));
        var subtotal_sin_iva = parseFloat($(this).val()) * pre_ven_bs;

        var iva = subtotal_sin_iva * parseFloat($("#can_"+id).attr('por_iva'))/100;
        ivar = parseFloat(iva).toFixed(2);

        $("#subtotal_" + id).html(formato.precio(subtotal_sin_iva)); 
        $("#total_iva_"+id).html(formato.precio(subtotal_sin_iva+iva));

        var total_con_iva = subtotal_sin_iva + iva;
        $("#total_iva_" + id).attr('total_con_iva', formato.montoLiteral(total_con_iva));
        var sttt_ = (subtotal_sin_iva+iva)/dolar;
        //$("#total_dolar_fila_"+id).html("$ " + formato.formatear(sttt_.toFixed(2)));
        calcularTotal();
    });

      function contarFilas(){
          var i = 0;
          $(".fila_numero").each(function(){
              i++
              $(this).html(i);
          });
          calcularTotal();
      }

      function calcularTotal(){
          var total = 0;
          var total_sin_iva = 0;
          var total_iva = 0;

          $(".cantidades").each(function(){
              var id = $(this).attr('id_p');
              total = total + parseFloat($("#total_iva_"+id).attr("total_con_iva"));
              total_sin_iva = total_sin_iva + ((parseFloat($(this).attr('pre_ven_bs')))*parseFloat($(this).val()));
              var monto_iva = ((parseFloat($(this).attr('pre_ven_bs')))*parseFloat($(this).val())) * parseFloat($("#can_"+id).attr('por_iva'))/100;
              total_iva = total_iva + (parseFloat(monto_iva));
          });

          total = total.toFixed(2);

          $(".total_sin_iva").html(formato.precio(total_sin_iva));

          //var descuento = parseFloat(formato.montoLiteral($("#descuento").val()));
          var descuento = 0;
          var total_descuento = (total_sin_iva * descuento)/100;

          total_iva = total_iva - (total_iva * descuento)/100;
          total = total - (total * descuento)/100;

          $("#total_descuento").html(formato.precio(total_descuento)+ " Bs ");
          $(".total_iva").html(formato.precio(total_iva));
          $(".total_final").html(formato.precio(total) + " BS");
          $(".total_final").attr('total_final', (total));
          //$(".total_final_usd").html(formato.precio((total/dolar)) + " $");
          $("#total_metodo").val(formato.precio(total)).attr('total_metodo',total).keyup();
          calcularTotalAbonado();
      }
		formato = {
			 separador: '.', // separador para los miles
			 sepDecimal: ',', // separador para los decimales
			 formatear:function (num){
                 num +='';
                 var splitStr = num.split('.');
                 var splitLeft = splitStr[0];
                 var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
                 var regx = /(\d+)(\d{3})/;
                 while (regx.test(splitLeft)) {
                         splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
                 }
                 return this.simbol + splitLeft  +splitRight;
			 },
			 precioExacto:function(num, simbol){
			   this.simbol = simbol ||'';
			   if(num==null || !num) {
					   num = 0;
			   }
			   var num = num.toString();
			   num=num.replace(/,/g, '.');
               //num = parseFloat(num);
			   //num=Math.round(num * 100) / 100;
			   //num=(num * 100) / 100;
			   return this.formatear(num.toFixed(2));

             }, 
             precio:function(num, simbol){
			   this.simbol = simbol ||'';

			   if(num==null || !num) {
					   num = 0;
			   }

			   var num = num.toString();
			   num=num.replace(/,/g, '.');
               num = parseFloat(num);
			   num=Math.round(num * 100) / 100;
			   num=(num * 100) / 100;
			   return this.formatear(num.toFixed(2));
			 }, 
			 montoLiteral:function(num){
               if(num == "")
                 num = 0;
               if (typeof num === 'string' || num instanceof String){
                 if(num.indexOf(".")>0){
                   num = num.split(".");
                   num = num.join("");
                 }
                 if(num.indexOf(",")>0)
                  num = num.replace(",", ".");
               }
			   return parseFloat(num).toFixed(2);
			 },
             getPrecioById:  function(precios, id_precio){
                for(var i =0; i < precios.length; i++){
                  var data = precios[i];
                  if(data.id == id_precio)
                    return data.precio_dl;
                }
              }
		}
		function calcularTotalAbonado(){
          var total_abonado = 0;
          $(".total_metodo").each(function(){
              var abono = parseFloat($(this).attr('total_metodo'));
              if(isNaN(abono))
                abono = 0;
              total_abonado = total_abonado + abono;
              var t = parseFloat($(this).attr('total_metodo'));
              var total_final = parseFloat($("#total_final").attr('total_final'));
              var faltante = total_final - total_abonado;
              if(faltante>0){
                var diff = "<span id='faltante_sobrante' style='color:red;padding-left:2em;'>  &nbsp;FALTAN: " + formato.precio(faltante) +"</span>";
              }else if(faltante<0){
                var diff = "<span id='faltante_sobrante' style='color:green;padding-left:2em;'>  &nbsp;SOBRAN: " + formato.precio(faltante) +"</span>";
              }else{
                var diff = "";
              }
              $(this).parent().parent().find(".total_metodo_usd").val(formato.precio((t/dolar)));
              $("#total_abonado_final").html(formato.precio(total_abonado) + diff);
              $("#total_abonado_final").attr("total_abonado_final", formato.montoLiteral(total_abonado));
          });
          return parseFloat($("#total_abonado_final").attr('total_abonado_final'));
		}


		$("#for_cliente").submit(function(){
            var cli = $("#cliente").val();
            var naci = $("#naci").val();
            if(cli.trim().length<7){
              $("#nom_cli").html('');
              alert("Debes completar tu numero de identificación. \n(MAX 7 CARACTERES)");
              return false;
            }
            if(cli!=""){
                $(".img_cargando").show();
                $("#nom_cli").html("");
                var obj = { 
                    modulo: 'clientes',
                    tipo: 'buscarCliente',
                    idc: cli,
                    naci: naci
                }
                
                $.post('modelo/ajax_php.php', obj, function(data){
                    $(".img_cargando").hide();
                    
                    if(data.r == true){
                        $("#nom_cli").html(data.cliente.naci+ "-" + data.cliente.cod_cli+" - "+ data.cliente.nom_cli );
                        $("#nom_cli").attr("idc", data.cliente.id);
                        $("#tipo").val(data.cliente.tipo).change();
                        $("#producto").focus();
                        
                        /*if(data.pendientes.length>0){
                          var html = "";
                          html+= '<h4>Este cliente tiene ' + data.pendientes.length+ ' facturas pendientes por pagar.</h4>';
                          var facturas = data.pendientes;
                          for(var i = 0; i<facturas.length;i++){
                            var fac = facturas[i];
                            html+= "<div class='col-md-12'>FACTURA:&nbsp; ";
                            html+= "<a class='btn btn-catalogo bt_ver_factura bt_editar_opcion modal-trigger' data-toggle='modal' id='"+fac['idf']+"' href='#md-detalle'> "+fac['cod_fac']+ " &nbsp; &nbsp;<i class='fa fa-eye'></i></a>";
                          }

                          $("#contenedor_alerta").html("");
                          $("#contenedor_alerta").append(html);
                          $("#md-alerta").modal("show");
                          $("#bt_continuar_alerta").focus();
                        }*/
                    }else{
                        /*htmlnew = `<table>
                          <td><input style="width:50px;" type="text" id="nac_cli_inp" class="form-control" value="${naci}" readonly placeholder="NACIONALIDAD" autocomplete="off"></td>
                          <td><input type="text" id="ced_cli_inp" class="form-control" value="${cli}" readonly placeholder="CEDULA" autocomplete="off"></td>
                          <td><input type="text" id="nom_cli_inp" maxlength="100" class="form-control text" placeholder="NOMBRE/RAZÓN SOCIAL" autocomplete="off"></td>
                          <td><input type="text" id="tlf_cli_inp" maxlength="12" class="form-control" placeholder="TELÉFONO" autocomplete="off"></td>
                          </table>`;*/
                          htmlnew = `<div class="row">
                          <div class="mb-1 col-4 col-md-2 mt-2 "><input style="width:50px;" type="text" id="nac_cli_inp" class="form-control" value="${naci}" readonly placeholder="NACIONALIDAD" autocomplete="off"></div>
                          <div class="mb-1 col-8 col-md-10 mt-2 "><input type="text" id="ced_cli_inp" class="form-control" value="${cli}" readonly placeholder="CEDULA" autocomplete="off"></div>
                          <div class="mb-1 col-md-6 mt-2 " ><input type="text" id="nom_cli_inp" maxlength="100" class="form-control text" placeholder="NOMBRE/RAZÓN SOCIAL" autocomplete="off"></div>
                          <div class="mb-1 col-md-6 mt-2 "><input type="text" id="tlf_cli_inp" maxlength="12" class="form-control" placeholder="TELÉFONO" autocomplete="off"></div>
                          </div>`;
                          
                        $("#formulario_buscar_cliente").attr('noShowSelect', 0);
                        $("#nom_cli").html("No tenemos tus datos, completa el formulario para continuar con el pedido:"+htmlnew);
                        $("#tlf_cli_inp").inputmask({placeholder:"",mask:"9999-9999999"});
                        $(".text").validCampo("abcdefghijklmnñopqrstuvwxyzáÁéÉíÍóÓúÚ ");
                        $("#nom_cli").attr("idc", "0");
                        $("#md-resultados-clientes").modal("show");
                        $("#caja_buscar_cliente").val(cli)
                        $("#formulario_buscar_cliente").submit();
                    }
                });
            }
			return false;
		});

  $("#bt_enviar").click(function(){
      var tipo_boton = 'procesar';
      var idc = $("#nom_cli").attr('idc');

      if(idc == "" || idc == undefined){
          alert("Debes ingresar y buscar tu numero de identificación");
          $("#cliente").focus();
          return;
      }
      if(idc == "0" && $("#nom_cli_inp").val().trim().length<3){
          alert("Debes completar tu nombre / razón social");
          $("#nom_cli_inp").focus();
          return;
      }
      
      if(idc == "0" && $("#tlf_cli_inp").val().trim().length<12){
          alert("Debes completar tu teléfono");
          $("#tlf_cli_inp").focus();
          return;
      }

      if($("#met").val()==''){
          alert("Debes seleccionar a que banco realizo el pago");
          return;
      }

      if($("#ref").val().trim().length<4 && $("#met").val()!='Dólar efectivo'){
          alert("Debes completar la referencia del pago");
          $("#ref").focus();
          return;
      }

      if(!confirm("Esta seguro que desea enviar este pedido a facturación?")){
          return;
      }

      var productos = [];
      var pagos = [];

      var obj = {modulo: 'pedidos', tipo: 'nuevoPedidoCatalogo'};

      
      //var ped = $("#pedido").is(":checked");

      

      if(idc==0){
        obj.nacc = $("#nac_cli_inp").val();
        obj.cedc = $("#ced_cli_inp").val();
        obj.nomc = $("#nom_cli_inp").val();
        obj.tlfc = $("#tlf_cli_inp").val();
      }
      obj.idc = idc;


      obj.id_almacen = null;
      obj.tipo_almacen = 2; // ONLINE

      if($(".fila_numero").length == 0){
          alert("No ha agregado ningún producto");
          return;
      }

      //var descuento = parseFloat($("#descuento").val());
      var descuento = 0;
      obj.descuento = descuento;

      obj.despacho = 1;

      $(".cantidades").each(function(){
          if(parseInt($(this).attr('pedido'))==1){
              obj.pedido = true;
          }
          productos.push(
              {
              idp: parseInt($(this).attr('id_p')),
              can: parseFloat($(this).val()),
              can_des: ((obj.despacho==1)?parseFloat($(this).val()):0),
              dolar: parseFloat($(this).attr('dolar')),
              precio: parseFloat($(this).attr('precio_p')),
              precio_bs: (parseFloat($(this).attr('pre_ven_bs'))),
              ivap: parseFloat($(this).attr('por_iva')),
              id_iva: parseInt($(this).attr('id_iva')),
              tipo_precio: $(this).attr('tipo_precio')
          });
      });

      obj.productos = productos;
      obj.nota = $("#nota").val();
      obj.tipo_fac = -100;
      obj.estatus = 0;
      obj.met = $("#met").val();
      obj.ref = $("#ref").val();
      //obj.totalbs = $(".total_final").html();
      obj.totalbs = $(".total_final").attr('total_final');
      obj.tipo_entrega = $("#tipo_entrega option:selected").val();
      //obj.totaldo = $(".total_final_usd").html();
      //console.log(obj);
      $("#bt_enviar").attr("disabled",true);
      $(".img_cargando").show();
      $.post('modelo/ajax_php.php', obj, function(data){
        //console.log(data);
          $("#bt_enviar").attr("disabled",false);
          $(".img_cargando").hide();
          if(data.r == true){
            $(".msj_ped").html('<b>Codigo del pedido:</b> '+data.codfac);
            $("#md-carrito").modal('hide');
            $("#md-enviado").modal('show');
            resetearCarrito();
            $("#cliente").get(0).focus();
          }else{
            alert(data.msj);
          }
      });
  });

  $(document).on('change', '#tipo_entrega', function(){
    var v = $("#tipo_entrega option:selected").val();
    var nota = $("#nota").val();
    if(v=='delivery'){
      $("#nota").val(nota + "Dirección de envío: ").focus();
    }
  });

  $(document).on('click', '.bt_quitar', function(){
      var id = this.id;
      var bq = buscarEnCarrito(id);
      borrarDeCarrit(bq.index);
      $("#fila_" + id).remove();
      contarFilas();
  });

  function buscarEnCarrito(idp){
    var objIndex = carrito.findIndex((obj => parseInt(obj.id) == parseInt(idp)));
    return {index: objIndex, data: carrito[objIndex]};
  }

  function editarProductoCarrito(data, index){
    carrito[index] = data;
    session.set('data', {"carrito": carrito});
  }
    function agregarAlCarrito(producto){
      carrito.push(producto);
      session.set('data', {"carrito": carrito});
    }

  cargarProductosDelCarrito();
  function cargarProductosDelCarrito(){
    for(var i = 0; i < carrito.length; i++){

      var data = {
        producto: carrito[i],
      }
      agregarProducto(data, 1);
    }
  }

  function borrarDeCarrit(index){
    carrito.splice(index, 1);
    session.set('data', {"carrito": carrito});
  }

  function resetearCarrito(){
    localStorage.clear();
    resetearPedido();
  }
		function resetearPedido(){
          var ff = new Date();
          $("#editar_factura").val("");
          $("#cliente").val("");
          $("#producto").val("");
          $(".fila_numero").parent().remove();

          $("#descuento").val('0');
          $("#total_descuento").html('');
          $("#total_sin_iva").html("0.00 BS");
          $("#total_iva").html("0.00 BS");
          $("#total_final").html("0.00 BS");
          $("#total_final").attr("total_final", 0);
          //$("#total_final_usd").html("0.00 $");
          $("#nom_cli").text("");
          $("#nom_cli").attr("idc", "");
          $("#cod_fac").text("PENDIENTE");
          $("#nota").val("");
          $("#despacho").prop('checked', false);
          $("#tipo").val(1).change();
          $(".total_metodo").val(0);
          $(".total_metodo_usd").val(0);
          $("#total_vuelto").val(0);
          $("#total_vuelto_usd").val(0);
          $("#total_abonado_final").html("0");
          $("#total_abonado_final").attr("total_abonado_final", 0);
          $(".nuevo_abono").remove();
          $("#titulo_pedido_pendiente").hide();
          $("button#procesar").show();
          $("#faltante_sobrante").remove();
          $("#vuelto").val("").change();
          $("#vendedor").val("");
		}

});
