<?php
  error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
  session_start();
  $_SESSION['ide'] = 1; //id de la empresa local para obtener sus productos
  //$_SESSION['correo_send'] = 'castellanoscarlos15@gmail.com';
  $_SESSION['correo_send'] = 'serv.reigo@gmail.com';
  $_SESSION['descuento'] = 10;

  include_once('ruta.php');
  include_once("modelo/Orm.php");
  include_once("modelo/Empresa.php");
  $dominio = $orm->obtenerDominioWeb();
  $empr = new Empresa();
  $empresa = $empr->findById($_SESSION['ide']);
  $dolar = $empresa["dolar"];

  $limite_productos = 12;
?>
<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-toto-fit=no">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Tu ferretería online. Distribuidores ferreteros mayoristas y al detal. Bombillas led para casa. Compra de cemento al por mayor. Visita nuestro catálogo.">
	<meta name="author" content="REIGO">
	<meta name="keywords" content="1. ferreteria online, ferretería más cercana, articulos de ferreteria, productos de ferreteria, materiales de ferreteria, ferreteria nacional, lista de productos de ferreteria y sus precios, proveedores de ferreteria, distribuidores ferreteros mayoristas, ferreteria a domicilio, ferreteria mayorista, super ferreteria, mayorista de ferreteria, productos ferreteros, cementos precio, precio sacos de cemento, comprar cemento, cemento blanco precio, venta de cemento barato, focos led para casa, bombillas led para casa, comprar bombillas led, bombilla led 12v, bombillas baratas, precio bombillas led">

	<title>Ayudando a construir el futuro – Grupo Reigo</title>

	<link rel="icon" href="static/img/favicon.png">
	<link rel="stylesheet" href="static/css/bootstrap.min.css">
	<link rel="stylesheet" href="static/css/animate.css">
	<link rel="stylesheet" href="static/css/owl.carousel.min.css">
	<link rel="stylesheet" href="static/css/themify-icons.css">
	<link rel="stylesheet" href="static/css/flaticon.css">
	<link rel="stylesheet" href="static/css/magnific-popup.css">
	<link rel="stylesheet" href="static/css/slick.css">
	<!-- <link rel="stylesheet" href="static/css/style.css"> -->
	<link rel="stylesheet" href="static/css/style.min.css">
	<link rel="stylesheet" href="static/css/all.min.css">
	<link rel="stylesheet" href="static/css/slider.css">
	<!-- <link rel="stylesheet" href="static/css/flickity.css"> -->
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/> -->
	<script src="static/js/jquery-1.12.1.min.js"></script>
    <?php 
      include_once("analytics.php");
    ?>
</head>

<body style="overflow-x: hidden !important;">
	<header class="main_menu home_menu fixed-top ">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-12">
					<nav class="nav-bar navbar-expand-lg navbar-light">
						<a class="navbar-brand" href="index.php">
							<img src="static/img/logo-reigo-nav.jpg" class="logo-reigo" alt="logo">
						</a>
						
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="ti-menu"></span>
						</button>

						<div class="collapse navbar-collapse main-menu-item justify-content-end"
							id="navbarSupportedContent">
							<ul class="navbar-nav align-items-center">
								<li class="nav-item">
									<a class="nav-link" href="<?php echo (($_GET['op']=='inicio'||!isset($_GET['op']))?'#inicio':'index.php');?>">Inicio</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="<?php echo (($_GET['op']=='inicio'||!isset($_GET['op']))?'#nosotros':'index.php#nosotros');?>">Nosotros</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="<?php echo (($_GET['op']=='inicio'||!isset($_GET['op']))?'#contacto':'index.php#contacto');?>">Contacto</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="?op=runreigo">Run Reigo</a>
								</li>
								<!--<li class="nav-item">
									<a class="nav-link" href="">Blog</a>
								</li> -->
								<li class="nav-item">
									<a class="nav-link btn-reigo " href="<?php echo (($_GET['op']=='inicio'||!isset($_GET['op']))?'?op=productos':'?op=productos');?>">Productos</a>
								</li>
							</ul>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header>

	<?php include($ruta); ?>
	<input type="hidden" id="dolar" value="<?php echo $dolar;?>">
	<input type="hidden" id="descuento_pag" value="<?php echo $_SESSION['descuento'];?>">
	<!-- modal producto enviado correctamente -->
	<div class="modal" id="md-enviado" tabindex="-1" role="dialog" aria-labelledby="md-filtrarLabel" aria-hidden="true" style="margin-top: 5rem; z-index: 10000;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-head" style="background: #f5a603 !important;">
					<button type="button" class="close clr_black" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body text-center">
					<i class="fa fa-check-circle fa-4x"></i>
					<h4 class="clr_black">Su pedido fue enviado exitosamente, nos comunicaremos con usted por el número de teléfono facilitado.<br><span class="msj_ped"></span></h4>
					<a href="?op=productos" class="btn btn-catalogo">Aceptar</a>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="md-enviado-contact" tabindex="-1" role="dialog" aria-labelledby="md-filtrarLabel" aria-hidden="true" style="margin-top: 5rem; z-index: 10000;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-head" style="background: #f5a603 !important;">
					<button type="button" class="close clr_black" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body text-center">
					<i class="fa fa-check-circle fa-4x"></i>
					<h4 class="clr_black" style="color: #000;">Su información fue enviada exitosamente, nos comunicaremos con usted por el teléfono facilitado.</h4>
					<a href="#!" class="btn btn-catalogo close" data-dismiss="modal" aria-label="Close">Aceptar</a>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="md-enviado-run" tabindex="-1" role="dialog" aria-labelledby="md-filtrarLabel" aria-hidden="true" style="margin-top: 5rem; z-index: 10000;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-head" style="background: #f5a603 !important;">
					<button type="button" class="close clr_black" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body text-center">
					<i class="fa fa-check-circle fa-4x"></i>
					<h4 class="clr_black" style="color: #000;">Su información fue enviada exitosamente, nos comunicaremos con usted por el teléfono facilitado.<br><br><b style="color:#000;">El proceso de pago deberá ser realizado en las próximas 48 horas, de lo contrario sera eliminada su solicitud.</b></h4>
					<a href="#!" class="btn btn-catalogo close" data-dismiss="modal" aria-label="Close">Aceptar</a>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="md-enviado-exp" tabindex="-1" role="dialog" aria-labelledby="md-filtrarLabel" aria-hidden="true" style="margin-top: 5rem; z-index: 10000;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-head" style="background: #f5a603 !important;">
					<button type="button" class="close clr_black" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body text-center">
					<i class="fa fa-check-circle fa-4x"></i>
					<h4 class="clr_black" style="color: #000;">Su información fue enviada exitosamente, gracias por compartir tu experiencia.</h4>
					<a href="#!" class="btn btn-catalogo close" data-dismiss="modal" aria-label="Close">Aceptar</a>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="md-error-contact" tabindex="-1" role="dialog" aria-labelledby="md-filtrarLabel" aria-hidden="true" style="margin-top: 5rem; z-index: 10000;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-head" style="background: #f5a603 !important;">
					<button type="button" class="close clr_black" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body text-center">
					<i class="fa fa-times-circle fa-4x"></i>
					<h4 class="clr_black" style="color: #000;">Error al enviar la información, intenta más tarde.</h4>
					<a href="#!" class="btn btn-catalogo close" data-dismiss="modal" aria-label="Close">Aceptar</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal comen -->
	<div class="modal fade" id="md-comentarios" tabindex="-1" role="dialog" aria-labelledby="md-comentariosLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background-image: url(static/img/head.jpg);background-position: center;background-size: 100%;">
					<button type="button" class="close clr_white" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			
				<form class="form-contact contact_form" action="" method="post" id="contactExp" >
					<div class="modal-body">
						<div class="row m-0">
							<div class="text-center">
								<h3 class="clr_black">Comentanos tu experiencia en nuestra <b>página web</b></h3>	
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<input class="form-control text" name="name_e" id="name_e" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Escribe tu nombre'" placeholder = 'Escribe tu nombre'>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<input class="form-control" name="apellido_e" id="apellido_e" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Escribe tu apellido'" placeholder = 'Escribe tu apellido'>
								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<input class="form-control" name="email_e" id="email_e" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Escribe tu correo electrónico'" placeholder = 'Escribe tu correo electrónico'>
								</div>
							</div>

							<div class="col-12">
								<div class="form-group">
									<textarea class="form-control w-100" name="message_e" id="message_e" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Comentarios'" placeholder = 'Comentarios'></textarea>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group text-center">
									<h3 class="clr_black"><b>Califícanos</b></h3>
									<p class="clasificacion">
									    <input id="radio1" type="radio" name="estrellas" value="5"><!--
									    --><label for="radio1"><i class="fa fa-star"></i></label><!--
									    --><input id="radio2" type="radio" name="estrellas" value="4"><!--
									    --><label for="radio2"><i class="fa fa-star"></i></label><!--
									    --><input id="radio3" type="radio" name="estrellas" value="3"><!--
									    --><label for="radio3"><i class="fa fa-star"></i></label><!--
									    --><input id="radio4" type="radio" name="estrellas" value="2"><!--
									    --><label for="radio4"><i class="fa fa-star"></i></label><!--
									    --><input id="radio5" type="radio" name="estrellas" value="1"><!--
									    --><label for="radio5"><i class="fa fa-star"></i></label>
									</p>
									<label class="error lab_est" style="display:none;">Selecciona tu califícación</label>
								</div>
							</div>
							<div class="col-12">
								<label class="msj-loading-cont-e" style="display:none;"><img src="static/img/cargando.gif" style="width:40px;"> Enviando información. . .</label>
							</div>
						</div>
					
					</div>
					
					<div class="modal-footer">
						<div class="form-group mt-3 text-center">
							<button type="submit" class="button button-contactForm btn_e">Enviar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<style type="text/css">
		

	#contactExp .clasificacion {
	  text-align: center;
	}

	#contactExp .clasificacion label {
	  font-size: 25px;
	  margin: 5px;
	  cursor: pointer;
	}

	.clasificacion input[type="radio"] {
	  display: none;
	}

	.clasificacion label {
	  color: grey;
	}

	.clasificacion {
	  direction: rtl;
	  unicode-bidi: bidi-override;
	}

	.clasificacion label:hover,
	.clasificacion label:hover ~ label {
	  color: orange;
	}

	.clasificacion input[type="radio"]:checked ~ label {
	  color: orange;
	}
	</style>
	<footer class="footer-area">
		<div class="container-fluid">
			<div class="copyright_part_text text-center">
				<div class="row m-0">
					<div class="col-lg-12">
						<p class="footer-text m-0">
							Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados, desarrollado por <a href="https://pladecompany.com/" target="_blank">Plade Company C.A</a></p>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<span class="ir-arriba fa fa-arrow-alt-circle-up fa-3x"></span> 
	<script src="static/js/paginacion.js"></script>
	<script src="static/js/popper.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
	<script src="static/js/jquery.magnific-popup.js"></script>
	<script src="static/js/swiper.min.js"></script>
	<script src="static/js/isotope.pkgd.min.js"></script>
	<script src="static/js/owl.carousel.min.js"></script>
	<script src="static/js/jquery.nice-select.min.js"></script>
	<script src="static/js/slick.min.js"></script>
	<script src="static/js/jquery.counterup.min.js"></script>
	<script src="static/js/waypoints.min.js"></script>
	<script src="static/js/custom.js"></script>
	<script src="static/js/jquery.inputmask.min.js"></script>
	<script src="static/js/validarCampo.js"></script>
	<script src="static/js/jquery.validate.min.js"></script>
	<script src="static/js/contact.js"></script>
	<script src="static/js/exp.js"></script>
	<script src="static/js/runs.js"></script>
	<script src="static/js/flickity.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.ir-arriba').click(function(){
				$('body, html').animate({
					scrollTop: '0px'
				}, 300);
			});

			$(window).scroll(function(){
				if( $(this).scrollTop() > 0 ){
					$('.ir-arriba').slideDown(300);
				} else {
					$('.ir-arriba').slideUp(300);
				}
			});
		});
	</script>

	<div id="md-inscripcion" class="modal fade" role="dialog" style="z-index:10000;padding-right: 0px !important;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="title-box-d">
						<h6 class="title-d clr_black">Datos para la inscripción</h6>
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">-</span>
					</button>
				</div>

				<div class="modal-body">
					<form class="form-contact contact_form" action="" autocomplete="off" method="post" id="contactRun" >
						<div class="row m-0">
							<div class="col-6">
								<div class="form-group mb-2">
									<input class="form-control text"  name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nombre y apellido'" placeholder = 'Nombre y apellido'>
								</div>
							</div>

							<div class="col-6">
								<div class="form-group mb-2">
									<select class="form-control" name="ec" id="ec">
										<option value="" selected="">--Estado civil--</option>
										<option>Soltero</option>
										<option>Casado</option>
										<option>Divorciado</option>
										<option>Viudo</option>
									</select>
								</div>
							</div>

							<div class="col-6">
								<div class="form-group mb-2">
									<input class="form-control text" name="edad" id="edad" type="number" max="90" min="15" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Edad'" placeholder = 'Edad'>
								</div>
							</div>

							<div class="col-6">
								<div class="form-group mb-2">
									<input type="hidden" id="tip_cat" name="tip_cat">
									<select class="form-control" name="cat" id="cat" disabled>
										<option value="" selected="">--Categoría--</option>
										<option>Juvenil 15-17</option>
										<option>Libre 18-39</option>
										<option>+40</option>
										<option>+55</option>
									</select>
								</div>
							</div>

							<div class="col-6">
								<div class="form-group mb-2">
									<select class="form-control" name="gen" id="gen" >
										<option value="" selected="">--Género--</option>
										<option>Femenino</option>
										<option>Masculino</option>
									</select>
								</div>
							</div>

							<div class="col-6">
								<div class="form-group mb-2">
									<input class="form-control text" name="club" id="club" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Club al que pertenece'" placeholder = 'Club al que pertenece'>
								</div>
							</div>

							<div class="col-6">
								<div class="form-group mb-2">
									<input class="form-control text" name="ins" id="ins" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Instagram'" placeholder = 'Instagram'>
								</div>
							</div>

							<div class="col-6">
								<div class="form-group mb-2">
									<input class="form-control text" name="fac" id="fac" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Facebook'" placeholder = 'Facebook'>
								</div>
							</div>

							<div class="col-6">
								<div class="form-group mb-2">
									<input class="form-control text" name="tlf" id="tlf" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Teléfono'" placeholder = 'Teléfono'>
								</div>
							</div>

							<div class="col-6">
								<div class="form-group mb-2">
									<input class="form-control" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Escribe tu correo electrónico'" placeholder = 'Escribe tu correo electrónico'>
								</div>
							</div>

							<div class="col-6">
								<div class="form-group mb-2">
									<select class="form-control" name="tipo" id="tipo" >
										<option value="" selected="">--Tipo--</option>
										<option>Carrera 10k</option>
										<option>Caminata 6k</option>
									</select>
								</div>
							</div>

							<div class="col-6">
								<div class="form-group mb-2">
									<select class="form-control" name="pag" id="pag" >
										<option value="" selected="">--Método de pago--</option>
										<option>Dólar efectivo</option>
										<option>Dólar Zelle/Bofa</option>
									</select>
								</div>
							</div>

							<div class="col-6">
								<div class="form-group">
									<input class="form-control" name="talla" id="talla" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Talla de franela'" placeholder = 'Talla de franela'>
								</div>
							</div>
							<div class="col-12">
                              <p style='font-size:10px;color:#000;text-align:justify;text-transform: uppercase;'>HAGO CONSTAR QUE ESTOY EN ÓPTIMA CONDICIÓN PARA PARTICIPAR EN RUN REIGO 10K Y 5K, EL 29 DE NOVIEMBRE, COMPETIRE BAJO MI PROPIO RIESGO Y RESPONSABILIDAD, Y LIBERO A RUN REIGO Y AL COMITÉ ORGANIZADOR DE CUALQUIER DAÑO O ACCIDENTE QUE PUDIERA OCURRIRME ANTES, DURANTE Y DESPUES DEL EVENTO Y CEDO A LA ORGANIZACIÓN EL DERECHO DE REPRODUCIR MI NOMBRE, APELLIDO E IMAGEN EN MEDIOS IMPRESOS Y AFINES.<br><br>En la categoría que no se inscriban más de 10 participantes, se anexarán a la categoría que le antecede.<br><br>Al momento de retirar material, presentar cédula de identidad laminada.</p>
							  <br><p style='font-size:10px;text-align:justify;text-transform: uppercase;'><b style="color:#000;">El proceso de pago deberá ser realizado en las próximas 48 horas, de lo contrario sera eliminada su solicitud.</b></p>
							</div>
						</div>

						<div class="form-group mt-3 text-center">
							<button type="submit" class="button button-contactForm btn_r" style="width: 30%">REGISTRARSE</button>
							<label class="msj-loading-cont-r" style="display:none;"><img src="static/img/cargando.gif" style="width:40px;"> Guardando información. . .</label>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
