<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	
	include_once("modelo/Admin.php");
	include_once("modelo/Modulo.php");
	include_once("modelo/Categoria.php");
	include_once("modelo/Inventario.php");
	include_once("modelo/Empresa.php");
	$opcion = $_GET['op'];
	$empr = new Empresa();
	$empresa = $empr->findById($_SESSION['ide']);
	$dolar = $empresa["dolar"];

	$categoria = new Categoria(); 
	$categorias = $categoria->fetchAllActivas();
	
	$idp = $_GET['idp'];
	$inv = new Inventario();
	if($PRO = $inv->findById($idp)){

		if($PRO['img_inv']==null || $PRO['img_inv']=="")
			$PRO['img_inv'] = "static/img/product.jpg";

		if($PRO['img_1']==null || $PRO['img_1']=="")
			$PRO['img_1'] = "static/img/product.jpg";

		if($PRO['img_2']==null || $PRO['img_2']=="")
			$PRO['img_2'] = "static/img/product.jpg";

		if($PRO['img_3']==null || $PRO['img_3']=="")
			$PRO['img_3'] = "static/img/product.jpg";

	}else{
		header('Location: index.php?msj=Código no existe.');
		exit(1);
	}
?>

		<link type="text/css" rel="stylesheet" media="screen,projection" href="static/css/all.min.css">
		<link type="text/css" rel="stylesheet" media="screen,projection" href="static/css/catalogo.css">
		<link type="text/css" rel="stylesheet" media="screen,projection" href="static/css/smoothproducts.css">

	
		<div class="row m-0">
			<div class="col-md-2 mt-vh15 col-catalogo" >
				<?php include_once("vistas/include_menu_cat.php"); ?>
			</div>

			<div class="col-md-10  mt-vh15">
				<div class="row m-0 bg_black">
					<div class="col-sm-8 text-center">
						<div class="sp-loading">
							<img src="../static/img/sp-loading.gif" alt="">
							<br>Cargando imágenes
						</div>

						<div class="sp-wrap">
							<?php if($PRO['img_inv'] == "static/img/product.jpg" && $PRO['img_1'] == "static/img/product.jpg" && $PRO['img_2'] == "static/img/product.jpg" && $PRO['img_3'] == "static/img/product.jpg"){ ?>
								<a id="img_1" href="<?php echo $PRO['img_inv'];?>">
								<img id="img_1_" src="<?php echo $PRO['img_inv'];?>" onerror="this.src='static/img/product.jpg'" alt=""></a>
							<?php }else{ 
								if($PRO['img_inv']!= "static/img/product.jpg"){
								?>
									<a id="img_1" href="<?php echo $PRO['img_inv'];?>">
									<img id="img_1_" src="<?php echo $PRO['img_inv'];?>" onerror="this.src='static/img/product.jpg'" alt=""></a>
								<?php 
									} 
									if($PRO['img_1']!= "static/img/product.jpg"){
								?>
									<a id="img_2" href="<?php echo $PRO['img_1'];?>">
									<img id="img_2_" src="<?php echo $PRO['img_1'];?>" onerror="this.src='static/img/product.jpg'" alt=""></a>
								<?php 
									} 
									if($PRO['img_2']!= "static/img/product.jpg"){
								?>
									<a id="img_3" href="<?php echo $PRO['img_2'];?>">
									<img id="img_3_" src="<?php echo $PRO['img_2'];?>" onerror="this.src='static/img/product.jpg'" alt=""></a>
								<?php 
									} 
									if($PRO['img_3']!= "static/img/product.jpg"){
								?>
									<a id="img_4" href="<?php echo $PRO['img_3'];?>">
									<img id="img_4_" src="<?php echo $PRO['img_3'];?>" onerror="this.src='static/img/product.jpg'" alt=""></a>
							<?php } } ?>
						</div>
					</div>

					<div class="col-sm-4 p-0" style="background:#3e3e3e;">
						<h3 class="codigo-producto" id="nombre_producto clr_white"><?php echo $PRO['cod_inv'];?></h3>
						<h5 class="titulo-producto"><?php echo $PRO['nom_inv'];?></h5>
						<p class="desc-producto" id="descripcion_producto">
							<?php echo $PRO['des_inv'];?>
						</p>

						<div class="row">
							<div class="col s12 m12 text-center">
								<h5 class="clr_black"><b id="codigo_producto">Código: <?php echo $PRO['cod_inv'];?></b></h5><br>
							</div>

							<div class="col s12 m12 text-center">
								<?php
									$candis = 0; 
									if( $PRO['can_inv']>0)
										$candis =  $PRO['can_inv'];
								?>
								<h5 class="clr_green"><b id="cantidad_producto">(<?php echo $candis;?>) disponible.</b></h5>
							</div>
						</div>

						<div class="row m-0">
							<div class="col-md-12 text-center">
								<?php 
									/*$ivasumar = 0;
									if($PRO['ivap']){
										$ivasumar = ($PRO['pre_ven_inv'] * $PRO['ivap'])/100;
									}
									$monto_pro = ($PRO['pre_ven_inv']+$ivasumar)*$dolar;
									$monto_sin_descuento = $monto_pro;
									if($_SESSION['descuento'] && $_SESSION['descuento']>0){
										$descuen = ($monto_pro * $_SESSION['descuento']) / 100;
										$monto_pro = $monto_pro - $descuen;

									}*/

									$ivasumar = 0;
									if($PRO['ivap']){
										$ivasumar = ($PRO['pre_ven_inv'] * $PRO['ivap'])/100;
									}
									$monto_pro = ($PRO['pre_ven_inv']+$ivasumar)*$dolar;
									$monto_sin_descuento = $monto_pro;

									if($PRO['descuento_web'] && $PRO['descuento_web']>0){
										$descuen = ($monto_pro * $PRO['descuento_web']) / 100;
										$monto_pro = $monto_pro - $descuen;
									}

                                  if(isset($pro['promocion_web'])&&$pro['promocion_web']==1){
                                    $porcentaje_descuento_web = $pro['descuento_web'];
                                ?>
                                  <div class="etiqueta-oferta">
                                      <div class="animate__animated animate__heartBeat animate__slow animate__infinite">
                                          EN OFERTA		
                                      </div>
                                  </div>
                                <?php } ?>

								<p class="m-0" style="font-size: 20px;">ANTES <span class="tachado text-bold"><?php echo $orm->monto($monto_sin_descuento);?> Bs</span></p>
								<p class="precio-producto mb-0" id="precio_venta"><?php echo $orm->monto($monto_pro)." Bs ";?></p>
							</div>

							<div class="col-md-12 text-center">
								<hr><!--<a href="https://api.whatsapp.com/send?phone=5804125165587&amp;text=Hola quiero realizar una compra de: (<?php echo $PRO['cod_inv'];?>) <?php echo $PRO['nom_inv'];?>" class="btn btn-success btn-lg btn-whatsapp"><i class="fab fa-whatsapp"></i> Solicitar</a>-->
								<?php if($candis>0){ ?>
									<a href="#!" class="btn btn-catalogo bt_agregar_carrito mb-2" id="<?php echo $PRO['id'];?>" >Añadir al carrito <i class="fa fa-shopping-cart" data-toggle="tooltip" data-placement="bottom" title="Añadir al carrito"></i></a>

									<br><a href="?op=productos" class="mb-2"><b>Volver</b></a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a href="#!" class="btn-carrito btn bt_abrir_carrito" >
			<i class="fa fa-shopping-cart"></i>
		</a>
		<?php include_once('vistas/include_carrito.php'); ?>
		<script type="text/javascript" src="static/js/smoothproducts.js"></script>

		<script type="text/javascript">
			$(document).on('ready', function(){
			  $('.sp-wrap').smoothproducts();
			});
		</script>
