<div id="md-carrito" class="modal modal-producto modalfull fade pl-0 pr-0 " role="dialog" style="z-index:10000;padding-right: 0px !important;">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="title-box-d">
					<h3 class="title-d">Carrito de pedidos</h3>
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">-</span>
				</button>
			</div>

			<div class="modal-body">
				<div class="row">
				
					<div class="col-sm-12 col-md-7 ">
						<div class="table-responsive">
							<table class="table table-stripped" id="" style="margin-bottom:0px !important">
								<tr class="bg_gray text-center">
									<th style="width:50px;">#</th>
									<th colspan="2" class="text-left">PRODUCTO</th>
									<th colspan="1">DISP/ CANT</th>
									<th style="width:80px;">MONTO</th>
									<th style="width:80px;">IVA</th>
									<th style="width:80px;">SUB-TOTAL</th>
									<!--<th style="width:80px;">SUB-TOTAL $</th>-->
								</tr>
									
								<tbody id="contenedor_productos" class="contenedor_productos">
								</tbody>

								<tr style="font-size: 1.1em;" class="bg_gray cont_cli">
									<td colspan="3" class=' hidden-mv'>BASE</td>
									<td colspan="2" class="text-center clr-reigo total_sin_iva hidden-mv" id="total_sin_iva"><b>0.00 BS</b></td>
									<td colspan="3" class="text-left clr_green hidden-mv" id=""></td>
								</tr>
									
								<tr style="font-size: 1.1em;" class="bg_gray cont_cli">
									<td colspan="3" class=' hidden-mv'>I.V.A</td>
									<td colspan="2" class="text-center clr-reigo total_iva hidden-mv" id="total_iva"><b>0.00 BS</b></td>
									<td colspan="3" class="hidden-mv"></td>
								</tr>
								
								<tr style="font-size: 1.1em;" class="bg_gray">
									<!-- <td colspan="2" class="text-left" ></td> -->
									<td colspan="3" class='' style="font-size:0.9em !important;">TOTAL</td>

									<td colspan="2" class="text-center clr-reigo total_final" style="font-size:1.1em !important" id="total_final"><b>0.00 BS</b></td>
									<td colspan="2" class="text-center clr-reigo " style="font-size:1.1em !important" id="total_final_usd"><b></b></td>
									<td colspan="1"><input type="hidden" class="form-control" id="total_vuelto" value="0" readonly></td>
								</tr>
							</table>
						</div>

						

						
					</div>

					<div class=" col-sm-12 col-md-5 ">
							<div class="row pt-0 pr-4 pl-4  ">
								<form action="#!" class="row" id="for_cliente">
									<div class="col-md-12 cont_cli">
										<div class="row">
										<div class="col-12 text-center ">
										
											<h3 class="clr-reigo">Complete los datos del pago</h3>
										
										</div>
											<div class="col-4 col-md-3 mt-2 ">
												<div class="form-group mb-0">
													<select id="naci" class="form-control" style="">
														<option value="V">V</option>
														<option value="J">J</option>
														<option value="E">E</option>
														<option value="G">G</option>
													</select>
												</div>
											</div>

											<div class="col-9 col-md-6 pr-1  mt-2 " style="width:200px !important;">
												<input type="text" id="cliente" maxlength="10" class="form-control number" placeholder="INGRESA TU CÉDULA" autocomplete="off">
											</div>

											<div class="col-2 pr-1 pl-0 pt-0 text-center mt-2 " style="width:230px !important;">
												<button type="submit" class="btn btn-catalogo w-100"><i class="fa fa-search"></i></button>
											</div>
										</div>
									</div>


									<div class="col-md-12 cont_cli">
										<p id="nom_cli" class="clr_yellow"></p>
									</div>

									<div class="col-md-6 mt-4 ">
										<div class="form-group mb-0">
											<label>Tipo de entrega</label>
											<select id="tipo_entrega" class="form-control">
												<option value="" selected="">--Seleccione--</option>
												<option value="pickup">Buscar en la tienda (Pickup)</option>
												<option value="delivery">Delivery</option>
											</select>
										</div>
									</div>

									<div class="col-sm-6 mt-4 ">
										<div class="form-group mb-0">
											<label>Selecciona el banco</label>
											<select id="met" class="form-control">
												<option value="" selected="">--Seleccione--</option>
												<option>Transferencia provincial</option>
												<option>Transferencia bicentenario</option>
												<option>Pago móvil provincial</option>
												<option>Dólar efectivo</option>
												<option>Dólar transferencia</option>
												<option>Dólar Paypal</option>
											</select>
										</div>
									</div>

									<div class="col-sm-4 mt-4 ">
										<div class="form-group">
											<label>Referencia</label>
											<input type="text" class="form-control" placeholder="" id="ref" style="width:100%;">
										</div>
									</div>

									<div class="col-sm-12">
										<div class="form-group">
											<textarea type="text" class="form-control" placeholder="Escriba una nota" id="nota" style="width:100%;"></textarea>
										</div>
									</div>

									<table class="table table-stripped" id="" style="margin-bottom:0px !important">
								<!-- <tr class="bg_gray text-center">
									<th style="width:50px;">#</th>
									<th colspan="2" class="text-left">PRODUCTO</th>
									<th colspan="1">DISP/ CANT</th>
									<th style="width:80px;">MONTO</th>
									<th style="width:80px;">IVA</th>
									<th style="width:80px;">SUB-TOTAL</th>
									<th style="width:80px;">SUB-TOTAL $</th>
								</tr> -->
									
								<tbody id="contenedor_productos" class="contenedor_productos">
								</tbody>

								<!-- <tr style="font-size: 1.1em;" class=" bg_gray cont_cli">
									<td style="border:none" colspan="3" class='text-right hidden-mv'>BASE:</td>
									<td style="border:none" colspan="2" class="text-center clr-reigo total_sin_iva hidden-mv" id="total_sin_iva"><b>0.00 BS</b></td>
									<td style="border:none" colspan="3" class="text-left clr_green hidden-mv" id=""></td>
								</tr>
									
								<tr style="font-size: 1.1em;" class="  bg_gray cont_cli">
									<td style="border:none" colspan="3" class='text-right hidden-mv'>I.V.A:</td>
									<td  style="border:none" colspan="2" class="text-center clr-reigo total_iva hidden-mv" id="total_iva"><b>0.00 BS</b></td>
									<td  style="border:none" colspan="3" class="hidden-mv"></td>
								</tr>
								
								<tr style="font-size: 1.1em;" class=" bg_gray">
									<td style="border:none" colspan="2" class="text-left" ></td>
									<td style="border:none" colspan="1" class='text-right' style="font-size:0.9em !important;">TOTAL:</td>

									<td style="border:none" colspan="2" class="text-center clr-reigo total_final" style="font-size:1.1em !important" id="total_final"><b>0.00 BS</b></td>
									<td style="border:none" colspan="2" class="text-center clr_green " style="font-size:1.1em !important" id="total_final_usd"><b></b></td>
									<td style="border:none" colspan="1"><input type="hidden" class="form-control" id="total_vuelto" value="0" readonly></td>
								</tr> -->
							</table>

									
								</form>
							</div>
						</div>
				</div>

				<div class="row mt-5">
							<div class="col-sm-12">
								<div class="row ">
									<div class="col-md-4 col-12 mb-2">
										<div style="background: #333; border-radius: .5rem;padding: .5rem;">
											<h6 class="clr_yellow text-bold text-center text-uppercase">Montajes y Servicios Tecnicos Reigo CA</h6>
											<h6 class="clr_yellow text-bold text-center text-uppercase">Banco Provincial</h6>
											<p class="m-0">Cuenta corriente</p>
											<p class="m-0">J-29537660-0</p>
											<p class="m-0">0108-0542-2701-0015-0619</p>
											<p class="m-0">Serv.reigo@gmail.com</p>
										</div>
									</div>

									<div class="col-md-4 col-12 mb-2">
										<div style="background: #333; border-radius: .5rem;padding: .5rem;">
											<h6 class="clr_yellow text-bold text-center text-uppercase">Montajes y Servicios Tecnicos Reigo CA</h6>
											<h6 class="clr_yellow text-bold text-center text-uppercase">Banco Bicentenario</h6>
											<p class="m-0">Cuenta corriente</p>
											<p class="m-0">J-29537660-0</p>
											<p class="m-0">0175-0107-1800-0000-1495</p>
											<p class="m-0">Serv.reigo@gmail.com</p>
										</div>
									</div>

									<div class="col-md-4 col-12 mb-2">
										<div style="background: #333; border-radius: .5rem;padding: .5rem;">
											<h6 class="clr_yellow text-bold text-center text-uppercase">Pago móvil</h6>
											<p class="m-0">0108 (BANCO PROVINCIAL)</p>
											<p class="m-0">J-29537660-0</p>
											<p class="m-0">0424-5018883</p>
											<p class="m-0">Serv.reigo@gmail.com</p>
											<br>
										</div>
									</div>
								</div>
							</div>
						</div>
							
						<div class="row mt-3">
							<div class="col-sm-12 col-md-6 cont_carg">
								<div class="img_cargando" style="display:none;">
									<a href="#" clasS="">
										<img src="static/img/cargando.gif" style="width:40px;">
									</a>
								</div>
							</div>
							<div class="col-sm-12 col-md-6 cont_pedi">
								<button type="button" id="bt_enviar" class="btn btn-catalogo">Enviar pedido <i class="fa fa-rocket"></i></button>
							</div>
						</div>
			</div>
		</div>
	</div>
</div>

<script src="static/js/carjsss.js"></script>
