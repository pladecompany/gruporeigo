<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	include_once("modelo/Admin.php");
	include_once("modelo/Modulo.php");
	include_once("modelo/Categoria.php");
	include_once("modelo/Inventario.php");
	include_once("modelo/Empresa.php");
	$opcion = $_GET['op'];
	$empr = new Empresa();
	$empresa = $empr->findById($_SESSION['ide']);
	$dolar = $empresa["dolar"];
	if(isset($_GET['id_almacen']))
	  $_SESSION['id_almacen_seleccionado'] = $_GET['id_almacen'];
	else
	  $_SESSION['id_almacen_seleccionado'] = 1;


	$categoria = new Categoria(); 
	$categorias = $categoria->fetchAllActivas();
	$inv = new Inventario();
	$filtro = '';
		
	if(!isset($_GET['categoria']) && !isset($_GET['filtro'])){
		$productos = $inv->ultimosProductosVendidos($limite_productos);
		$titulo_catalogo = "ÚLTIMOS PRODUCTOS VENDIDOS";
		$li_paginacion = false;
	}else{
		$idc = '';
		$titulo_catalogo = '';
		if(isset($_GET['categoria']) && $_GET['filtro']==''){
			$idc = $_GET['categoria'];
			$cate = new Categoria();
			$CAT = $cate->findById($idc);
			$titulo_catalogo = "PRODUCTOS DE LA CATEGORIA (".$CAT['categoria'].")";
		}

		$li_paginacion = true;
		if(isset($_GET['pag'])) //validar si se recibe el numero de paginacion
			$numpag = $_GET['pag'];
		else
			$numpag = 1;

		//realizar operacion para calcular en que paginacion se debe mostrar en el sql
		$lim_sql = ($numpag-1) * $limite_productos; //se disminuye uno ya que en sql se empieza desde 0
		if(isset($_GET['filtro']) && $_GET['filtro']!=''){ //validar si se recibe el numero de paginacion
			$filtro = $_GET['filtro'];
			if($titulo_catalogo !='')
				$titulo_catalogo .= '<br>';
			$titulo_catalogo .= 'RESULTADO DE BUSQUEDA ('.$_GET['filtro'].')';
		}

		$productos = $inv->obtenerTodoslosProductosNormalesArray('', $idc, $filtro, $limite_productos, "$lim_sql");
		$productos_all = $inv->obtenerTodoslosProductosNormalesTot('', $idc, $filtro);
		
	}

	function paginacion($pag, $tot, $limite_productos) {

		 $tot = $tot;
		 $htmlpag = '';
		 if ($tot == 0)
			 echo $htmlpag;
		 else {
			 $pagtot = $tot / $limite_productos;
			 $pagtot = ceil($pagtot);

			 $pen_pag = $pag + 1;
			 if ($pag <= 2) {
				 $ne = 1;
				 $nf = 5;
			 } else if ($pen_pag == $pagtot && $pag > 3) {
				 $ne = $pag - 3;
				 $nf = $pagtot;
			 } else if ($pag == $pagtot && $pag > 4) {
				 $ne = $pag - 4;
				 $nf = $pagtot;
			 } else {
				 $ne = $pag - 2;
				 $nf = $pag + 2;
			 }
			 if ($nf > $pagtot)
				 $nf = $pagtot;
			 //saber si se encuentra en una categoria
			 $urlcat = '';
			 if(isset($_GET['categoria']))
				$urlcat = '&categoria='.$_GET['categoria'].'';
			 $urlfil = '';
			 if(isset($_GET['filtro']))
				$urlfil = '&filtro='.$_GET['filtro'].'';
			 for ($i = $ne; $i <= $nf; $i++) {
				 if ($pag == $i)
					 $htmlpag .= '<li class="active page-item"><a href="#!" class="page-link">' .$pag. '</a></li>';
				 else
					 $htmlpag .= '<li class="page-item" id="' . $i . '"><a href="?op=productos'.$urlcat.$urlfil.'&pag='.$i.'" class="page-link">' . $i . '</a></li>';
			 };
			 echo $htmlpag;
		 }

	 }
?>

		<link type="text/css" rel="stylesheet" media="screen,projection" href="static/css/all.min.css">
		<link type="text/css" rel="stylesheet" media="screen,projection" href="static/css/catalogo.css">
		<link type="text/css" rel="stylesheet" media="screen,projection" href="static/css/smoothproducts.css">

	
		<div class="row ml-0 mr-0 m-0">
			<div class="col-md-2 mt-vh15 ">
               
			   <div class="text-center ocultar-md mt-5 flex-center flex ">
			   <h2 class="clr-reigo" >Categorías</h2>
			   <div class="">
					<button type="button" onclick="categoria()" class="btn btn-reigo ml-3 " ><i class="fa fa-sliders-h"></i></button>
				</div>
			   </div>

			   <div id="categorias" class=" animate__animated animate__fadeIn ocultar-sm ">
				<?php include("vistas/include_menu_cat.php"); ?>
			   </div>

				<div class="row ml-0 mr-0 pt-1 pb-1 ocultar-sm ">
					<div class="col-12 flexCenter">
						<img src="static/img/delivery-gratis.png" width="60%" class="animate__animated animate__shakeY animate__slow animate__infinite">
					</div>

					<div class="col-12 flexCenter">
						<h3 class="text-center" style="color: #fff;"><b>10% de descuento + Delivery GRATIS</b><br> por cualquier compra</h3>
					</div>
				</div>

			</div>

			<div class="col-md-10 mt-vh15 p-0">
				<div class="col-md-12">
					<img src="static/img/banner-productos.jpg" width="100%" style="border-radius: .4rem;">
				</div>

				<form action="" class="mt-3 row ml-0 mr-0 " id="for_filtro">
					<!--<div class="col-md-12">
						<h2 class="mb-4 text-center clr_white mt-2" style="color:#fff;">Productos</h2>
					</div>-->
					<input type="hidden" name="op" value="productos">
					<?php if(isset($_GET['categoria'])) { ?>
						<input type="hidden" name="categoria" value="<?php echo $_GET['categoria']; ?>">
					<?php } ?>
					<div class="col-9 col-md-5 mt-3 offset-md-3">
						<div class="form-group">
							<input name="filtro" id="txtfil" type="search" class="form-control" placeholder="¿Que deseas buscar?" value="<?php echo $filtro;?>">
						</div>
					</div>

					<div class="col-3 col-md-2 mt-3 ">
						<div class="form-group text-center">
							<button type="submit" class="btn btn-catalogo" style="width: 100%;"><i class="fa fa-search"></i></button>
						</div>
					</div>
				</form>

				<div class="hidden-lp">
					<button type="button" class="btn btn-catalogo btn-filtrar" data-toggle="modal" data-target="#md-filtrar"><i class="fa fa-sliders-h"></i></button>
				</div>

				<div class="hidden-lp">
					<div class="modal fade" id="md-filtrar" tabindex="-1" role="dialog" aria-labelledby="md-filtrarLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-body" style="background: black !important;">
									<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<?php 
										$categorias = $categoria->fetchAllActivas();
										include("vistas/include_menu_cat.php");
									?>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row ml-0 mr-0 m-0">
					<div class="col-md-12">
						<h4 class="mb-4 text-center mt-3"><?php echo $titulo_catalogo;?></h4>
						<hr>
					</div>
					<?php
						if(count($productos)==0)
							echo '<div class="col-md-12"><h4 class="text-center">No se ha encontrado ningun resultado</h4></div>';
						for($i =0; $i < count($productos); $i++){
						  $pro = $productos[$i];

						  if($pro['img_inv']==null || $pro['img_inv'] == "")
							$pro['img_inv'] = "static/img/product.jpg";
					?>
						<div class="col-12 col-md-4 col-lg-3 mb-4">
							<div class="card card-productos rounded-top offer offer-warning">
                                <?php
                                  if(isset($pro['promocion_web'])&&$pro['promocion_web']==1){
                                    $porcentaje_descuento_web = $pro['descuento_web'];
                                ?>
								<div class="shape">
									<div class="animate__animated animate__heartBeat animate__slow animate__infinite">
										<div class="shape-text">
											OFERTA							
										</div>
									</div>
								</div>
                                <?php
                                  }else{
                                    $porcentaje_descuento_web=0;
                                  }
                                ?>
								<p class="text-center cod_producto mb-1 rounded-top bg_yellow"><?php echo $pro['cod_inv'];?></p>

								<div class="text-center">
									<a href="?op=verproducto&idp=<?php echo $pro['idv'];?>" class="bt_ver_producto">
									<img class="card-img-top img-catalogo" src="<?php echo $pro['img_inv'];?>" onerror="this.src='static/img/product.jpg'" alt="">
									</a>
								</div>

								<div class="card-body p-2">
									<h5 class="card-title mb-0 tit_producto">
										<a href="?op=verproducto&idp=<?php echo $pro['idv'];?>" class="bt_ver_producto mt-2 text-white">
										  <?php echo $pro['nom_inv'];?>
										</a>
									</h5>
									<p class="card-text mb-0"></p>
									
									<div class="row ml-0 mr-0 ">
										<div class="col-md-12 text-center">
											<?php 
												$ivasumar = 0;
												if($pro['ivap']){
													$ivasumar = ($pro['pre_ven_inv'] * $pro['ivap'])/100;
												}
												$monto_pro = ($pro['pre_ven_inv']+$ivasumar)*$dolar;
												$monto_sin_descuento = $monto_pro;

												if($pro['descuento_web'] && $pro['descuento_web']>0){
													$descuen = ($monto_pro * $pro['descuento_web']) / 100;
													$monto_pro = $monto_pro - $descuen;
												}
											?>
												<p class="m-0" style="font-size: 14px;">ANTES: <span class="tachado text-bold" > <?php echo $orm->monto($monto_sin_descuento);?> Bs</span></p>
											<b><?php echo $orm->monto($monto_pro);?> Bs</b>
										</div>
										
										<div class="col-md-12 text-center">
											<?php 
												if($pro['can_inv']>0){
											?>
											<a href="#!" class="btn btn-catalogo bt_agregar_carrito p-1"  style="margin-top:1.5em;width: 100px;"id="<?php echo $pro['id_producto'];?>" >+ <i class="fa fa-shopping-cart" data-toggle="tooltip" data-placement="bottom" title="Añadir al carrito"></i></a>
											<?php
												}else{
											?>
												<div style="color:#fff;">NO DISP.</div>
												<?php
												}
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php
						}
						if($li_paginacion){
					?>
						<div class="col-md-12 text-center m-2">
							<nav aria-label="Page navigation example ">
								<ul class="pagination" style="display: inline-flex;">
									<?php paginacion($numpag,$productos_all,$limite_productos); ?>
								</ul>
							</nav>
						</div>
					<?php
						}
					?>
				</div>
				<!--
				<div class="" style="background: #555;">
					<div class="container">
						<div class="row pt-1 pb-1">
							<div class="col-12 col-sm-6 flexCenter">
								<img src="static/img/delivery-gratis.png" width="40%" class="animate__animated animate__shakeY animate__slow animate__infinite">
							</div>

							<div class="col-12 col-sm-6 flexCenter">
								<h1 class="text-center" style="color: #fff;"><b>10% de descuento + Delivery GRATIS</b> por compras en nuestra página web</h1>
							</div>
						</div>
					</div>
				</div>-->
				
				<div class="row ml-0 mr-0 pt-1 pb-1 m-0 ocultar-md ">
					<div class="col-12 flexCenter">
						<img src="static/img/delivery-gratis.png" width="60%" class="animate__animated animate__shakeY animate__slow animate__infinite">
					</div>

					<div class="col-12 flexCenter">
						<h3 class="text-center" style="color: #fff;"><b>10% de descuento + Delivery GRATIS</b><br> por cualquier compra</h3>
					</div>
				</div>

				<section class="section_padding-banner bg-gradient " >
					<div class="container">
						<div class="row ml-0 mr-0 pt-1 pb-1">
							<div class="col-12  text-center">
								<h1 class="text-center mb-5" style="color: #fff;">Queremos saber <br> ¿Que te ha parecido nuestro servicio a través de la <b class="text-white" >página web</b>?</h1>
								<a href="#" data-toggle="modal" data-target="#md-comentarios" class="btn-no-bg rounded-5 ">Cuéntanos</a>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>

		<a href="#!" class="btn-carrito btn bt_abrir_carrito" >
			<i class="fa fa-shopping-cart"></i>
		</a>

		<?php include_once('vistas/include_carrito.php'); ?>

		<script type="text/javascript" src="static/js/smoothproducts.js"></script>
		
		<script type="text/javascript">
			$(window).load(function() {
			});
		</script>

		<script type="text/javascript">
			$(document).on('ready', function(){
			  var dolar = parseFloat('<?php echo $dolar;?>');
			  var ver_modal = 1;
			  var id_almacen = '<?php echo $_SESSION['id_almacen_seleccionado'];?>';

			  $("#for_filtro").submit(function(){
					if($("#txtfil").val().length >= 3 || $("#txtfil").val().length == 0){
						return true;
					}else{
						alert("Escriba al menos 3 carácteres para la busqueda.");
						return false;
					}

			  });

			  $(document).on('click', '.bt_ver_producto_2', function(){
				  $('.modal').modal();
				  $('[data-toggle="tooltip"]').tooltip()
				  var idc = this.id;
				  var obj = { 
					  modulo: 'productos',
					  tipo: 'buscarProductoByAlmacen',
					  idc: idc,
					  id_almacen: id_almacen
				  }

				  $.post('../ajax_php.php', obj, function(data){
					  $(".img_cargando").hide();
					  if(data.r == true){
						cargarResumenProducto(data);
					  }
				  });
			  });

			  function cargarResumenProducto(data){

				if(data.producto.img_inv == null){
				  $("#img_1").attr('src', "static/img/product.jpg");
				  $("#img_1_").attr('src', "static/img/product.jpg");
				}else{
				  $("#img_1").attr('src', data.producto.img_inv);
				  $("#img_1_").attr('src', data.producto.img_inv);
				}

				if(data.producto.img_1 == null){
				  $("#img_2").attr('src', "static/img/product.jpg");
				  $("#img_2_").attr('src', "static/img/product.jpg");
				}else{
				  $("#img_2").attr('src', data.producto.img_1);
				  $("#img_2_").attr('src', data.producto.img_1);
				}

				if(data.producto.img_2 == null){
				  $("#img_3").attr('src', "static/img/product.jpg");
				  $("#img_3_").attr('src', "static/img/product.jpg");
				}else{
				  $("#img_3").attr('src', data.producto.img_2);
				  $("#img_3_").attr('src', data.producto.img_2);
				}

				if(data.producto.img_3 == null){
				  $("#img_4").attr('src', "static/img/product.jpg");
				  $("#img_4_").attr('src', "static/img/product.jpg");
				}else{
				  $("#img_4").attr('src', data.producto.img_3);
				  $("#img_4_").attr('src', data.producto.img_3);
				}

				$(".sp-wrap").deleteSmoothProducts();
				$('.sp-wrap').smoothproducts();

				var ivap = data.producto.ivap;
				var monto_iva = (data.producto.pre_ven_inv * ivap) / 100;
				var monto_iva_bs = monto_iva * dolar;
				data.producto.pre_ven_inv = parseFloat(data.producto.pre_ven_inv);
				$("#codigo_producto").text("CODÍGO: " + data.producto.cod_inv);
				$("#nombre_producto").text(data.producto.nom_inv);
				$("#descripcion_producto").text(data.producto.des_inv);
				$("#cantidad_producto").text("Disponible: " + data.producto.can_inv);

				//$("#resumen_comprometido").text(data.producto.comprometido);
				//$("#resumen_reservado").text(data.producto.reservado);
				//$("#resumen_disponible").text(formato.precio(data.producto.disponible));
				//$("#resumen_descripcion").text(data.producto.des_inv);
				//$("#resumen_presentacion").text(data.producto.presentacion);
				//$("#resumen_precio_bs").text(formato.precio((monto_iva_bs + data.producto.pre_ven_inv*dolar)));
				var precio_venta = data.producto.pre_ven_inv + monto_iva;
				$("#precio_venta").text(formato.precio(precio_venta * dolar) + " Bs");
		
				$("#contenedor_resumen_precios").html("");
				if(data.producto.precios.length>0){
				  for(var i=0; i < data.producto.precios.length; i++){
					var fila = data.producto.precios[i];
					var precio = fila['precio_dl'];
					var html = "<tr>";
					html+= "<td>" + fila['tipo_precio'] + "</td>";
					html+= "<td>" + formato.precio(precio*dolar) + "</td>";
					html+= "<td>" + formato.precio(precio) + "</td>";
					html+= "</tr>";
					$("#contenedor_resumen_precios").append(html);
				  }
				}
				/*
				if(data.producto.almacenes.length>0){
				  for(var i=0; i < data.producto.almacenes.length; i++){
					var fila = data.producto.almacenes[i];
					var html = "<tr>";
					html+= "<td>" + fila['nombre'] + "</td>";
					html+= "<td>" + formato.precio(fila['stock_almacen']) + "</td>";
					html+= "<td>" + formato.precio(fila['disponible_almacen']) + "</td>";
					html+= "<td>" + formato.precio(fila['comprometido_almacen']) + "</td>";
					html+= "<td>" + formato.precio(fila['reservado_almacen']) + "</td>";
					html+= "</tr>";
					$("#contenedor_resumen_almacen").append(html);
				  }
				}
				*/
				ver_modal++;
			  }

		formato = {
			 separador: '.', // separador para los miles
			 sepDecimal: ',', // separador para los decimales
			 formatear:function (num){
				 num +='';
				 var splitStr = num.split('.');
				 var splitLeft = splitStr[0];
				 var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
				 var regx = /(\d+)(\d{3})/;
				 while (regx.test(splitLeft)) {
						 splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
				 }
				 return this.simbol + splitLeft  +splitRight;
			 },
			 precioExacto:function(num, simbol){
			   this.simbol = simbol ||'';
			   if(num==null || !num) {
					   num = 0;
			   }
			   var num = num.toString();
			   num=num.replace(/,/g, '.');
			   //num = parseFloat(num);
			   //num=Math.round(num * 100) / 100;
			   //num=(num * 100) / 100;
			   return this.formatear(num.toFixed(2));

			 }, 
			 precio:function(num, simbol){
			   this.simbol = simbol ||'';

			   if(num==null || !num) {
					   num = 0;
			   }

			   var num = num.toString();
			   num=num.replace(/,/g, '.');
			   num = parseFloat(num);
			   num=Math.round(num * 100) / 100;
			   num=(num * 100) / 100;
			   return this.formatear(num.toFixed(2));
			 }, 
			 montoLiteral:function(num){
			   if(num == "")
				 num = 0;
			   if (typeof num === 'string' || num instanceof String){
				 if(num.indexOf(".")>0){
				   num = num.split(".");
				   num = num.join("");
				 }
				 if(num.indexOf(",")>0)
				  num = num.replace(",", ".");
			   }
			   return parseFloat(num).toFixed(2);
			 },
			 getPrecioById:  function(precios, id_precio){
				for(var i =0; i < precios.length; i++){
				  var data = precios[i];
				  if(data.id == id_precio)
					return data.precio_dl;
				}
			  }
		}
			});
		</script>

    <script>
	function categoria(){
    document.getElementById('categorias').classList.toggle ('ocultar-sm');
    };
	</script>
