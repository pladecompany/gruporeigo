<div id="inicio">
	<div class="full-width mt-5 ">
		<div id="light-slider" class="carousel slide">
			<div id="carousel-area">
				<div id="carousel-slider" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-slider" data-slide-to="1"></li>
						<li data-target="#carousel-slider" data-slide-to="2"></li>
					</ol>

					<div class="carousel-inner" role="listbox">
						<div class="carousel-item active">
							<img src="static/img/slider-1.jpg" alt="">
							<div class="carousel-caption slider-1 flex item-center ">
								<div class="">
								    <h1 class="slide-title animated fadeInDown"><b>PROVEEDORES DE FERRETERÍA AL MAYOR Y DETAL</b></span></h1>
									<h3 class="slide-text animated fadeIn">Líderes en Distribución de Productos Eléctricos y Ferretería en General</h3>
									<h2 class="slide-text animated fadeIn font-better" style="color: #eda917;">Ayudando a construir el futuro</h2>
									
									<a href="?op=productos" class="btn button btn-slider"><i class="fa fa-shopping-cart"></i> Compra Online</a>
								</div>
							</div>
						</div>

						<div class="carousel-item">
							<img src="static/img/slider-2.jpg" alt="">
							<div class="carousel-caption flex item-center ">

							    <div class=" ">
								<h1 class="slide-title animated fadeInDown"><b>GRUPO REIGO</b></h1>
								<h3 class="slide-text animated fadeIn">Nos caracterizamos por manejar altos <br>estándares en atención al cliente</h3>
								
								<a href="#nosotros" class="btn button btn-slider btn-lg"><i class="fa fa-users"></i> Conoce más</a>
								</div>
							</div>
						</div>

						<div class="carousel-item">
							<img src="static/img/slider-3.jpg" alt="">
								<div class="carousel-caption flex item-center ">
								<div class="">
									<h1 class="slide-title animated fadeInDown"><b>COMPRAS ONLINE</b></h1>
									<h3 class="slide-text animated fadeIn">Para tu comodidad, te traemos  <br> nuestro Catálogo en Línea</h3>
									
									<a href="?op=productos" class="btn button btn-slider btn-lg"><i class="fa fa-boxes"></i> Ver productos</a>
								 </div>
							</div>
						</div>
					</div>

					<a class="carousel-control-prev" href="#carousel-slider" role="button" data-slide="prev">
						<i class="fa fa-chevron-left"></i>
					</a>

					<a class="carousel-control-next" href="#carousel-slider" role="button" data-slide="next">
						<i class="fa fa-chevron-right"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<section class="section_padding-banner" style="background: #212121;">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 text-center mt-3 ">
				<div style="background: #fff;border-radius: 10px;padding: 1rem;text-align: center;">
					<div class="flexCenter">
						<div style="background: #222;border-radius: 50%;height: 80px;width: 80px;padding: 15px;color: #fff;text-align: center;">
							<i class="fa fa-tags fa-3x"></i>
						</div>
					</div>
					<h5 style="color: #222 !important;" class="p-1"><b>10% de descuento en todos nuestros productos</b></h5>
					<a href="?op=productos" target="_blank" class="btn button btn-reigo animate__animated animate__pulse  animate__infinite infinite"><i class="fa fa-shopping-cart"></i> Compra Online</a>
				</div>
			</div>

			<div class="col-lg-4 text-center mt-3 ">
				<div style="background: #fff;border-radius: 10px;padding: 1rem;text-align: center;">
					<div class="flexCenter">
						<div style="background: #222;border-radius: 50%;height: 80px;width: 80px;padding: 15px;color: #fff;text-align: center;">
							<i class="fab fa-google-play fa-3x"></i>
						</div>
					</div>
					<h5 style="color: #222 !important;" class="p-1"><b>Descarga nuestra Aplicación móvil y mantente informado</b></h5>
					<a href="https://play.google.com/store/apps/details?id=io.plade.reigo" target="_blank" class="btn button btn-reigo animate__animated animate__pulse  animate__infinite infinite "><i class="fa fa-download"></i> Descárgala</a>
				</div>
			</div>

			<div class="col-lg-4 text-center mt-3 ">
				<div style="background: #fff;border-radius: 10px;padding: 1rem;text-align: center;">
					<div class="flexCenter">
						<div style="background: #222;border-radius: 50%;height: 80px;width: 80px;padding: 15px;color: #fff;text-align: center;">
							<i class="fab fa-whatsapp fa-3x"></i>
						</div>
					</div>
					<h5 style="color: #222 !important;" class="p-1"><b>¡Estamos activos! solicita tu presupuesto o información aquí</b></h5>
					<a href="https://api.whatsapp.com/send?phone=5804125165587&amp;text=Hola quiero realizar una compra de: " class="btn button btn-reigo animate__animated animate__pulse  animate__infinite infinite" target="_blank"><i class="fa fa-comment"></i> Escríbenos</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="about_part pt-5 pb-5 mt-0 bg-degradado">
	<div class="container">
		<div class="row m-0">
			<div class="col-sm-12 col-md-7 flex align-center ">
				<div class="about_part_text">
					<h2 class="text-center">GRUPO REIGO ¡DESCÁRGALA!</h2>
					<p style="color: #222;"><strong>¡Disfruta de los beneficios que tenemos para tí!</strong> Para tu mayor comodidad hemos creado una aplicación móvil donde puedes realizar tus compras y mantenerte ínformado de nuestras promociones y productos en oferta</p>
					<a href="https://play.google.com/store/apps/details?id=io.plade.reigo"><p class="text-center"><b>Disponible en Play Store</b></p></a>
					
					<a href="https://play.google.com/store/apps/details?id=io.plade.reigo" target="_blank">
					<div class="text-center">
						<img src="static/img/playstore.png" width="40%">
					</div>
					</a>
					
				</div>
			</div>

			<div class="col-sm-12 col-md-5 text-center">
				<img src="static/img/app_home.png" width="80%">
			</div>
		</div>
	</div>
</section>

<section id="nosotros">
	<div class="about_part experiance_part section_padding">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<div class="col-md-6 col-lg-6">
					<div class="about_part_img">
						<img src="static/img/sobre-nosotros.jpg" alt="Sobre grupo Reigo" class="w-100 border-radius">
					</div>
				</div>

				<div class="col-md-6 col-lg-6">
					<div class="about_part_text">
						<h2>Sobre Nosotros</h2>
						<p class="mb-2">En Grupo Reigo brindamos servicios de Ferretería desde hace más de 5 años en nuestra tienda principal en Guanare.</p>

						<p class="mb-2">Contamos con sucursales en Guanare y Guanarito, que conservan firmemente la esencia de servicio del Grupo Reigo.</p>

						<p>Ofrecemos diversidad de productos de alta gama a precios competitivos, una de las categorías con más demandas son los materiales eléctricos.</p>

						<div class="about_text_iner">
							<div class="about_text_counter">
								<h2>5</h2>
							</div>
							<div class="about_iner_content">
								<h3>Años <span>de Experiencia</span></h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section style="background: #333;">
	<div class="container">
		<div class="row pt-1 pb-1">
			<div class="col-12 col-sm-6 flexCenter">
				<img src="static/img/delivery-gratis.png" width="60%" class="animate__animated animate__shakeY animate__slow animate__infinite">
			</div>

			<div class="col-12 col-sm-6 flexCenter">
				<h1 class="text-center" style="color: #fff;"><b>Delivery GRATIS</b> por compras en nuestra página web ó aplicación móvil</h1>
			</div>
		</div>
	</div>
</section>


<section class="about_part section_padding">
	<div class="container">
		<div class="row align-items-center justify-content-between">
			<div class="col-md-6 col-lg-5">
				<div class="about_part_text">
					<h2>Aprovecha los beneficios que tenemos para tí</h2>
					<p>En las Ferreterías Reigo tenemos una alternativa a la compra en la tienda y además ofertas especiales.</p>
					
					<p>Pensando siempre en nuestros clientes, les brindamos la posibilidad de solicitar nuestros productos por medio de nuestro catálogo en línea. Para que disfrutes de adquirir tus productos desde la comodidad de tu casa.</p>

					<p>Adicionalmente por comprar en línea obtienes un descuento en los productos adquiridos.</p>
				</div>
			</div>

			<div class="col-md-6 col-lg-4">
				<div class="about_part_img"> 
					<video src="static/video/video.mp4" controls width="80%" style="outline: none !important;">
						<source src="static/video/video.mp4" type="audio/mp4">
					</video>
				</div>
			</div>
		</div>
	</div>
</section>


<!--
<section id="productos" style="background-position:center" class="review_part section_padding pt-0 about_part">
	<div class="container-fluid text-center ">
		 <div class="row align-items-center justify-content-around "> 
			<div class="about_part_text flex flex-center ">

			<h2 class="text-center  mb-5 w-80 clr-reigo" >Mira como ha sido la experiencia de compra online</h2>

			</div>
					
		<div  class="main-carousel  mb-5 " data-flickity='{ "cellAlign": "left", "contain": true , "wrapAround": true , "freeScroll": true, "groupCells": true }' >

			

			<div class=" cell  flex flex-center p-2 ">

				<div class=" rounded-card w-80  ">

					 <div class="relative  ">

						<div class="w-100 rounded-top text-center pb-4 bg-dark border-bottom ">
							<p class="font-lg" >Jean viña</p>
						</div>

						<div class="absolute-user flex flex-center ">
						<img src="static/img/user.png" class="w-25 border-user " alt="user">
						</div>

					 </div>

					<div class="text-justify rounded-bottom bg-reigo pt-5 pb-2 ">
					<p class="pl-3 pr-3" >Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores ipsam, aperiam unde ex quo numquam rem harum modi quaerat odit.</p>
					</div>

				</div>

			</div>

			<div class=" cell  flex flex-center p-2 ">

				<div class=" rounded-card w-80  ">

					 <div class="relative  ">

						<div class="w-100 rounded-top text-center pb-4 bg-dark border-bottom ">
							<p class="font-lg" >Jose perez</p>
						</div>

						<div class="absolute-user flex flex-center ">
						<img src="static/img/user.png" class="w-25 border-user " alt="user">
						</div>

					 </div>

					<div class="text-justify rounded-bottom bg-reigo pt-5 pb-2 ">
					<p class="pl-3 pr-3" >Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores ipsam, aperiam unde ex quo numquam rem harum modi quaerat odit.</p>
					</div>

				</div>

			</div>

			<div class=" cell  flex flex-center p-2 ">

				<div class=" rounded-card w-80  ">

					 <div class="relative  ">

						<div class="w-100 rounded-top text-center pb-4 bg-dark border-bottom ">
							<p class="font-lg" >Juan lozada</p>
						</div>

						<div class="absolute-user flex flex-center ">
						<img src="static/img/user.png" class="w-25 border-user " alt="user">
						</div>

					 </div>

					<div class="text-justify rounded-bottom bg-reigo pt-5 pb-2 ">
					<p class="pl-3 pr-3" >Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores ipsam, aperiam unde ex quo numquam rem harum modi quaerat odit.</p>
					</div>

				</div>

			</div>

			<div class=" cell  flex flex-center p-2 ">

				<div class=" rounded-card w-80  ">

					 <div class="relative  ">

						<div class="w-100 rounded-top text-center pb-4 bg-dark border-bottom ">
							<p class="font-lg" >Alberto peña</p>
						</div>

						<div class="absolute-user flex flex-center ">
						<img src="static/img/user.png" class="w-25 border-user " alt="user">
						</div>

					 </div>

					<div class="text-justify rounded-bottom bg-reigo pt-5 pb-2 ">
					<p class="pl-3 pr-3" >Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores ipsam, aperiam unde ex quo numquam rem harum modi quaerat odit.</p>
					</div>

				</div>

			</div>

			<div class=" cell  flex flex-center p-2 ">

				<div class=" rounded-card w-80  ">

					 <div class="relative  ">

						<div class="w-100 rounded-top text-center pb-4 bg-dark border-bottom ">
							<p class="font-lg" >Daniela mendoza</p>
						</div>

						<div class="absolute-user flex flex-center ">
						<img src="static/img/user.png" class="w-25 border-user " alt="user">
						</div>

					 </div>

					<div class="text-justify rounded-bottom bg-reigo pt-5 pb-2 ">
					<p class="pl-3 pr-3" >Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores ipsam, aperiam unde ex quo numquam rem harum modi quaerat odit.</p>
					</div>

				</div>

			</div>

			<div class=" cell  flex flex-center p-2 ">

				<div class=" rounded-card w-80  ">

					 <div class="relative  ">

						<div class="w-100 rounded-top text-center pb-4 bg-dark border-bottom ">
							<p class="font-lg" >Perejila linarez</p>
						</div>

						<div class="absolute-user flex flex-center ">
						<img src="static/img/user.png" class="w-25 border-user " alt="user">
						</div>

					 </div>

					<div class="text-justify rounded-bottom bg-reigo pt-5 pb-2 ">
					<p class="pl-3 pr-3" >Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores ipsam, aperiam unde ex quo numquam rem harum modi quaerat odit.</p>
					</div>

				</div>

			</div>

			<div class="col-lg-6 col-sm-12">
				<div class="review_part_cotent owl-carousel">
					<div class="single_review_part">
						<div class="tour_pack_content">
							<img src="static/img/RG-cemento.jpg" width="100%">
							<h4>Cemento</h4>
						</div>
					</div>

					<div class="single_review_part">
						<div class="tour_pack_content">
							<img src="static/img/RG-electrodo.jpg" width="100%">
							<h4>Electrodo</h4>
						</div>
					</div>
					
					<div class="single_review_part">
						<div class="tour_pack_content">
							<img src="static/img/RG-manto.jpg" width="100%">
							<h4>Manto</h4>
						</div>
					</div>
				</div>
			</div> 
			<div class="col-12 text-center">
				<a href="?op=productos" class="button">Ver más productos</a>
			</div>
		</div>
	</div>
</section>
-->

<section class=" bg-gradient section_padding-banner">
	<div class="container">
		<div class="row pt-1 pb-1  ">
			<div class="col-12  text-center">
				<h1 class="text-center mb-5" style="color: #fff;">Queremos saber <br> ¿Que te ha parecido nuestro servicio a través de la <b class="text-white" >página web</b>?</h1>
				<a href="#" data-toggle="modal" data-target="#md-comentarios" class="btn-no-bg rounded-5 aclarar ">Cuéntanos</a>
			</div>
		</div>
	</div>
</section>


<!-- <section id="contacto" class="contact-section section_padding">
	<div class="container">
	<div class="d-none d-sm-block mb-5 pb-4">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1970.1598445239401!2d-69.74111689269508!3d9.034574151153741!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e7cf5ce4a44afa9%3A0x493198cdc5a7b9f8!2sReigo%20C.A.!5e0!3m2!1ses!2sve!4v1594339005587!5m2!1ses!2sve" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
		</div>

		<div class="row m-0">
			<div class="col-12">
				<h2 class="contact-title">Contacto</h2>
			</div>
			
			<div class="col-lg-8 p-0">
				<form class="form-contact contact_form" action="" method="post" id="contactForm" >
					<div class="row m-0">
						<div class="col-12">
							<div class="form-group">
								<textarea class="form-control w-100" name="message" id="message" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Comentarios'" placeholder = 'Comentarios'></textarea>
							</div>
						</div>

					<div class="col-sm-6">
						<div class="form-group">
							<input class="form-control text" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Escribe tu nombre'" placeholder = 'Escribe tu nombre'>
						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
							<input class="form-control" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Escribe tu correo electrónico'" placeholder = 'Escribe tu correo electrónico'>
						</div>
					</div>

					<div class="col-12">
						<div class="form-group">
							<input class="form-control text" name="subject" id="subject" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Asunto'" placeholder = 'Asunto'>
						</div>
					</div>
				</div>

				<div class="form-group mt-3 text-center">
					<button type="submit" class="button button-contactForm btn_1">Enviar mensaje</button>
					<label class="msj-loading-cont" style="display:none;"><img src="static/img/cargando.gif" style="width:40px;"> Enviando información. . .</label>
				</div>
			</form>
		</div>

		<div class="col-lg-4">
			<div class="media contact-info">
				<span class="contact-info__icon"><i class="fab fa-whatsapp"></i></span>
				<div class="media-body">
					<h3><a href="https://api.whatsapp.com/send?phone=5804125165587&amp;text=Hola quiero realizar una compra de: "><b>0412-5165587</b></a></h3>
					<h5>¡Escríbenos al whatsapp!</h5>
				</div>
			</div>

			<div class="media contact-info">
				<span class="contact-info__icon"><i class="ti-map"></i></span>
				<div class="media-body">
					<h3>Inicio de la Av. Unda. Guanare, Portuguesa - Venezuela</h3>
					<h5>Dirección</p>
				</div>
			</div>

			<div class="media contact-info">
				<span class="contact-info__icon"><i class="ti-map"></i></span>
				<div class="media-body">
					<h3>Cra. 5ta con Av. Sucre. Guanare, Portuguesa - Venezuela</h3>
					<h5>Sucursal</h5>
				</div>
			</div>

			<div class="media contact-info">
				<span class="contact-info__icon"><i class="ti-map"></i></span>
				<div class="media-body">
					<h3>Guanarito, Portuguesa - Venezuela</h3>
					<h5>Sucursal</h5>
				</div>
			</div>

			<div class="media contact-info">
				<span class="contact-info__icon"><i class="ti-map"></i></span>
				<div class="media-body">
					<h3>Chabasquén, Portuguesa - Venezuela</h3>
					<h5>Sucursal</h5>
				</div>
			</div>

			<div class="media contact-info">
				<span class="contact-info__icon"><i class="ti-map"></i></span>
				<div class="media-body">
					<h3>Biscucuy, Portuguesa - Venezuela</h3>
					<h5>Sucursal (PRÓXIMAMENTE)</h5>
				</div>
			</div>
			
			<div class="media contact-info">
				<span class="contact-info__icon"><i class="ti-email"></i></span>
				<div class="media-body">
					<h3>contacto@gruporeigo.com</h3>
					<h5>¡Escríbenos!</h5>
				</div>
			</div>
		</div>
	</div>
</section> -->

<section id="contacto" class="contact-section  section_padding">
	<div class="container  p-0">
		<div class="d-none d-sm-block mb-5">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1970.1598445239401!2d-69.74111689269508!3d9.034574151153741!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e7cf5ce4a44afa9%3A0x493198cdc5a7b9f8!2sReigo%20C.A.!5e0!3m2!1ses!2sve!4v1594339005587!5m2!1ses!2sve" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" class="border-radius"></iframe>
		</div>
	</div>
		 
	<div class="container relative w-95 bg-degradado p-1 pb-5 p-md-5 border-radius">	
		<div class="absolute-redes flex mb-2 mr-5 ">
			<a href="https://www.facebook.com/gruporeigo/" target="_blank" ><p class="mb-0 h3 text-dark " ><i class="fab fa-facebook-f"></i></p> </a>
			<a href="https://instagram.com/reigo_ca?igshid=1smed8buwaxww" target="_blank" > <p class="mb-0 text-dark h3 ml-3 " ><i class="fab fa-instagram"></i></p> </a>
			<a href="https://web.telegram.org/#/im?p=@GrupoReigo"><p class="mb-0 h3 ml-3 text-dark " ><i class="fab fa-telegram"></i></p></a>
		</div>

		<div class="row bg-white m-0  rounded-card  "  >
			<div class=" col-sm-12 col-lg-6 p-0 bg-white">
				<h2 class="contact-title">Contacto</h2>
					<form class="form-contact contact_form" action="" method="post" id="contactForm" >
						<div class="row m-0">

						<div class="col-sm-12 col-lg-12">
							<div class="form-group">
								<input class="form-control text" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Escribe tu nombre'" placeholder = 'Escribe tu nombre'>
							</div>
						</div>

						<div class="col-sm-12 col-lg-12 ">
							<div class="form-group">
								<input class="form-control" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Escribe tu correo electrónico'" placeholder = 'Escribe tu correo electrónico'>
							</div>
						</div>
							<div class="col-12">
								<div class="form-group">
									<textarea class="form-control w-100" name="message" id="message" cols="20" rows="4" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Comentarios'" placeholder = 'Comentarios'></textarea>
								</div>
							</div>

						<div class="col-12">
							<div class="form-group">
								<input class="form-control text" name="subject" id="subject" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Asunto'" placeholder = 'Asunto'>
							</div>
						</div>
					</div>

					<div class="form-group mt-3 pl-4 ">
						<button type="submit" class="button btn-reigo button-contactForm btn_1">Enviar mensaje</button>
						<label class="msj-loading-cont" style="display:none;"><img src="static/img/cargando.gif" style="width:40px;"> Enviando información. . .</label>
					</div>
				</form>
			</div>

			<div class="col-lg-1"></div>

			<div class="col-sm-12 col-lg-5 mr-0 ml-0 flex flexCenter row bg-dark border-radius">
				<div class="col-lg-12">
					<div class=" mt-4 row media contact-info">
						<div class="col-lg-12 text-center">
							<span class="clr-reigo"><i class="fab fa-whatsapp h2 "></i></span>
						</div>
				
						<div class="media-body col-lg-12 text-center ">
							<h3><a href="https://api.whatsapp.com/send?phone=5804125165587&amp;text=Hola quiero realizar una compra de: " target="_blank"><b>0412-5165587</b></a></h3>
							<h5>¡Escríbenos al whatsapp!</h5>
						</div>
					</div>

					<div class=" mt-4 row media contact-info">
						<div class="col-lg-12 text-center">
							<span class="clr-reigo"><i class="ti-email h2 "></i></span>
						</div>

						<div class="media-body col-lg-12 text-center ">
							<h3>contacto@gruporeigo.com</h3>
							<h5>¡Escríbenos!</h5>
						</div>
					</div>

					<div class=" mt-4 row media contact-info">
						<div class="col-lg-12 text-center ">
						<span class="clr-reigo"><i class="ti-map h2 "></i></span>
						</div>
						<div class="media-body col-lg-12 text-center ">
							<h3>Inicio de la Av. Unda. Guanare, Portuguesa - Venezuela</h3>
							<h5>Dirección</p>
						</div>
					</div>
			
					<div class=" mt-4 row media contact-info">
						<div class="col-lg-12 text-center ">
						<span class="clr-reigo"><i class="ti-map h2 "></i></span>
						</div>
						<div class="media-body col-lg-12 text-center ">
							<h3>Cra. 5ta con Av. Sucre. Guanare, Portuguesa - Venezuela</h3>
							<h5>Sucursal</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="pt-3 pb-3" style="background: #fff;">
	<div class="container">
		<div class="row m-0">
			<div class="col-12 col-sm-6 flexCenter">
				<h1 class="text-center"><b>Próximamente Tarjeta VIP</b></h1>
			</div>

			<div class="col-12 col-sm-3 text-center mb-2">
				<img src="static/img/tarjeta-reigo.png" width="90%">
			</div>
			
			<div class="col-12 col-sm-3 text-center mb-2">
				<img src="static/img/tarjeta-reigo-trasera.png" width="90%">
			</div>
		</div>
	</div>
</section>



