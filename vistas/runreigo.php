<div class="mt-5" id="inicio">
	<div class="full-width mt-5 ">
		<div id="light-slider" class="carousel slide">
			<div id="carousel-area">
				<div id="carousel-slider" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner" role="listbox">
						<div class="carousel-item text-center active">
							<img src="static/img/runreigo.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<section class="about_part experiance_part section_padding  mb-0 ">
	<div class="container">
		<div class="row align-items-center justify-content-between">
			<div class="col-md-6 col-lg-6">
				<div class="about_part_text">
					<h2 class="clr_white">Categoría Libre <span >Masculino</span></h2>
				</div>
			</div>
			<div class="col-md-8 col-lg-6">
				<div class="about_part_img flex flex-end ">
					<img src="static/img/ganadores/2.jpg" width="65%">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="about_part section_padding mt-0 mb-0 ">
	<div class="container">
		<div class="row align-items-center justify-content-between">
			<div class="col-md-8 col-lg-6">
				<div class="about_part_img">
					<img src="static/img/ganadores/3.jpg" width="65%">
				</div>
			</div>
			<div class="col-md-4 col-lg-5">
				<div class="about_part_text">
					<h2 class="clr_white">Categoría Libre <span >Femenino</span></h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="about_part experiance_part mt-0 mb-0 section_padding">
	<div class="container">
		<div class="row align-items-center justify-content-between">
			<div class="col-md-6 col-lg-6">
				<div class="about_part_text">
					<h2 class="clr_white">Categoría Juvenil <span >Femenino / Masculino</span></h2>
				</div>
			</div>
			<div class="col-md-8 col-lg-6">
				<div class="about_part_img flex flex-end ">
					<img src="static/img/ganadores/6.jpg" width="65%">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="about_part section_padding mt-0 mb-0 ">
	<div class="container">
		<div class="row align-items-center justify-content-between">
			<div class="col-md-8 col-lg-6">
				<div class="about_part_img">
					<img src="static/img/ganadores/1.jpg" width="100%">
				</div>
			</div>
			<div class="col-md-4 col-lg-5">
				<div class="about_part_text">
					<h2 class="clr_white">Categoría +40 <span>Femenino</span></h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="about_part experiance_part section_padding mt-0 mb-0 ">
	<div class="container">
		<div class="row align-items-center justify-content-between">
			<div class="col-md-6 col-lg-6">
				<div class="about_part_text">
					<h2 class="clr_white">Categoría +40 <span>Masculino</span></h2>
				</div>
			</div>
			<div class="col-md-8 col-lg-6">
				<div class="about_part_img flex flex-end ">
					<img src="static/img/ganadores/4.jpg" width="65%">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="about_part section_padding mt-0 mb-0 ">
	<div class="container">
		<div class="row align-items-center justify-content-between">
			<div class="col-md-8 col-lg-6">
				<div class="about_part_img">
					<img src="static/img/ganadores/5.jpg" width="100%">
				</div>
			</div>
			<div class="col-md-4 col-lg-5">
				<div class="about_part_text">
					<h2 class="clr_white">Categoría +55 <span>Femenino / Masculino</span></h2>
				</div>
			</div>
		</div>
	</div>
</section>


<style type="text/css">
	.review_part .single_review_part img{
		 max-height: 500px !important;
	}
	.review_part .single_review_part{
    	padding: 50px 50px 50px;
	}
</style>

<section id="galeria" class="review_part section_padding about_part">
	<div class="container-fluid">
		<div class="row align-items-center justify-content-end">
			<div class="col-sm-12">
			<div class=" about_part_text text-center col-md-12">
                        <h2>Mejores momentos de Run reigo</h2>
					</div>
				<div id="pagina1" class="row animate__animated animate__fadeIn animate__slower	3s ">
					<?php 
						for ($i = 1; $i <= 9; $i++) {
							?>
							<div  class="col-sm-6 col-md-4  col-lg-4 col-xl-4 p-3  ">
								<div class="tour_pack_content">
									<img src="static/img/runreigo/<?php echo $i; ?>.jpg" width="100%">
								</div>
							</div>
							<?php
						}
					?>
				</div>
				<div id="pagina2" class="row ocultar animate__animated animate__fadeIn animate__slower	3s ">
				<?php 
						for ($i = 10; $i <= 18; $i++) {
							?>
							<div  class="col-sm-6 col-md-4  col-lg-4 col-xl-4 p-3  ">
								<div class="tour_pack_content">
									<img src="static/img/runreigo/<?php echo $i; ?>.jpg" width="100%">
								</div>
							</div>
							<?php
						}
					?>
				</div>
				<div  id="pagina3" class="row ocultar animate__animated animate__fadeIn  animate__slower	3s">
				<?php 
						for ($i = 19; $i <= 28; $i++) {
							?>
							<div class="col-sm-6 col-md-4  col-lg-4 col-xl-4 p-3  ">
								<div class="tour_pack_content">
									<img src="static/img/runreigo/<?php echo $i; ?>.jpg" width="100%">
								</div>
							</div>
							<?php
						}
					?>
				</div>
				<nav aria-label="..." class="col-sm-12 col-md-12 col-lg-12 col-xl-12 flex flex-center mt-4 " >
					<ul class="pagination pagination-lg rounded-5 ">
						<li onclick="pag1()" class="page-item"><a class="page-link rounded-top-left rounded-bottom-left text-white bg-gradient " href="#galeria" tabindex="-1">1</a></li>
						<li onclick="pag2()" class="page-item"><a class="page-link text-white bg-gradient " href="#galeria">2</a></li>
						<li onclick="pag3()" class="page-item"><a class="page-link rounded-top-right rounded-bottom-right text-white bg-gradient " href="#galeria">3</a></li>
					</ul>
					</nav>
			</div>
		</div>
	</div>
</section>


<section class="section_padding" style="background: #f5a603;">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<img src="static/img/logo-runreigo.png" width="15%;">
				<h1 class="text-center clr_white">GRACIAS POR FORMAR PARTE DE <span style="font-weight: bold;">RUN REIGO 2020</span></h1>
			</div>
		</div>
	</div>
</section>