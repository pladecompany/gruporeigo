<div id="inicio">
	<div class="full-width">
		<div id="light-slider" class="carousel slide">
			<div id="carousel-area">
				<div id="carousel-slider" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner" role="listbox">
						<div class="carousel-item text-center active">
							<img src="static/img/banner-runreigo.jpg" alt="">
							<div class="carousel-caption flexCenter slider-run">
								<h1><span style="color: #fff;font-weight: bold;text-align: center;">GRUPO REIGO</span> <br>te invita a participar en nuestra carrera<br></h1>
								<a href="#" id="bt_inscribir" data-toggle="modal" data-target="#md-inscripcion" class="btn button btn-runreigo animate__animated animate__tada animate__infinite"><i class="fa fa-run"></i> INSCRÍBETE AQUÍ</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<section class="about_part section_padding">
	<div class="container">
		<div class="row align-items-center justify-content-between">
			<div class="col-md-8 col-lg-6">
				<div class="about_part_img">
					<img src="static/img/ruta-reigo.jpg" width="100%">
				</div>
			</div>
			<div class="col-md-4 col-lg-5">
				<div class="about_part_text">
					<h2>Carrera 10k</h2>
					<h3 class="m-0">Contaremos con:</h3>
					<p class="m-0"><i class="fa fa-check-circle"></i> Puntos hidratación</p>
					<p class="m-0"><i class="fa fa-check-circle"></i> Seguridad (cumpliendo con las normas de bioseguridad)</p>
					<p class="m-0"><i class="fa fa-check-circle"></i> Refrigerio</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="about_part experiance_part section_padding">
	<div class="container">
		<div class="row align-items-center justify-content-between">
			<div class="col-md-6 col-lg-6">
				<div class="about_part_text">
					<h2>Caminata 6k</h2>
					<h3 class="m-0">Contaremos con:</h3>
					<p class="m-0"><i class="fa fa-check-circle"></i> Puntos hidratación</p>
					<p class="m-0"><i class="fa fa-check-circle"></i> Seguridad (cumpliendo con las normas de bioseguridad)</p>
					<p class="m-0"><i class="fa fa-check-circle"></i> Refrigerio</p>
				</div>
			</div>
			<div class="col-md-8 col-lg-6">
				<div class="about_part_img">
					<img src="static/img/ruta-reigo-caminata.jpg" width="100%">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="about_part section_padding">
	<div class="container">
		<div class="row align-items-center justify-content-between">
			<div class="col-md-8 col-lg-6">
				<div class="about_part_img">
					<img src="static/img/runreigo.jpeg" width="70%">
				</div>
			</div>
			<div class="col-md-4 col-lg-5">
				<div class="about_part_text">
					<h2>PREMIOS</h2>
					<h3>Todos los que crucen la meta, tendrán premios sorpresa</h3>
					<h3>La <b>Inscripción</b> tiene un costo de 10$, incluye:</h3>

					<p class="m-0"><i class="fa fa-check-circle"></i> Franela</p>
					<p class="m-0"><i class="fa fa-check-circle"></i> Medalla</p>
					<p class="m-0"><i class="fa fa-check-circle"></i> Pulsera</p>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="section_padding" style="background: #f5a603;">
	<div class="container">
		<div class="row">
			<div class="col-12 flexCenter">
				<h1 class="text-center clr_white">¿Qué esperas para formar parte de nuestro equipo RUN REIGO? <br>
					<a href="#" class="clr_black" data-toggle="modal" data-target="#md-inscripcion"><strong>Inscríbete YA <img src="static/img/cursor.png" width="30px" class="animate__animated animate__pulse animate__infinite"></strong></a>
				</h1>
			</div>
		</div>
	</div>
</section>
<script>
$(document).on('ready', function(){
<?php
  if(isset($_GET['inscribete'])){
?>
  $("#bt_inscribir").trigger('click');
<?php
  }
?>
});
</script>
