<?php
   include_once("../../modelo/RunSave.php");
   $ver = new Run();
   $r = $ver->findById($idrun);
   if($r!= false){
      $n = $r['nombre'];
      $ec = $r['est_civ'];
      $ed = $r['edad'];
      $tc = $r['cat'];
      $g = $r['gen'];
      $c = $r['club'];
      $i = $r['ins'];
      $f = $r['face'];
      $t = $r['tlf'];
      $e = $r['email'];
      $ti = $r['tipo'];
      $ta = $r['talla'];
      $nrasig =$r['nr_asig'];
      $p = $r['metodo'];
   }
   
   
?>
<style type="text/css">
    *{
        font-size: 12px;
        line-height: 1.3;	
    }
</style>
<page style="font-size: 12pt">
    <img src="../../static/img/head.jpg" style="width:100%;">
   	<table style="margin-top:5mm;border-collapse: collapse;text-transform:uppercase;">
   		<tr>
   			<th colspan="3" style="text-align:center;border: 1px solid #222;padding-left:3px;background:#222;font-size:14px;color:#f5a603">PLANILLA DE INSCRIPCIÓN</th>
   		</tr>
<tr>
   			<th colspan="3" style="text-align:center;border: 1px solid #222;padding-left:3px;background:#fff;font-size:14px;color:#f5a603">&nbsp;</th>
   		</tr>

   		<tr>
   			<th style="text-align:center;border: 1px solid #222;padding-left:3px;background:#222;font-size:12;color:#f5a603">Nombre y apellido</th>
   			<th style="text-align:center;border: 1px solid #222;padding-left:3px;background:#222;font-size:12;color:#f5a603">TELEFONO</th>
   			<th style="text-align:center;border: 1px solid #222;padding-left:3px;background:#222;font-size:12;color:#f5a603">Correo</th>
   		</tr>
   		<tr>
   			<td align="center" style="width:320px;vertical-align: top;border: 1px solid #222;">
   				<?php echo $n;?>
   			</td>
   			<td align="center" style="width:140px;vertical-align: top;border: 1px solid #222;">
   				<?php echo $t;?>
   			</td>
   			<td align="center" style="width:280px;vertical-align: top;border: 1px solid #222;">
   				<?php echo $e;?>
   			</td>
   		</tr>
   	</table>	
   	<table style="margin-top:5mm;border-collapse: collapse;text-transform:uppercase;">
   		<tr>
   			<th style="text-align:center;vertical-align: middle;border: 1px solid #222;padding-left:3px;background:#222;font-size:12;width:78px;color:#f5a603">Edad</th>
   			<th style="text-align:center;vertical-align: middle;border: 1px solid #222;padding-left:3px;background:#222;font-size:12;width:78px;color:#f5a603">Categoria</th>
   			<th style="text-align:center;vertical-align: middle;border: 1px solid #222;padding-left:3px;background:#222;font-size:12;width:78px;color:#f5a603">Genero</th>
   			<th style="text-align:center;vertical-align: middle;border: 1px solid #222;padding-left:3px;background:#222;font-size:12;width:78px;color:#f5a603">Estado civil</th>
   		</tr>
   		<tr>
   			<td align="center" style="width:90px;vertical-align: top;border: 1px solid #222;">
   				<?php echo $ed;?>
   			</td>
   			<td align="center" style="width:223px;vertical-align: top;border: 1px solid #222;">
   				<?php echo $tc;?>
   			</td>
   			<td align="center" style="width:140px;vertical-align: top;border: 1px solid #222;">
   				<?php echo $g;?>
   			</td>
   			<td align="center" style="width:280px;vertical-align: top;border: 1px solid #222;">
   				<?php echo $ec;?>
   			</td>
   		</tr>
   	</table>
   	<table style="margin-top:5mm;border-collapse: collapse;text-transform:uppercase;">
   		<tr>
   			<th style="text-align:center;vertical-align: middle;border: 1px solid #222;padding-left:3px;background:#222;font-size:12;width:78px;color:#f5a603">Club</th>
   			<th style="text-align:center;vertical-align: middle;border: 1px solid #222;padding-left:3px;background:#222;font-size:12;width:78px;color:#f5a603">Tipo</th>
   			<th style="text-align:center;vertical-align: middle;border: 1px solid #222;padding-left:3px;background:#222;font-size:12;width:78px;color:#f5a603">Talla</th>
   		</tr>
   		<tr>
   			<td align="center" style="width:320px;vertical-align: top;border: 1px solid #222;">
   				<?php echo $c;?>
   			</td>
   			<td align="center" style="width:140px;vertical-align: top;border: 1px solid #222;">
   				<?php echo $ti;?>
   			</td>
   			<td align="center" style="width:280px;vertical-align: top;border: 1px solid #222;">
   				<?php echo $ta;?>
   			</td>
   		</tr>
   	</table>
   	<table style="margin-top:5mm;border-collapse: collapse;text-transform:uppercase;">
   		<tr>
   			<th style="text-align:center;vertical-align: middle;border: 1px solid #222;padding-left:3px;background:#222;font-size:12;width:140px;color:#f5a603">Método de pago</th>
            <th style="text-align:center;vertical-align: middle;border: 1px solid #222;padding-left:3px;background:#222;font-size:12;width:140px;color:#f5a603">Numero asignado</th>
   			<th style="text-align:center;vertical-align: middle;border: 1px solid #222;padding-left:3px;background:#222;font-size:12;width:140px;color:#f5a603">Instagram</th>
   			<th style="text-align:center;vertical-align: middle;border: 1px solid #222;padding-left:3px;background:#222;font-size:12;width:140px;color:#f5a603">Facebook</th>
   		</tr>
   		<tr>
            <td align="center" style="width:140px;vertical-align: top;border: 1px solid #222;">
               <?php 
                  if(!$p)
                     echo '-';
                  else 
                     echo $p;
               ?>
            </td>
   			<td align="center" style="width:140px;vertical-align: top;border: 1px solid #222;">
   				<?php 
   						echo $nrasig;
   				?>
   			</td>

   			<td align="center" style="width:140px;vertical-align: top;border: 1px solid #222;">
   				<?php 
   					if($i=='')
   						echo '-';
   					else 
   						echo $i;
   				?>
   			</td>
   			<td align="center" style="width:140px;vertical-align: top;border: 1px solid #222;">
   				<?php 
   					if($f=='')
   						echo '-';
   					else 
   						echo $f;
   				?>
   			</td>

   			
   		</tr>
   	</table>
    <br><br><br>
   	<table style="margin-top:5mm;border-collapse: collapse;text-transform:uppercase;width:100%;">
	  <tr>
   			<th style="width:100%;text-align:center;vertical-align: middle;border: 1px solid #222;padding-left:3px;background:#222;font-size:12px;width:78px;color:#f5a603;">TERMINOS Y CONDICIONES</th>
	</tr>
	  <tr>
   		<th style="text-align:center;vertical-align: middle;border: 1px solid #222;padding-left:3px;background:#fff;font-size:12px;width:100%;color:#000;">HAGO CONSTAR QUE ESTOY EN ÓPTIMA CONDICIÓN PARA PARTICIPAR EN RUN REIGO 10K Y 5K, EL 29 DE NOVIEMBRE, COMPETIRE BAJO MI PROPIO RIESGO Y RESPONSABILIDAD, Y LIBERO A RUN REIGO Y AL COMITÉ ORGANIZADOR DE CUALQUIER DAÑO O ACCIDENTE QUE PUDIERA OCURRIRME ANTES, DURANTE Y DESPUES DEL EVENTO Y CEDO A LA ORGANIZACIÓN EL DERECHO DE REPRODUCIR MI NOMBRE, APELLIDO E IMAGEN EN MEDIOS IMPRESOS Y AFINES.<br><br>En la categoría que no se inscriban más de 10 participantes, se anexarán a la categoría que le antecede.<br><br>Al momento de retirar material, presentar cédula de identidad laminada.
         </th>
	</tr>
   	</table>
    <br><br><br>
      <table style="margin-top:5mm;border-collapse: collapse;text-transform:uppercase;width:100%;">
	  <tr>
          <th style="text-align:center;vertical-align: middle;padding-left:3px;background:#fff;font-size:12px;width:100%;color:#000;">
                _________________________________<br>
   				<?php echo $n;?>
          </th>
	</tr>
   	</table>
</page>
