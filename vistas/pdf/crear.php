<?php
error_reporting(1);
ob_start();
include dirname(__FILE__).'/html/ver.php';

$content= ob_get_clean();
 
try
{
  $html2pdf = new Html2Pdf('P', 'A4', 'es',true,'UTF-8');
  $html2pdf->pdf->SetDisplayMode('fullpage'); //Ver otros parámetros para SetDisplaMode
  $html2pdf->writeHTML($content); //Se escribe el contenido
  
  $archivo='../vistas/pdf/pdfs/PDF'.$idrun.'.pdf';
  $html2pdf->Output($archivo,'F'); //Nombre default del PDF
}
catch(HTML2PDF_exception $e) {
  echo $e;
  exit;
}
 
?>
