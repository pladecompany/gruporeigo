<?php
  include_once("Orm.php");
  include_once("Conexion.php");
  include_once("Almacen.php");
  include_once("Almacen_ubi.php");
  class Inventario{
    private $tabla = "inventario";
    public $data = [];
    public $dataConteo = [];
    public $dataDetalleConteo = [];
    public $dataNuevoConteo = [];
    public $orm = null;
    public $estatusAuditoria = array("0"=>array("txt"=>"POR COMENZAR", "col"=>"bg-warning"), "1"=>array("txt"=>"EN PROCESO", "col"=>"bg-info"), "2"=>array("txt"=>"CONTEO FINALIZADO", "col"=>"bg-info"), "3"=>array("txt"=>"PROCESO FINALIZADO", "col"=>"bg-success"), "-1"=>array("txt"=>"CANCELADO", "col"=>"bg-danger"));
    public $tipo_precios = array("1"=>array("txt"=>"PRECIO MAYORISTA"), "2"=>array("txt"=>"PRECIO MENOR"), "3"=>array("txt"=>"PRECIO MAYOR"));

    public function Inventario(){
      $this->orm = new Orm(new Conexion());
      $tihs->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function findByCod($cod){
      $id = strtoupper($cod);
      $ide = $_SESSION['ide'];
      $sql = "SELECT P.*, I.descripcion iva, I.porcentaje ivap, PR.presentacion FROM " . $this->tabla . " P, ivas I, presentaciones_inventario PR WHERE P.cod_inv='$cod' AND P.id_empresa=$ide AND P.id_iva=I.id AND P.id_presentacion=PR.id;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function getEstatus($index){
      return $this->estatusAuditoria[$index];
    }

    public function getTipoPrecios(){
      $sql = "SELECT * FROM tipo_precios;";
      $r = $this->orm->consultaPersonalizada($sql);
      return $r;
    }

    public function verificarAuditoriaActiva(){
      $ide = $_SESSION['ide'];
      $sql = "SELECT * FROM auditoria_inventario WHERE estatus<>-1 AND estatus<>3 AND id_empresa=$ide;";
      $r = $this->orm->consultaPersonalizada($sql);
      return $r;
    }


    public function findAuditoria($ida){
      $ide = $_SESSION['ide'];
      $sql = "SELECT I.*, A.nombre FROM auditoria_inventario I, almacenes A WHERE I.id=$ida AND I.id_empresa=$ide AND I.id_almacen=A.id;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        $f = $r->fetch_assoc();
        $f['estatus_'] = $this->getEstatus($f['estatus']);
        return $f;
      }else{
        return false;
      }
    }

    public function getDetallesAuditoria($ida){
      $sql = "SELECT SE.*, S.id_ubicacion FROM seleccionados_auditoria SE, stock_almacen S, auditoria_inventario A WHERE SE.id_auditoria=$ida AND A.id_almacen=S.id_almacen AND S.id_producto=SE.id_producto AND SE.id_auditoria=A.id ORDER BY S.id_ubicacion, SE.cod_pro;";
      $r = $this->orm->consultaPersonalizada($sql);
      return $r;
    }

    public function getProductoInConteo($idp){
      $ide = $_SESSION['ide'];
      $sql = "SELECT * FROM seleccionados_auditoria S, auditoria_inventario A WHERE A.estatus>=0 AND A.estatus<=2 AND S.id_auditoria=A.id AND S.id_producto=$idp AND A.id_empresa=$ide;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows>0){
        return true;
      }else{
        return false;
      }
    }

    public function getUltimaCantidadCategoria($idc){
      $ide = $_SESSION['ide'];
      $sql = "select (count(*)+1) n from inventario where id_categoria=$idc AND id_empresa=$ide;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==0)
        return 0;
      else
        return $r->fetch_assoc()['n'];
    }

    public function getDetallesAuditoriaConteo($ida, $conteo_id){
      $sql = "SELECT C.*, S.existencia FROM conteo_auditoria C, seleccionados_auditoria S WHERE C.id_auditoria=$ida AND S.id_auditoria=$ida AND C.id_producto=S.id_producto AND C.nom_conteo='$conteo_id' AND (C.conteo_realizado-C.existencia_anterior)<>0;";
      $r = $this->orm->consultaPersonalizada($sql);
      return $r;
    }

    public function getPromocionesPantalla(){
      $ide = $_SESSION['ide'];
      $sql = "SELECT * FROM inventario WHERE id_empresa=$ide AND slider=1;";
      $r = $this->orm->consultaPersonalizada($sql);
      return $r;
    }

    public function getConteosRealizados($ida){
      $sql = "SELECT *, sum(conteo_realizado<>existencia_anterior) diferencias, count(*) total_productos FROM conteo_auditoria WHERE id_auditoria=$ida GROUP BY nom_conteo;";
      $r = $this->orm->consultaPersonalizada($sql);
      return $r;
    }
    
    public function findById($id){
      $id = strtoupper($id);
      $ide = $_SESSION['ide'];
      $sql = "SELECT P.*, I.descripcion iva, I.porcentaje ivap, PR.presentacion FROM " . $this->tabla . " P, ivas I, presentaciones_inventario PR WHERE (P.id='$id' OR P.cod_inv='$id') AND P.id_empresa=$ide AND P.id_iva=I.id AND P.id_presentacion=PR.id;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows>=1){
        $f = $r->fetch_assoc();
        $f["monto_iva"]=$this->calcularIva($f['pre_ven_inv'], $f['ivap']);
        $f['disponible']= $this->orm->montoTxt($this->getExistenciaDisponible($f['id']));
        $f['comprometido']=$this->getCantidadComprometida($f['id']);
        $f['reservado']=$this->getCantidadReservada($f['id']);
        $f['precios'] = $this->getPreciosProducto($f['id'], $f['pre_inv']);
        $f['almacenes'] = $this->getAlmacenesStock($f['id']);
        return $f;
      }else{
        return false;
      }
    }

    public function getAlmacenesStock($id_producto){
      $sql = "SELECT * FROM stock_almacen S, almacenes A WHERE S.id_producto=$id_producto AND S.id_almacen=A.id;";
      $r = $this->orm->consultaPersonalizada($sql);
      $a = [];
      while($f = $r->fetch_assoc()){
        $id_almacen = $f['id_almacen'];
        $f['disponible_almacen']= $this->orm->montoTxt($this->getExistenciaDisponibleAlmacen($id_producto, $id_almacen));
        $f['reservado_almacen']=  $this->getCantidadReservadaAlmacen($id_producto, $id_almacen);
        $f['comprometido_almacen']=$this->getCantidadComprometidaAlmacen($id_producto, $id_almacen);
        $f['ubicacion']=$this->getUbicacionProducto($id_almacen, $id_producto);
        $a[] = $f;
      }

      return $a;
    }

    public function getPreciosProducto($idp, $precio){
      $sql = "SELECT T.*, A.porcentaje FROM asignar_precios A, tipo_precios T WHERE A.id_producto=$idp AND A.id_precio=T.id;";
      $precios = $this->orm->consultaPersonalizada($sql);
      $a =[];
      while($alma = $precios->fetch_assoc()){
        $alma['precio_dl'] = $precio + (($precio*$alma['porcentaje'])/100);
        $a[]=$alma;
      }
      return $a;
    }

    public function findByIdAlmacen($id, $id_almacen){
      $id = strtoupper($id);
      $ide = $_SESSION['ide'];
      $sql = "SELECT P.*, I.descripcion iva, I.porcentaje ivap, PR.presentacion FROM " . $this->tabla . " P, ivas I, presentaciones_inventario PR WHERE (P.id='$id' OR P.cod_inv='$id') AND P.id_empresa=$ide AND P.id_iva=I.id AND P.id_presentacion=PR.id;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows>=1){
        $f = $r->fetch_assoc();
        $f["monto_iva"]=$this->calcularIva($f['pre_ven_inv'], $f['ivap']);
        $f['disponible']=$this->getExistenciaDisponible($f['id']);
        $f['disponible_almacen']=$this->orm->montoTxt($this->getExistenciaDisponibleAlmacen($f['id'], $id_almacen));
        $f['comprometido']=$this->getCantidadComprometida($f['id']);
        $f['reservado']=$this->getCantidadReservada($f['id']);
        $f['stock_almacen']=$this->getUltimoSaldoByAlmacen($id_almacen, $f['id']);
        $f['ubicacion']=$this->getUbicacionProducto($id_almacen, $f['id']);
        $f['precios'] = $this->getPreciosProducto($f['id'], $f['pre_inv']);
        return $f;
      }else{
        return false;
      }
    }

    public function asignarUbicacionProducto($idp, $ida, $idu){
      $sql = "UPDATE stock_almacen SET id_ubicacion=$idu WHERE id_producto=$idp AND id_almacen=$ida;";
      return $this->orm->editarPersonalizado($sql);
    }

    public function getUbicacionProducto($id_almacen, $id_producto){
        $sql = "SELECT id_ubicacion FROM stock_almacen WHERE id_producto=$id_producto AND id_almacen=$id_almacen ORDER BY id_stock DESC limit 1;";
        $r = $this->orm->consultaPersonalizada($sql);
        if($fr = $r->fetch_assoc()){
          if($fr['id_ubicacion']==null)
            return null;
          $ubicacion = new Ubialmacen();
          $ubi = $ubicacion->findById($fr['id_ubicacion']);
          return $ubi;
        }else{
          return null;
        }
    }

    public function calcularIva($precio, $iva){
      $iva = ($precio*$iva)/100;
      return $iva;
    }

    public function getPrecioPromedio($idp, $can, $cos){
      $disponible = $this->getExistenciaDisponible($idp);
      $producto = $this->findById($idp);
      $pro1 = ($disponible*$producto['pre_inv']) + ($can*$cos);
      $pro2 = ($disponible + $can);
      $promedio = $pro1 / $pro2;
      return $promedio;
    }

    public function fetchAll(){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, V.id as idv, M.id as idm  FROM ".$this->tabla." V, categorias M, ivas I, presentaciones_inventario P  WHERE V.id_categoria=M.id AND V.id_empresa='$ide' AND V.id_iva=I.id AND V.id_presentacion=P.id ORDER BY V.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchAlldatatable($sql_env){
      $ide = $_SESSION['ide'];
      $sql = $sql_env;
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchUltimos(){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, V.id as idv, M.id as idm  FROM ".$this->tabla." V, categorias M, ivas I, presentaciones_inventario P  WHERE V.id_categoria=M.id AND V.id_empresa='$ide' AND V.id_iva=I.id AND V.id_presentacion=P.id ORDER BY V.id DESC LIMIT 50;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function getPresentaciones(){
      $sql = "SELECT * FROM presentaciones_inventario;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function getExistenciaDisponibleAlmacen($idp, $ida){
      $comprometido = $this->getCantidadComprometidaAlmacen($idp, $ida);
      $reservada = $this->getCantidadReservadaAlmacen($idp, $ida);
      $sql = "SELECT stock_almacen as can_inv FROM stock_almacen WHERE id_producto='$idp' AND id_almacen=$ida;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($fr = $r->fetch_assoc()){
        $total = $fr['can_inv'];
        $total = $total-$comprometido-$reservada;
        return $total;
      }else{
        return 0;
      }
    }

    public function getExistenciaDisponible($idp){
      $comprometido = $this->getCantidadComprometida($idp);
      $reservada = $this->getCantidadReservada($idp);
      $sql = "SELECT can_inv FROM inventario WHERE id='$idp';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($fr = $r->fetch_assoc()){
        $total = $fr['can_inv'];
        $total = $total-$comprometido-$reservada;
        return $total;
      }else{
        return 0;
      }
    }

    public function getUltimoSaldoByAlmacen($id_almacen, $id_producto){
        $sql = "SELECT stock_almacen saldo_almacen FROM stock_almacen WHERE id_producto=$id_producto AND id_almacen=$id_almacen ORDER BY id_stock DESC limit 1;";
        $r = $this->orm->consultaPersonalizada($sql);
        if($fr = $r->fetch_assoc()){
          return $fr['saldo_almacen'];
        }else{
          return 0;
        }
    }

    public function getExistenciaDisponibleByAlmacenes($idp){
      $almacen = new Almacen();
      $almacenes = $almacen->fetchAll();
      $a =[];
      while($alma = $almacenes->fetch_assoc()){
          $alma['saldo_almacen']=$this->getUltimoSaldoByAlmacen($alma['id'], $idp);
          $alma['ubicacion'] = $this->getUbicacionProducto($alma['id'], $idp);
          $a[]=$alma;
      }
      return $a;
    }

    public function getCantidadComprometidaAlmacen($idp, $ida){
      $sql = "SELECT sum(D.can_ven)-D.can_des reservado FROM detalle_factura D, facturas F WHERE D.id_factura=F.id AND (F.tipo=1 OR F.tipo=0) AND F.id_almacen=$ida AND F.despacho=0 AND D.id_inventario='$idp' AND F.id_empresa='".$_SESSION['ide']."' GROUP BY D.id_inventario, D.id;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($fr = $r->fetch_assoc()){
        return $fr['reservado'];
      }else{
        return 0;
      }
    }

    public function getCantidadComprometida($idp){
      $sql = "SELECT sum(D.can_ven)-D.can_des reservado FROM detalle_factura D, facturas F WHERE D.id_factura=F.id AND (F.tipo=1 OR F.tipo=0) AND F.despacho=0 AND D.id_inventario='$idp' AND F.id_empresa='".$_SESSION['ide']."' GROUP BY D.id_inventario, D.id;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($fr = $r->fetch_assoc()){
        return $fr['reservado'];
      }else{
        return 0;
      }
    }
    public function getCantidadReservadaAlmacen($idp, $ida){
      $sql = "SELECT sum(D.can_ven) reservado FROM detalle_factura D, facturas F WHERE D.id_factura=F.id AND F.id_almacen=$ida AND F.tipo=-1 AND D.id_inventario='$idp' AND F.id_empresa='".$_SESSION['ide']."' GROUP BY D.id_inventario, D.id;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($fr = $r->fetch_assoc()){
        return $fr['reservado'];
      }else{
        return 0;
      }
    }
    public function getCantidadReservada($idp){
      $sql = "SELECT sum(D.can_ven) reservado FROM detalle_factura D, facturas F WHERE D.id_factura=F.id AND F.tipo=-1 AND D.id_inventario='$idp' AND F.id_empresa='".$_SESSION['ide']."' GROUP BY D.id_inventario, D.id;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($fr = $r->fetch_assoc()){
        return $fr['reservado'];
      }else{
        return 0;
      }
    }

    public function fetchAllNormalesByCate($idc){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, V.id as idv, M.id as idm  FROM ".$this->tabla." V, categorias M, presentaciones_inventario P  WHERE V.id_presentacion=P.id AND V.id_categoria=M.id AND V.compuesto=0 AND V.id_empresa='$ide' AND V.id_categoria=$idc ORDER BY V.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function obtenerTodoslosProductosNormalesArray($id_almacen, $id_categoria, $filtrar, $lim, $ind){
      $ide = $_SESSION['ide'];
      if($id_almacen!=""){
        $alma_sql1 = ", stock_almacen S  ";
        $alma_sql2 = " AND S.id_producto=V.id AND S.id_almacen=$id_almacen ";
      }

      $cate_sql = "";
      if($id_categoria!="" && $filtrar==""){
        $cate_sql = "  AND V.id_categoria=$id_categoria";
      }

      $limit_sql = '';
      if($lim!='' && $ind!=''){
        $limit_sql = " LIMIT $ind,$lim";
      }

      $filtro = '';
      if($filtrar!=""){
        $filtro = "  AND (V.cod_inv like '%$filtrar%' OR V.nom_inv like '%$filtrar%' OR V.des_inv like '%$filtrar%')";
        //$filtro = "  AND V.nom_inv like '%$filtrar%'";
      }

      $sql = "SELECT *, V.id id_producto, V.id as idv, M.id as idm, (SELECT porcentaje FROM ivas WHERE id=V.id_iva) as ivap  FROM ".$this->tabla." V, categorias M, presentaciones_inventario P $alma_sql1  WHERE V.id_presentacion=P.id AND V.id_categoria=M.id AND V.compuesto=0 AND V.id_empresa='$ide' AND V.can_inv>0 AND V.pre_ven_inv > 0 AND V.est_inv=1 $cate_sql $alma_sql2 $filtro ORDER BY V.id DESC $limit_sql;";
      $a=[];
      $r = $this->orm->consultaPersonalizada($sql);
     while($f = $r->fetch_assoc()){
       $a[]=$f;
     }
     return $a;
    }

    public function obtenerTodoslosProductosNormalesTot($id_almacen, $id_categoria, $filtrar){
      $ide = $_SESSION['ide'];
      if($id_almacen!=""){
        $alma_sql1 = ", stock_almacen S  ";
        $alma_sql2 = " AND S.id_producto=V.id AND S.id_almacen=$id_almacen ";
      }

      $cate_sql = "";
      if($id_categoria!="" && $filtrar==""){
        $cate_sql = "  AND V.id_categoria=$id_categoria";
      }

      $filtro = '';
      if($filtrar!=""){
        $filtro = "  AND (V.cod_inv like '%$filtrar%' OR V.nom_inv like '%$filtrar%' OR V.des_inv like '%$filtrar%')";
        //$filtro = "  AND V.nom_inv like '%$filtrar%'";
      }

      $sql = "SELECT *, V.id as idv, M.id as idm  FROM ".$this->tabla." V, categorias M, presentaciones_inventario P $alma_sql1  WHERE V.id_presentacion=P.id AND V.id_categoria=M.id AND V.compuesto=0 AND V.id_empresa='$ide' AND V.pre_inv > 0 AND V.can_inv>0 AND V.est_inv=1 $cate_sql $alma_sql2 $filtro ORDER BY V.id DESC;";
      $a=[];
      $r = $this->orm->consultaPersonalizada($sql);
      $tot_pag = $r->num_rows;
     
     return $tot_pag;
    }

    public function obtenerTodoslosProductosNormales($id_almacen, $id_categoria, $mostrar_existencia){
      $ide = $_SESSION['ide'];
      if($id_almacen!=""){
        $alma_sql1 = ", stock_almacen S  ";
        $alma_sql2 = " AND S.id_producto=V.id AND S.id_almacen=$id_almacen ";

        if($mostrar_existencia != null)
          $exi_sql = " AND S.stock_almacen>0";
      }else{
        if($mostrar_existencia != null)
          $exi_sql = " AND V.can_inv>0";
      }
      if($id_categoria!=""){
        $cate_sql = "  AND V.id_categoria=$id_categoria";
      }



      $sql = "SELECT *, V.id as idv, M.id as idm  FROM ".$this->tabla." V, categorias M, presentaciones_inventario P $alma_sql1  WHERE V.id_presentacion=P.id AND V.id_categoria=M.id AND V.compuesto=0 AND V.id_empresa='$ide' $cate_sql $alma_sql2 $exi_sql ORDER BY V.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchAllNormales(){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, V.id as idv, M.id as idm  FROM ".$this->tabla." V, categorias M, presentaciones_inventario P  WHERE V.id_presentacion=P.id AND V.id_categoria=M.id AND V.compuesto=0 AND V.id_empresa='$ide' ORDER BY V.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function alertaInventarioByAlmacen($id_almacen){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, V.id as idv, M.id as idm  FROM ".$this->tabla." V, categorias M, presentaciones_inventario P, stock_almacen S WHERE V.id=S.id_producto AND S.id_almacen=$id_almacen AND V.id_presentacion=P.id AND V.id_categoria=M.id AND V.compuesto=0 AND (V.can_inv < V.min OR V.can_inv > V.max) AND V.id_empresa=$ide AND EXISTS (SELECT id FROM detalle_entradas_salidas D WHERE D.id_producto=V.id) ORDER BY V.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }


    public function alertaInventario(){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, V.id as idv, M.id as idm  FROM ".$this->tabla." V, categorias M, presentaciones_inventario P WHERE V.id_presentacion=P.id AND V.id_categoria=M.id AND V.compuesto=0 AND (V.can_inv < V.min OR V.can_inv > V.max) AND V.id_empresa=$ide AND EXISTS (SELECT id FROM detalle_entradas_salidas D WHERE D.id_producto=V.id) ORDER BY V.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function getProductosCompuestos(){
      $ide = $_SESSION['ide'];
      $sql = "SELECT * FROM inventario WHERE compuesto=1 AND id_empresa=$ide;";
      return $this->orm->consultaPersonalizada($sql);
    }
    
    public function getSliders(){
      $sql = "SELECT *, V.id as idv, M.id as idm  FROM ".$this->tabla." V, categorias M WHERE V.id_categoria=M.id AND V.slider=1 ORDER BY V.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function buscarPorUbicacionAlmacen($idc, $ida){
        $ide=$_SESSION['ide'];
        if($idc=="sin_ubicacion"){
          $sql="SELECT I.*, S.stock_almacen, C.categoria FROM " . $this->tabla . " I, categorias C, stock_almacen S WHERE S.id_producto=I.id AND S.id_almacen=$ida AND (S.id_ubicacion is null) AND I.id_categoria=C.id AND I.id_empresa=$ide;";
        }else{
          $sql="SELECT I.*, S.stock_almacen, C.categoria FROM " . $this->tabla . " I, categorias C, stock_almacen S WHERE S.id_producto=I.id AND S.id_almacen=$ida AND S.id_ubicacion=$idc AND I.id_categoria=C.id AND I.id_empresa=$ide;";
        }

        $r = $this->orm->consultaPersonalizada($sql);
        $a=[];
       while($f = $r->fetch_assoc()){
         $f['ubicacion']=$this->getUbicacionProducto($ida, $f['id']);
         $a[]=$f;
       }
       return $a;
    }

    public function buscarPorCategoriaAlmacen($idc, $ida){
      $ide=$_SESSION['ide'];
      $sql = "SELECT I.*, C.categoria FROM " . $this->tabla . " I, categorias C, stock_almacen S WHERE S.id_producto=I.id AND S.id_almacen=$ida AND I.id_categoria=$idc AND I.id_categoria=C.id AND I.id_empresa=$ide;";
        $r = $this->orm->consultaPersonalizada($sql);
        $a=[];
       while($f = $r->fetch_assoc()){
         $f['ubicacion']=$this->getUbicacionProducto($ida, $f['id']);
         $a[]=$f;
       }
       return $a;
    }

    public function buscarPorNombreAlmacen($txt, $id_almacen){
      $ide=$_SESSION['ide'];
      $sql = "SELECT I.*, C.categoria FROM " . $this->tabla . " I, categorias C, stock_almacen S WHERE S.id_producto=I.id AND S.id_almacen=$id_almacen AND (I.cod_inv like '$txt%' OR I.nom_inv like '%$txt%') AND I.id_categoria=C.id AND I.id_empresa=$ide ORDER BY I.cod_inv;";
        $r = $this->orm->consultaPersonalizada($sql);
        $a=[];
       while($f = $r->fetch_assoc()){
          $f["monto_iva"]=$this->calcularIva($f['pre_ven_inv'], $f['ivap']);
          $f['disponible']=$this->getExistenciaDisponible($f['id']);
          $f['comprometido']=$this->getCantidadComprometida($f['id']);
          $f['reservado']=$this->getCantidadReservada($f['id']);
          $f['stock_almacen']=$this->getUltimoSaldoByAlmacen($id_almacen,$f['id']);
          $f['precios'] = $this->getPreciosProducto($f['id'], $f['pre_inv']);
          $f['ubicacion']=$this->getUbicacionProducto($id_almacen, $f['id']);
         $a[]=$f;
       }
       return $a;
    }

    public function buscarPorNombre($txt, $id_almacen){
      $ide=$_SESSION['ide'];
      $sql = "SELECT I.*, C.categoria, IV.porcentaje ivap FROM " . $this->tabla . " I, categorias C, ivas IV WHERE I.id_iva=IV.id AND (I.cod_inv like '$txt%' OR I.nom_inv like '%$txt%') AND I.id_categoria=C.id AND I.id_empresa=$ide ORDER BY I.cod_inv;";
        $r = $this->orm->consultaPersonalizada($sql);
        $a=[];
       while($f = $r->fetch_assoc()){
          $f["monto_iva"]=$this->calcularIva($f['pre_ven_inv'], $f['ivap']);
          $f['disponible']=$this->getExistenciaDisponible($f['id']);
          $f['comprometido']=$this->getCantidadComprometida($f['id']);
          $f['reservado']=$this->getCantidadReservada($f['id']);
          $f['stock_almacen']=$this->getUltimoSaldoByAlmacen($id_almacen,$f['id']);
          $f['disponible_almacen']=$this->orm->montoTxt($this->getExistenciaDisponibleAlmacen($f['id'], $id_almacen));
          $f['precios'] = $this->getPreciosProducto($f['id'], $f['pre_inv']);
         $a[]=$f;
       }
       return $a;
    }

    public function obtenerAuditoriasCreadas($f1, $f2, $est){
      $ide = $_SESSION['ide']; 
      if($est != null)
        $est = "AND estatus=$est";
      $sql = "SELECT * FROM auditoria_inventario WHERE fecha_inicio>='$f1 00:00:00' AND fecha_inicio<='$f2 23:59:59' $est ORDER BY id DESC;;";
      $r = $this->orm->consultaPersonalizada($sql);
      $a=[];
     while($f = $r->fetch_assoc()){
       $f['estatus_'] = $this->getEstatus($f['estatus']);
       $a[]=$f;
     }
     return $a;
    }

    public function ultimasSalidas($id){
      $sql = "SELECT * FROM detalle_salidas D, entradas_salidas E WHERE D.id_salida=E.id AND D.id_producto='$id' LIMIT 15";
      $r = $this->orm->consultaPersonalizada($sql);
      $a=[];
       while($f = $r->fetch_assoc()){
         $a[]=$f;
       }
       return $a;
    }
    public function ultimasEntradas($id){
      $sql = "SELECT * FROM detalle_entradas D, entradas_salidas E WHERE D.id_entrada=E.id AND D.id_producto='$id' LIMIT 15";
      $r = $this->orm->consultaPersonalizada($sql);
      $a=[];
       while($f = $r->fetch_assoc()){
         $a[]=$f;
       }
       return $a;
    }

    public function deleteIngredientes($idi){
      $sql = "DELETE FROM elementos_inventario WHERE id_inventario=$idi;";
      $this->orm->eliminarPersonalizado($sql);
    }

    public function saveIngredientes($id, $productos, $cantidades){
      for($i = 0; $i<count($productos);$i++){
        $sql = "INSERT INTO elementos_inventario values(null, $id, ".$productos[$i].", ".$cantidades[$i].");";
        $this->orm->insertarPersonalizado($sql);
      }
    }

    public function getIngredientesById($id){
      $sql = "SELECT * from elementos_inventario E, inventario I WHERE E.id_elemento=I.id AND E.id_inventario=$id";
      $r = $this->orm->consultaPersonalizada($sql);
      $a=[];
       while($f = $r->fetch_assoc()){
         $a[]=$f;
       }
     return $a;
    }

    public function getIngredientes(){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, I.id as idp FROM ".$this->tabla." I, categorias C, presentaciones_inventario P WHERE I.id_presentacion=P.id AND I.id_categoria=C.id AND I.compuesto=0 AND I.id_empresa=$ide ORDER BY I.id;";
      $r = $this->orm->consultaPersonalizada($sql);
      $a=[];
       while($f = $r->fetch_assoc()){
         $a[]=$f;
       }
     return $a;
    }

    public function ultimosProductos($idc){
      $ide = $_SESSION['ide'];
      $sql = "SELECT * FROM ".$this->tabla." WHERE est_inv=1 AND id_categoria='$idc' AND pos=1 AND id_empresa='$ide' ORDER BY id DESC LIMIT 20;";
      $r = $this->orm->consultaPersonalizada($sql);
      $a=[];
     while($f = $r->fetch_assoc()){
       $a[]=$f;
     }
     return $a;
    }

    public function ultimosProductosVendidos($limit){
      $ide = $_SESSION['ide'];
      $sql = "select *, I.id id_producto, I.id idv from detalle_factura D, inventario I WHERE D.id_inventario=I.id AND I.id_empresa=$ide AND I.est_inv=1 AND I.pre_inv > 0 AND I.can_inv>0 GROUP BY D.id_inventario, I.cod_inv ORDER BY D.id DESC LIMIT $limit;";
      $r = $this->orm->consultaPersonalizada($sql);
      $a=[];
      while($f = $r->fetch_assoc()){
        $a[]=$f;
      }
      return $a;
    }

    public function productosActivos(){
      $ide = $_SESSION['ide'];
      $sql = "SELECT * FROM ".$this->tabla." V WHERE est_inv=1 AND id_empresa='$ide' ORDER BY id DESC;";
      $r = $this->orm->consultaPersonalizada($sql);
      $a=[];
     while($f = $r->fetch_assoc()){
       $a[]=$f;
     }
     return $a;
    }
    public function saveNuevoConteo(){
      $sql = "INSERT INTO conteo_auditoria VALUES(";
      $i = 0;
      $n = count($this->dataNuevoConteo);
      foreach($this->dataNuevoConteo as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= strtoupper("'$index'");
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }
    public function saveDetalleConteo(){
      $sql = "INSERT INTO seleccionados_auditoria VALUES(";
      $i = 0;
      $n = count($this->dataDetalleConteo);
      foreach($this->dataDetalleConteo as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= strtoupper("'$index'");
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

    public function saveConteo(){
      $sql = "INSERT INTO auditoria_inventario VALUES(";
      $i = 0;
      $n = count($this->dataConteo);
      foreach($this->dataConteo as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= strtoupper("'$index'");
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= strtoupper("'$index'");
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

      public function editConteo($id){
        $sql = "UPDATE auditoria_inventario SET ";
        $i = 0;
        $n = count($this->dataConteo);
        foreach($this->dataConteo as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);
    }
      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);
    }
  }
?>
