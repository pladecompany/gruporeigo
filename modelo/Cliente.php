<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Cliente{
    private $tabla = "clientes";
    public $data = [];
    public $orm = null;
    public $dias_credito = [7, 15, 30, 50];
    public $dias_totales = [15, 30, 45, 60];

    public function Cliente(){
      $this->orm = new Orm(new Conexion());
      $tihs->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE (id='$id' OR cod_cli='$id');";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function findByCodNac($id,$nac){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE cod_cli='$id' AND naci='$nac';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function getDiasCredito(){
      return $this->dias_credito;
    }

    public function findByCod($id, $idc){
      if($idc != "")
        $aux = " AND id<>'$idc'";
      else $auto = "";

      $sql = "SELECT * FROM " . $this->tabla . " WHERE cod_cli='$id'$aux;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function login($u, $p){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE ced_usu='$u' AND pas_usu='$p';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function filtroClientesSelect($txt){
      $ide = $_SESSION['ide'];
      $a5 =[];

      if(strlen($txt)<=2)
        return $a5;

      $sql = "SELECT * FROM clientes WHERE cod_cli like '$txt%' OR nom_cli like '%$txt%' ORDER BY cod_cli limit 10;";
      $r5= $this->orm->consultaPersonalizada($sql);
      while($f5 = $r5->fetch_assoc()){
        $a5[] = array("id"=>$f5['cod_cli'], "text"=>$f5['naci'].$f5['cod_cli']." - ".$f5['nom_cli']);
      }
      return $a5;
    }

    public function filtrarClientes($txt){
      $ide = $_SESSION['ide'];
      $sql = "SELECT * FROM clientes WHERE cod_cli like '$txt%' OR nom_cli like '%$txt%' limit 10;";
      $r5= $this->orm->consultaPersonalizada($sql);
      $a5 =[];
      while($f5 = $r5->fetch_assoc()){
        $a5[] = $f5;
      }
      return $a5;
    }

    public function enviarCorreo($para, $mensaje, $asunto){
      return $this->orm->enviarCorreo($para, $mensaje, $asunto);
    }

    public function validarOlvido($ced, $cor){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE ced_usu='$ced' AND cor_usu='$cor';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }

    }

    public function cambiarClave($act, $nue, $idu){
      $sql = "UPDATE usuarios SET pas_usu='$nue' WHERE id='$idu' AND pas_usu='$act';";
      return $this->orm->editarPersonalizado($sql);
    }

    public function findByEmail($id, $idc){
      if($idc != "")
        $aux = " AND id<>'$idc'";
      else $auto = "";
      $sql = "SELECT * FROM " . $this->tabla . " WHERE cor_cli='$id'$aux;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function fetchAll(){
      $sql = "SELECT * FROM ".$this->tabla." ORDER BY id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function save(){
 
      $n = count($this->data);
      $sql = "INSERT INTO ".$this->tabla ." (";
      $j=0;

      foreach($this->data as $key => $index){
        $j++;
        $sql.= "$key";
        if($j < $n){
          $sql.= ",";
        }
      }

      $sql.= ") VALUES(";
      $i = 0;

      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          if($index=="")
            $sql.= $key."=null";
          else
            $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id=$id;";
      return $this->orm->editarPersonalizado($sql);
    }

  }
?>
