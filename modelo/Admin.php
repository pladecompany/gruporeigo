<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Admin{
    private $tabla = "usuarios";
    public $data = [];
    public $niveles = array('0'=>'Administrador', '1'=>'Vendedor', '2'=>'Reportes');
    public $orm = null;
    public $visitados = [];

    public function Admin(){
      $this->orm = new Orm(new Conexion());
      $tihs->data = [];
    }

    public function login($u, $p){
      $sql = "SELECT * FROM usuarios WHERE cor_adm='$u' AND pas_adm=md5('$p');";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function getModulos(){
      $sql = "SELECT * FROM modulos where dependiente=0 and estatus=1 ORDER BY modulo;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function saveVisita($idu, $mod){
      $sql = "SELECT * FROM visitados WHERE id_usuario='$idu' ORDER BY id DESC LIMIT 6;";
      $r = $this->orm->consultaPersonalizada($sql);
      $i=0;
      while($f = $r->fetch_assoc()){
        if($f['id_modulo'] == $mod['id'])
          $i++;
      }

      if($i==0){
        $sql = "INSERT INTO visitados VALUES(null, $idu, ".$mod['id'].");";
        return $this->orm->insertarPersonalizado($sql);
      }else return false;
    }

    public function getVisitados($idu){
      $sql = "SELECT * FROM visitados V, modulos M WHERE V.id_modulo=M.id AND V.id_usuario='$idu' AND M.mostrar=1 ORDER BY V.id DESC LIMIT 6;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function getModulosByDepen($id){
      $sql = "SELECT * FROM modulos WHERE dependiente='$id' and estatus=1 ORDER BY modulo;";
      return $this->orm->consultaPersonalizada($sql);
    }

    
    public function borrarRoles($idu){
      return $this->orm->eliminar('id_usuario', $idu, "asignar_modulos");
    }

    public function borrarRoles_CARGOS($idu){
      return $this->orm->eliminar('id_cargo', $idu, "asignar_modulos_cargos");
    }

    public function borrarEmpresas($idu){
      return $this->orm->eliminar('id_usuario', $idu, "asignar_empresas");
    }

    public function asignarRoles($modulos, $idu){
      for($i=0; $i<count($modulos);$i++){
        $sql = "INSERT INTO asignar_modulos VALUES(null, $idu, ".$modulos[$i].");";
        $this->orm->insertarPersonalizado($sql);
      }
      return $i;
    }

    public function asignarRoles_CARGOS($modulos, $idu){
      for($i=0; $i<count($modulos);$i++){
        $sql = "INSERT INTO asignar_modulos_cargos VALUES(null, $idu, ".$modulos[$i].");";
        $this->orm->insertarPersonalizado($sql);
      }
      return $i;
    }

    public function getEmpresas($idu){
      $sql = "SELECT *, E.id as id_empresa FROM asignar_empresas A, empresas E where A.id_empresa=E.id AND A.id_usuario=$idu;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function asignarEmpresas($empresas, $idu){
      for($i=0; $i<count($empresas);$i++){
        $sql = "INSERT INTO asignar_empresas VALUES(null, $idu, ".$empresas[$i].", NOW());";
        $this->orm->insertarPersonalizado($sql);
      }
      return $i;
    }

    public function getPermisos($idu){
      $sql = "SELECT *, M.id as id_rol FROM asignar_modulos A, modulos M WHERE A.id_usuario='$idu' AND A.id_modulo=M.id ORDER BY M.modulo;" ;
      return $this->orm->consultaPersonalizada($sql);
    }

    public function getSubpermisos($idu, $idr){
      $sql = "SELECT *, M.id as id_rol FROM asignar_modulos A, modulos M WHERE A.id_modulo=M.id AND A.id_usuario='$idu' AND M.dependiente='$idr' ORDER BY M.modulo;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function verificarRol($rol, $idu){
      $sql = "SELECT * FROM asignar_modulos WHERE id_usuario='$idu' AND id_modulo='$rol';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return true;
      }else{
        return false;
      }
    }

    public function verificarRol_cargos($rol, $idu){
      $sql = "SELECT * FROM asignar_modulos_cargos WHERE id_cargo='$idu' AND id_modulo='$rol';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return true;
      }else{
        return false;
      }
    }

    public function validarPermisoDeEntrada($idu, $opc){
      $sql = "SELECT * FROM asignar_modulos A, modulos M WHERE A.id_usuario='$idu' AND A.id_modulo=M.id AND M.cod_mod='$opc' AND estatus=1;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows>0){
        return true;
      }else{
        return false;
      }
    }

    public function removeById($id){
      $this->borrarRoles($id);
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function getNiveles(){
      return $this->niveles;
    }

    public function getNivel($index){
      return $this->niveles[$index];
    }

    public function actualizarPassword($n, $a, $ida){
      $sql = "UPDATE usuarios SET pas_adm=md5('$n') WHERE id='$ida' AND pas_adm=md5('$a');";
      return $this->orm->editarPersonalizado($sql);
    }

    public function fetchAll(){
      $sql = "SELECT * FROM ".$this->tabla." ORDER BY id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }


      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          if($index == "")
            $sql.= $key."=null";
          else
            $sql.= $key."='$index'";

          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }
    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }
    public function getRolById($id){
      $sql = "SELECT * FROM modulos WHERE id='$id' and estatus=1;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }
    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }


  }
?>
