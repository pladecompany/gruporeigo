<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Empresa{
    private $tabla = "empresas";
    public $data = [];
    public $orm = null;

    public function Empresa(){
      $this->orm = new Orm(new Conexion());
      $tihs->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function fetchAllActivas(){
      $sql = "SELECT * FROM ".$this->tabla." WHERE est_emp=1;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function verificarEmpresa($emp, $idu){
      $sql = "SELECT * FROM asignar_empresas WHERE id_usuario='$idu' AND id_empresa='$emp';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return true;
      }else{
        return false;
      }
    }

    public function fetchAll(){
      $sql = "SELECT * FROM ".$this->tabla."";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id=$id;";
      return $this->orm->editarPersonalizado($sql);
    }

  }
?>
