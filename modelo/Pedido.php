<?php

  include_once("Orm.php");
  include_once("Conexion.php");
  include_once("Almacen.php");
  include_once("NotaEntrega.php");
  include_once("Salida.php");
  include_once("Cliente.php");
  include_once("Salida.php");
  include_once("Pago.php");
  include_once("Admin.php");
  include_once("Miscuentas.php");
  include_once("Lote.php");

  class Pedido{
    private $tabla = "facturas";
    public $data = [];
    public $dataDetalle = [];
    public $estatus = array("-5"=>array("txt"=>"ANULADA", "col"=>"#dc3545"), "-1"=>array("txt"=>"PEDIDO", "col"=>"#f59c2d"), "0"=>array("txt"=>"CRÉDITO", "col"=>"#1f85ef"), "1"=>array("txt"=>"CONTADO", "col"=>"#28a745"));
    public $estatus_factura = array("0"=>array("txt"=>"POR COBRAR", "col"=>"#f59c2d"), "1"=>array("txt"=>"PAGADA", "col"=>"#28a745"), "-5"=>array("txt"=>"ANULADO", "col"=>"#dc3545"));
    public $estatus_despacho = array("0"=>array("txt"=>"POR DESPACHAR", "col"=>"#f59c2d"), "1"=>array("txt"=>"DESPACHADO", "col"=>"#28a745"));
    public $tipos_almacen=array("1"=>array("txt"=>"MINORISTA"), "2"=>array("txt"=>"MAYORISTA"));

    public $orm = null;

    public function Pedido(){
      $tihs->data = [];
      $this->orm = new Orm(new Conexion());
    }

    public function getEstatus($est){
      return $this->estatus[$est];
    }

    public function getTipoAlmacen($index){
      return $this->tipos_almacen[$index];
    }

    public function fetchEstatusFacturas(){
      return $this->estatus;
    }

    public function getEstatusDespacho($est){
      return $this->estatus_despacho[$est];
    }

    public function getEstatusFactura($est){
      return $this->estatus_factura[$est];
    }
    public function removeDetallesByFactura($idf){
      return $this->orm->eliminar('id_factura', $idf, "detalle_factura");
    }

    public function remove($idf){
      return $this->orm->eliminar('id', $idf, "facturas");
    }

    public function removePagos($idf){
      return $this->orm->eliminar('id_factura', $idf, "pagos");
    }

    public function anularFactura($id){
      $fac = $this->findById($id);
      if($fac['estatus'] == -5){
        return false;
      }
      $sal = new Salida();
      $salida = $sal->findByFactura($id);
      $r = $sal->anularSalida($salida['id']);
      $this->data['estatus'] = -5;
      return $this->edit($id);
      //$this->orm->eliminar('id_factura', $id, "pagos");
      //$this->orm->eliminar('id_factura', $id, "detalle_factura");
      //$this->orm->eliminar('id', $id, $this->tabla);
    }

    public function removeById($id){
      $sal = new Salida();
      $salida = $sal->findByFactura($id);
      $sal->removeById($salida['id']);
      $this->orm->eliminar('id_factura', $id, "pagos");
      $this->orm->eliminar('id_factura', $id, "detalle_factura");
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function findByFactura($id){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, F.tipo tipo_factura, F.id as idf, F.id_vendedor vendedor,  C.id as idc FROM ".$this->tabla." F, clientes C, estatus_pedidos E WHERE F.id_est=E.id AND F.id_cliente=C.id AND (F.cod_fac='$id' OR F.id=$id) AND F.id_empresa=$ide;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        $f = $r->fetch_assoc();
        $f["tipo_"] = $this->getEstatus($f['tipo_factura']);
        $f["estatus_"] = $this->getEstatusFactura($f['estatus']);
        $f["despacho_"] = $this->getEstatusDespacho($f['despacho']);
        $usu = new Admin();
        $f['usuario'] = $usu->findById($f['id_usuario']);
        $f['vendedor'] = $usu->findById($f['vendedor']);
        $almacen = new Almacen();
        $f['almacen']= $almacen->findById($f['id_almacen']);
        $f['tipo_almacen_']=$this->getTipoAlmacen($f['tipo_almacen']);
        return $f;
      }else{
        return false;
      }
    }
    public function findById($id){
      $sql = "SELECT *, F.id as idf, C.id as idc, F.tipo tipo_factura, F.id_vendedor vendedor FROM ".$this->tabla." F, clientes C, estatus_pedidos E WHERE F.id_est=E.id AND F.id_cliente=C.id AND F.id=$id;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        $f = $r->fetch_assoc();
        $f["tipo_"] = $this->getEstatus($f['tipo_factura']);
        $f["estatus_"] = $this->getEstatusFactura($f['estatus']);
        $f["despacho_"] = $this->getEstatusDespacho($f['despacho']);
        return $f;
      }else{
        return false;
      }
    }

    public function fetchDetalles2($idf){
      $sql = "SELECT * FROM detalles_pedido WHERE id_pedido=$idf";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function findDetalle($id){
      $sql = "SELECT D.*, F.id_almacen FROM detalle_factura D, facturas F WHERE D.id='$id' AND D.id_factura=F.id;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function editDetalle($id){
      $sql = "UPDATE detalle_factura SET ";
      $i = 0;
      $n = count($this->dataDetalle);
      foreach($this->dataDetalle as $key => $index){
        $i++;
        if($index == "")
          $sql.= $key."=null";
        else
          $sql.= $key."='$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= " WHERE id='$id';";
      $this->dataDetalle=[];
      return $this->orm->editarPersonalizado($sql);
    }

    public function getPedidosPorDespachar(){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, F.tipo tipo_factura, F.id as idf FROM ".$this->tabla." F, clientes C, estatus_pedidos E, usuarios U WHERE F.id_usuario=U.id AND F.id_est=E.id AND F.id_cliente=C.id AND (F.tipo=0 OR F.tipo=1) AND id_empresa='$ide' AND F.despacho=0 ORDER BY F.id DESC;";
      $r5= $this->orm->consultaPersonalizada($sql);
      $a5 =[];
      while($f5 = $r5->fetch_assoc()){
        $f5["tipo_"] = $this->getEstatus($f5['tipo_factura']);
        $f5["estatus_"] = $this->getEstatusFactura($f5['estatus']);
        $a5[] = $f5;
      }
      return $a5;
    }
    public function getPedidosPorDespacharByCliente($idc){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, F.tipo tipo_factura, F.id as idf FROM ".$this->tabla." F, clientes C, estatus_pedidos E, usuarios U WHERE F.id_usuario=U.id AND F.id_est=E.id AND F.id_cliente=C.id AND (F.tipo=0 OR F.tipo=1) AND F.id_cliente=$idc AND F.id_empresa='$ide' AND F.despacho=0 ORDER BY F.id DESC;";
      $r5= $this->orm->consultaPersonalizada($sql);
      $a5 =[];
      while($f5 = $r5->fetch_assoc()){
        $f5["tipo_"] = $this->getEstatus($f5['tipo_factura']);
        $f5["estatus_"] = $this->getEstatusFactura($f5['estatus']);
        $a5[] = $f5;
      }
      return $a5;
    }

    public function getPedidosDespachados($f1, $f2){
      $sql = "SELECT *, F.tipo tipo_factura FROM despachos D, usuarios U, facturas F, clientes C WHERE F.id_cliente=C.id AND D.id_factura=F.id AND D.id_usuario=U.id AND D.fec_reg_des>='$f1 00:00:00' AND D.fec_reg_des<='$f2 23:59:59' AND F.despacho=1 GROUP BY D.id_factura DESC;";
      $r5= $this->orm->consultaPersonalizada($sql);
      $a5 =[];
      while($f5 = $r5->fetch_assoc()){
        $f5["tipo_"] = $this->getEstatus($f5['tipo_factura']);
        $f5["estatus_"] = $this->getEstatusFactura($f5['estatus']);
        $a5[] = $f5;
      }
      return $a5;
    }

    public function getPagos($idf){
      $sql = "SELECT *, P.referencia referencia_pago, M.nombre met_pago, C.nombre cuenta FROM pagos P, usuarios U, mis_cuentas C, metodos_pagos M where P.id_cuenta=C.id AND P.id_metodo=M.id AND P.id_factura='$idf' AND P.id_usuario=U.id;";
      $r5= $this->orm->consultaPersonalizada($sql);
      $a5 =[];
      while($f5 = $r5->fetch_assoc()){
        $a5[] = $f5;
      }
      return $a5;

    }
    public function calcularIva($precio, $iva){
      $iva = ($precio*$iva)/100;
      return $iva;
    }

    public function fetchDetallesServicios($idf){
      $sql = "SELECT *, D.can_ven as cantidad, D.pre_ven as precio, IV.descripcion iva, D.id id_detalle, (SELECT tipo_precio FROM tipo_precios TT WHERE D.id_tipo_precio=TT.id) tipo_precio FROM detalle_factura D, ivas IV WHERE D.id_iva=IV.id AND D.id_factura=$idf AND D.id_inventario is null;";
      $r5= $this->orm->consultaPersonalizada($sql);
      $a5 =[];
      while($f5 = $r5->fetch_assoc()){
        $fac = $this->findById($idf);
        $inv = new Inventario();
        $f5["monto_iva"]=$this->calcularIva($f5['pre_ven_inv'], $f5['ivap']);
        $f5["disponible"]=$inv->getExistenciaDisponible($f5['idp']);
        $f5['comprometido']=$inv->getCantidadComprometida($f5['idp']);
        $f5['reservado']=$inv->getCantidadReservada($f5['idp']);
        $f5['disponible_almacen']=$inv->getExistenciaDisponibleAlmacen($f5['idp'], $fac['id_almacen']);
        $a5[] = $f5;
      }
      return $a5;
    }

    public function fetchDetalles($idf){
      $sql = "SELECT *, I.id as idp, D.can_ven as cantidad, D.pre_ven as precio, IV.descripcion iva, D.id id_detalle, (SELECT tipo_precio FROM tipo_precios TT WHERE D.id_tipo_precio=TT.id) tipo_precio FROM detalle_factura D, inventario I, categorias C, ivas IV WHERE D.id_iva=IV.id AND D.id_factura=$idf AND D.id_inventario=I.id AND I.id_categoria=C.id;";
      $r5= $this->orm->consultaPersonalizada($sql);
      $a5 =[];
      while($f5 = $r5->fetch_assoc()){
        $fac = $this->findById($idf);
        $inv = new Inventario();
        $f5["monto_iva"]=$this->calcularIva($f5['pre_ven_inv'], $f5['ivap']);
        $f5["disponible"]=$inv->getExistenciaDisponible($f5['idp']);
        $f5['comprometido']=$inv->getCantidadComprometida($f5['idp']);
        $f5['reservado']=$inv->getCantidadReservada($f5['idp']);
        $f5['disponible_almacen']=$inv->getExistenciaDisponibleAlmacen($f5['idp'], $fac['id_almacen']);
        $f5['precios'] = $inv->getPreciosProducto($f5['idp'], $f5['pre_inv']);
        if($f5['lotes']==1){
          $f5['mis_lotes']=$inv->getLotes($f5['idp']);
          $f5['tipo_lote_txt']=$inv->getNombreLote($f5['tipo_lote']);
          $f5['lotes_factura']=$this->getLotesFactura($idf);
        }
        $a5[] = $f5;
      }
      return $a5;
    }

    public function getLotesFactura($idf){
      $sql = "SELECT DL.*, L.lote, L.serie, L.valor FROM detalle_lotes DL, detalle_factura DF, facturas F, lotes L WHERE DL.id_detalle_factura=DF.id AND DL.id_lote=L.id AND DF.id_factura=F.id AND F.id=$idf;";
      $lotes = $this->orm->consultaPersonalizada($sql);
      $a =[];
      while($lote = $lotes->fetch_assoc()){
        $a[]=$lote;
      }
      return $a;
    }

    public function fetchAll(){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, F.id as idf, (SELECT sum(D.can_pro*D.pre_pro) FROM detalles_pedido D WHERE D.id_pedido=F.id) as monto  FROM ".$this->tabla." F, clientes C WHERE F.id_cliente=C.id AND F.id_empresa='$ide' ORDER BY F.id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function obtenerSalidasDeProductosByUser($desde, $hasta, $idu, $est){
      $ide = $_SESSION['ide'];
      if($est !=null)
        $est="AND F.tipo='$est'";
      if($idu!=null)
        $usu = "AND F.id_usuario=$idu";

      $sql = "SELECT D.id_producto, I.cod_inv, D.nom_pro, sum(D.can_pro) cantidad, E.fec_fac, L.linea FROM detalle_entradas_salidas D, entradas_salidas E, facturas F, inventario I, lineas_inventario L WHERE I.id_linea=L.id AND E.id_factura=F.id AND D.id_producto=I.id AND D.id_es=E.id AND E.fec_fac>='$desde 00:00:00' AND E.fec_fac<='$hasta 23:59:59' AND E.id_empresa='$ide' $est $usu GROUP BY D.id_producto ORDER BY cantidad ASC;";
      $a =[];
      $r= $this->orm->consultaPersonalizada($sql);
      while($f = $r->fetch_assoc()){
        $a[]=$f;
      }
      return $a;
    }

    public function obtenerSalidasByLineas($desde, $hasta, $idu, $est){
      $ide = $_SESSION['ide'];
      if($est !=null)
        $est="AND F.tipo='$est'";

      if($idu!=null)
        $usu = "AND F.id_usuario=$idu";

      $sql = "select sum(D.pre_ven) total_venta_bs, sum(D.pre_ven/D.tasa_ven) total_venta_dl, sum(D.can_ven) can_ven, L.linea from detalle_factura D, facturas F, inventario I, lineas_inventario L WHERE F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59' AND D.id_factura=F.id AND D.id_inventario=I.id AND I.id_linea=L.id AND F.id_empresa=$ide $est $usu GROUP BY I.id_linea, L.linea;";
      $a =[];
      $r= $this->orm->consultaPersonalizada($sql);
      while($f = $r->fetch_assoc()){
        $a[]=$f;
      }
      return $a;
    }

    public function obtenerSalidasDeProductos($desde, $hasta){
      $ide = $_SESSION['ide'];
      $sql = "SELECT D.id_producto, D.nom_pro, sum(D.can_pro) cantidad, E.fec_fac FROM detalle_entradas_salidas D, entradas_salidas E WHERE D.id_es=E.id AND E.fec_fac>='$desde 00:00:00' AND E.fec_fac<='$hasta 23:59:59' AND E.id_empresa='$ide' GROUP BY D.id_producto;";
      $a =[];
      $r= $this->orm->consultaPersonalizada($sql);
      while($f = $r->fetch_assoc()){
        $a[]=$f;
      }
      return $a;
    }

    public function obtenerPedidosPorMetodoVueltoByUser($desde, $hasta, $idu){
      $ide= $_SESSION['ide'];
      $sql = "SELECT P.nombre, count(*) cantidad, sum(P.monto) monto, sum(P.monto)/P.tasa_pago monto_dolar FROM pagos P, facturas F WHERE P.id_factura=F.id AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.estatus=1 AND P.monto<0 AND F.id_usuario=$idu AND F.id_empresa='$ide' GROUP BY P.nombre;";
      $a =[];
      $r= $this->orm->consultaPersonalizada($sql);
      while($f = $r->fetch_assoc()){
        $a[]=$f;
      }
      return $a;
    }

    public function obtenerPedidosPorMetodoVuelto($desde, $hasta){
      $ide = $_SESSION['ide'];
      $sql = "SELECT P.nombre, count(*) cantidad, sum(P.monto) monto, sum(P.monto)/P.tasa_pago monto_dolar FROM pagos P, facturas F WHERE P.id_factura=F.id AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.estatus=1 AND P.monto<0 AND F.id_empresa='$ide' GROUP BY P.nombre;";
      $a =[];
      $r= $this->orm->consultaPersonalizada($sql);
      while($f = $r->fetch_assoc()){
        $a[]=$f;
      }
      return $a;
    }

    public function obtenerPedidosPorMetodoByUser($desde, $hasta, $idu, $est){
      $ide = $_SESSION['ide'];
      if($est !="")
        $est="AND F.tipo='$est'";

      $sql = "SELECT M.nombre nom_met, C.nombre nom_cue, sum(P.monto) monto, sum(P.monto)/P.tasa_pago monto_dolar FROM pagos P, mis_cuentas C, metodos_pagos M, facturas F WHERE P.id_factura=F.id AND F.id_usuario=$idu AND P.id_cuenta=C.id AND P.id_metodo=M.id AND P.id_empresa='$ide' AND P.fecha>='$desde 00:00:00' AND P.fecha<='$hasta 23:59:59' $est GROUP BY P.id_cuenta;";
      $a =[];
      $r= $this->orm->consultaPersonalizada($sql);
      while($f = $r->fetch_assoc()){
        $a[]=$f;
      }
      return $a;
    }

    public function obtenerPedidosPorMetodoComision($desde, $hasta, $idv, $est){
      if($idv!=null)
        $usuario = " AND F.id_vendedor=$idv";
      if($est!=null)
        $estatus = " AND F.tipo=$est";
      $ide = $_SESSION['ide'];
      $sql = "SELECT M.nombre nom_met, C.nombre nom_cue, sum(P.monto) monto, sum(P.monto)/P.tasa_pago monto_dolar FROM pagos P, mis_cuentas C, metodos_pagos M, facturas F WHERE P.id_factura=F.id AND P.id_cuenta=C.id AND P.id_metodo=M.id AND P.id_empresa='$ide' AND P.fecha>='$desde 00:00:00' AND P.fecha<='$hasta 23:59:59' $usuario $estatus GROUP BY P.id_cuenta;";
      $a =[];
      $r= $this->orm->consultaPersonalizada($sql);
      while($f = $r->fetch_assoc()){
        $a[]=$f;
      }
      return $a;
    }

    public function obtenerPedidosPorMetodo($desde, $hasta, $idu, $est){
      if($idu!=null&&$idu!=""){
        $usuario = " AND P.id_usuario=$idu";
        $usuario2 = " AND PC.id_usuario=$idu ";
      }

      if($est!=null&&$est!=""){
        $estatus = " AND F.tipo=$est";
        $estatus2 = " AND FC.tipo=$est";
      }

      $ide = $_SESSION['ide'];
      $sql = "SELECT M.id id_metodo, C.id id_cuenta, M.nombre nom_met, C.nombre nom_cue, (sum(P.monto)) monto, (sum(P.monto/P.tasa_pago)) monto_dolar, (SELECT count(*) from pagos PC, facturas FC where PC.id_factura=FC.id AND PC.monto>0 AND PC.id_metodo=P.id_metodo AND PC.fecha>='$desde 00:00:00' AND PC.fecha<='$hasta 23:59:59'$usuario2 $estatus2) n_metodo_e, (SELECT count(*) from pagos PC, facturas FC where PC.id_factura=FC.id AND PC.monto<0 AND PC.id_metodo=P.id_metodo AND PC.fecha>='$desde 00:00:00' AND PC.fecha<='$hasta 23:59:59'$usuario2 $estatus2) n_metodo_s FROM pagos P, mis_cuentas C, metodos_pagos M, facturas F WHERE P.id_factura=F.id AND F.tipo<>-5 AND P.id_cuenta=C.id AND P.id_metodo=M.id AND P.id_empresa='$ide' AND P.fecha>='$desde 00:00:00' AND P.fecha<='$hasta 23:59:59' $usuario $estatus GROUP BY P.id_metodo ORDER BY C.nombre;";

      $a =[];
      $r= $this->orm->consultaPersonalizada($sql);
      while($f = $r->fetch_assoc()){
        $a[]=$f;
      }
      return $a;
    }

    public function obtenerPedidosPorCuenta($desde, $hasta, $idu, $est){
      if($idu!=null){
        $usuario = " AND P.id_usuario=$idu";
        $usuario2= " AND PC.id_usuario=$idu ";
      }

      if($est!=null){
        $estatus = " AND F.tipo=$est";
        $estatus2 = " AND FC.tipo=$est";

      }
      $ide = $_SESSION['ide'];

      $sql = "SELECT C.id id_cuenta, C.nombre nom_cue, (sum(P.monto)) monto, (sum(P.monto/P.tasa_pago)) monto_dolar, (SELECT count(*) from pagos PC, facturas FC where PC.id_factura=FC.id AND PC.monto>0 AND PC.id_cuenta=P.id_cuenta AND PC.fecha>='$desde 00:00:00' AND PC.fecha<='$hasta 23:59:59'$usuario2 $estatus2) n_metodo_e, (SELECT count(*) from pagos PC, facturas FC where PC.id_factura=FC.id AND PC.monto<0 AND PC.id_cuenta=P.id_cuenta AND PC.fecha>='$desde 00:00:00' AND PC.fecha<='$hasta 23:59:59'$usuario2 $estatus2) n_metodo_s FROM pagos P, mis_cuentas C, facturas F WHERE P.id_factura=F.id AND F.tipo<>-5 AND P.id_cuenta=C.id AND P.id_empresa='$ide' AND P.fecha>='$desde 00:00:00' AND P.fecha<='$hasta 23:59:59' $usuario $estatus GROUP BY P.id_cuenta ORDER BY C.nombre;";
      $a =[];
      $r= $this->orm->consultaPersonalizada($sql);
      while($f = $r->fetch_assoc()){
        $a[]=$f;
      }
      return $a;
    }

    public function obtenerPedidosPorCuentaByUser($desde, $hasta, $idu, $est){
      if($idu!=null){
        $usuario = " AND F.id_usuario=$idu";
        $usuario2 = " AND PC.id_usuario=$idu";
      }

      if($est!=null){
        $estatus = " AND F.tipo=$est";
        $estatus2 = " AND FC.tipo=$est";
      }

      $ide = $_SESSION['ide'];

      $sql = "SELECT C.id id_cuenta, C.nombre nom_cue, (sum(P.monto)) monto, (sum(P.monto/P.tasa_pago)) monto_dolar, (SELECT count(*) from pagos PC, facturas FC where PC.id_factura=FC.id AND PC.monto>0 AND PC.id_cuenta=P.id_cuenta AND PC.fecha>='$desde 00:00:00' AND PC.fecha<='$hasta 23:59:59' $usuario2 $estatus2) n_metodo_e, (SELECT count(*) from pagos PC, facturas FC where PC.id_factura=FC.id AND  PC.monto<0 AND PC.id_cuenta=P.id_cuenta AND PC.fecha>='$desde 00:00:00' AND PC.fecha<='$hasta 23:59:59' $usuario2 $estatus2) n_metodo_s FROM pagos P, mis_cuentas C, facturas F WHERE P.id_factura=F.id AND F.tipo<>-5 AND P.id_cuenta=C.id AND P.id_empresa='$ide' AND P.fecha>='$desde 00:00:00' AND P.fecha<='$hasta 23:59:59' $usuario $estatus GROUP BY P.id_cuenta ORDER BY C.nombre;";
      $a =[];
      $r= $this->orm->consultaPersonalizada($sql);
      while($f = $r->fetch_assoc()){
        $a[]=$f;
      }
      return $a;
    }

    public function obtenerPedidosPorProductoByUser($desde, $hasta, $todo, $idu, $est){
      $ide = $_SESSION['ide'];
      if($est !=null)
        $est="AND F.tipo='$est'";
      if($idu!=null)
        $esu="ANF F.id_usuario=$idu";

      if($todo == 1)
        $sql = "select D.ivap, D.pre_ven, D.tasa_ven, D.nom_pro, D.id_inventario, D.can_ven, sum(D.can_ven) as cantidad from detalle_factura D, facturas F where D.id_factura=F.id AND (F.fec_fac>='$desde 00:00:00' AND F.id_empresa=$ide AND F.fec_fac<='$hasta 23:59:59') AND F.estatus=1 AND F.id_usuario='$idu' $est $usu GROUP BY D.id_inventario, D.tasa_ven, D.pre_ven;";
      else
        $sql = "select D.ivap, D.pre_ven, D.tasa_ven, D.nom_pro, D.id_inventario, D.can_ven, sum(D.can_ven) as cantidad, sum(D.pre_ven*D.can_ven)/D.tasa_ven monto, sum((D.pre_ven*D.can_ven)) monto_bs from detalle_factura D, facturas F where D.id_factura=F.id AND F.id_empresa=$ide AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.estatus=1 AND F.id_usuario='$idu' $est GROUP BY D.id_inventario;";

      $r= $this->orm->consultaPersonalizada($sql);
      $b =[];
      while($p = $r->fetch_assoc()){
        $b[] = $p;
      }
      return array("pedidos"=>$a, "detalles"=>$b);
    }

    public function obtenerPedidosPorProducto($desde, $hasta, $todo){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, F.id as idf FROM detalle_factura D, facturas F, inventario I WHERE D.id_factura=F.id AND F.estatus=1 AND D.id_inventario=I.id AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.id_empresa=$ide AND I.id_empresa=$ide;";
      $r= $this->orm->consultaPersonalizada($sql);
      $a =[];
      while($f = $r->fetch_assoc()){
        $a[] = $f;
      }

      if($todo == 1){
        $sql = "select D.ivap, D.pre_ven, D.tasa_ven, D.nom_pro, D.id_inventario, D.can_ven, sum(D.can_ven) as cantidad from detalle_factura D, facturas F where D.id_factura=F.id AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.id_empresa=$ide AND F.estatus=1 GROUP BY D.id_inventario, D.tasa_ven, D.pre_ven;";
      } else{
        $sql = "select D.ivap, D.pre_ven, D.tasa_ven, D.nom_pro, D.id_inventario, D.can_ven, sum(D.can_ven) as cantidad, F.cod_fac, sum((D.pre_ven * D.can_ven)/D.tasa_ven) monto, sum(((D.pre_ven * D.can_ven)/D.tasa_ven) + (((D.pre_ven * D.can_ven)/D.tasa_ven)*D.ivap)/100) monto_iva_dolar, sum((D.pre_ven*D.can_ven)) monto_bs from detalle_factura D, facturas F where D.id_factura=F.id AND (F.fec_fac>='$desde 00:00:00' AND F.id_empresa=$ide AND F.fec_fac<='$hasta 23:59:59') AND F.estatus=1 GROUP BY D.id_inventario ORDER BY cantidad DESC;";
      }

      $r= $this->orm->consultaPersonalizada($sql);
      $b =[];
      while($p = $r->fetch_assoc()){
        $b[] = $p;
      }
      return array("pedidos"=>$a, "detalles"=>$b);
    }

    public function obtenerPedidosACreditoByClienteSoloCredito($idc){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, F.tipo as tipo_factura, F.id as idf, (SELECT sum((D.can_ven * (D.pre_ven * D.tasa_ven))+(D.can_ven * (D.pre_ven * D.tasa_ven))*D.ivap/100) FROM detalle_factura D WHERE D.id_factura=F.id) as monto  FROM ".$this->tabla." F, clientes C, estatus_pedidos E, usuarios U WHERE F.id_est=E.id AND F.tipo=0 AND F.id_usuario=U.id AND F.id_cliente=C.id AND F.estatus=0 AND F.id_cliente=$idc AND F.id_empresa='$ide' ORDER BY F.id DESC;";
      $r= $this->orm->consultaPersonalizada($sql);
      $a =[];
      while($f = $r->fetch_assoc()){
        $f["tipo_"] = $this->getEstatus($f['tipo_factura']);
        $f["estatus_"] = $this->getEstatusFactura($f['estatus']);
        $a[] = $f;
      }
      return $a;
    }

    public function obtenerPedidosACreditoByCliente($idc){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, F.tipo as tipo_factura, F.id as idf, (SELECT sum((D.can_ven * (D.pre_ven))+(D.can_ven * (D.pre_ven))*D.ivap/100) FROM detalle_factura D WHERE D.id_factura=F.id) as monto  FROM ".$this->tabla." F, clientes C, estatus_pedidos E, usuarios U WHERE F.id_est=E.id AND F.id_usuario=U.id AND F.id_cliente=C.id AND F.estatus=0 AND F.id_cliente=$idc AND F.id_empresa='$ide' ORDER BY F.id DESC;";
      $r= $this->orm->consultaPersonalizada($sql);
      $a =[];
      while($f = $r->fetch_assoc()){
        $f["tipo_"] = $this->getEstatus($f['tipo_factura']);
        $f["estatus_"] = $this->getEstatusFactura($f['estatus']);
        $f["totales"] = $this->getTotalesFactura($f['idf']);
        $a[] = $f;
      }
      return $a;
    }
    public function obtenerPedidosACredito($f1, $f2, $estatus, $id_cliente, $id_vendedor){
      $ide = $_SESSION['ide'];
      if($f1!=""){
        $sql_fecha = "F.fec_fac>='$f1 00:00:00' AND F.fec_fac<='$f2 23:59:59' AND ";
      }

      if($id_cliente!=null&&$id_cliente!=""){
        $sql_cliente = " AND F.id_cliente=$id_cliente";
      }

      if($id_vendedor!=null&&$id_vendedor!=""){
        $sql_vendedor = " AND F.id_vendedor=$id_vendedor";
      }

      if($estatus!=""||$estatus!=null)
        $txt_est = "AND F.estatus=$estatus ";
      else
        $txt_est = "AND F.estatus=0";

      $sql = "SELECT *, DATEDIFF(date_add(F.fec_fac, INTERVAL C.dias_credito DAY), NOW()) dias_vencido, F.tipo tipo_factura, F.id as idf, (SELECT sum((D.can_ven * (D.pre_ven * D.tasa_ven))+(D.can_ven * (D.pre_ven * D.tasa_ven))*D.ivap/100)/D.tasa_ven FROM detalle_factura D WHERE D.id_factura=F.id) as monto, date_add(F.fec_fac, INTERVAL C.dias_credito DAY) vencimiento  FROM ".$this->tabla." F, clientes C, estatus_pedidos E WHERE $sql_fecha F.id_est=E.id AND F.id_cliente=C.id $txt_est AND F.id_empresa='$ide'$sql_cliente$sql_vendedor ORDER BY F.id DESC;";

      $r= $this->orm->consultaPersonalizada($sql);
      $a =[];
      while($f = $r->fetch_assoc()){
        $f["tipo_"] = $this->getEstatus($f['tipo_factura']);
        $f["estatus_"] = $this->getEstatusFactura($f['estatus']);
        $f['totales'] = $this->getTotalesFactura($f['idf']);
        $a[] = $f;
      }
      return $a;
    }

    public function obtenerPedidosPorEstatus($desde, $hasta, $t){
      $ide = $_SESSION['ide'];

      if($t == "")
        $sql = "SELECT *, F.id as idf, (SELECT sum((D.can_ven * (D.pre_ven))+(D.can_ven * (D.pre_ven))*D.ivap/100) FROM detalle_factura D WHERE D.id_factura=F.id) as monto  FROM ".$this->tabla." F, clientes C, estatus_pedidos E WHERE F.id_est=E.id AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.id_cliente=C.id AND F.id_empresa='$ide' ORDER BY F.id DESC;";
      else{
        if($desde=="" || $hasta=="")
          $sql = "SELECT *, F.id as idf, (SELECT sum((D.can_ven * (D.pre_ven))+(D.can_ven * (D.pre_ven))*D.ivap/100) FROM detalle_factura D WHERE D.id_factura=F.id) as monto  FROM ".$this->tabla." F, clientes C, estatus_pedidos E WHERE F.id_est=E.id AND F.id_cliente=C.id AND F.estatus=$t AND F.id_empresa='$ide' ORDER BY F.id DESC;";
        else{
          $sql = "SELECT *, F.id as idf, (SELECT sum((D.can_ven * (D.pre_ven))+(D.can_ven * (D.pre_ven))*D.ivap/100) FROM detalle_factura D WHERE D.id_factura=F.id) as monto  FROM ".$this->tabla." F, clientes C, estatus_pedidos E WHERE F.id_est=E.id AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.id_cliente=C.id AND F.estatus=$t AND F.id_empresa='$ide' ORDER BY F.id DESC;";
        }
      }
      $r= $this->orm->consultaPersonalizada($sql);
      $a =[];
      while($f = $r->fetch_assoc()){
        $f["tipo_"] = $this->getEstatus($f['tipo']);
        $f["estatus_"] = $this->getEstatusFactura($f['estatus']);
        $f["totales"] = $this->getTotalesFactura($f['idf']);
        $a[] = $f;
      }
      return $a;
    }

    public function obtenerPedidosByUser($desde, $hasta, $idu, $est){
      $ide = $_SESSION['ide'];
      if($est !="")
        $est="AND F.tipo='$est'";

      //$sql = "SELECT *, F.tipo tipo_factura, F.id as idf, (SELECT (sum(((D.pre_ven)*D.can_ven)+((D.pre_ven)*D.can_ven)*D.ivap/100))/D.tasa_ven FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda_dolar, (SELECT sum((D.pre_ven*D.tasa_ven)*D.can_ven)/D.tasa_ven FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda, (SELECT sum((((D.pre_ven)*D.can_ven)*D.ivap/100)) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_iva, (SELECT sum(P.monto) FROM pagos P WHERE P.id_factura=F.id) as monto, (SELECT sum((P.monto/P.tasa_pago)) FROM pagos P WHERE P.id_factura=F.id) as monto_dolar FROM ".$this->tabla." F, clientes C, estatus_pedidos E, usuarios U  WHERE F.id_est=E.id AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.id_cliente=C.id AND F.id_vendedor=U.id AND F.id_usuario=$idu AND F.id_empresa='$ide' $est ORDER BY F.id DESC;";

      $sql = "SELECT *, F.tipo tipo_factura, F.id as idf,(SELECT (sum((((D.pre_ven)*D.can_ven)*D.ivap/100)/D.tasa_ven)*D.tasa_ven) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_iva, (SELECT (sum((D.pre_ven*D.can_ven)+((D.pre_ven*D.can_ven)*D.ivap/100)))/D.tasa_ven FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda_dolar, F.id as idf, (SELECT (sum(((D.pre_ven/D.tasa_ven)*(D.can_ven+D.can_dev))+((D.pre_ven/D.tasa_ven)*(D.can_ven+D.can_dev))*D.ivap/100)*D.tasa_ven) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda_original, (SELECT (sum(((D.pre_ven/D.tasa_ven)*D.can_ven)+((D.pre_ven/D.tasa_ven)*D.can_ven)*D.ivap/100)*D.tasa_ven) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda, (SELECT (sum(P.monto)) FROM pagos P WHERE P.id_factura=F.id) as monto, (SELECT (sum((P.monto/P.tasa_pago))) FROM pagos P WHERE P.id_factura=F.id) as monto_dolar FROM ".$this->tabla." F, clientes C, estatus_pedidos E, usuarios U  WHERE F.id_est=E.id AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.id_cliente=C.id AND F.id_usuario=U.id AND F.id_empresa='$ide' AND F.id_usuario=$idu $est ORDER BY F.id DESC;";
      $r= $this->orm->consultaPersonalizada($sql);
      $a =[];
      while($f = $r->fetch_assoc()){
        $f["tipo"] = $this->getEstatus($f['tipo_factura']);
        $f["estatus_"] = $this->getEstatusFactura($f['estatus']);
        $a[] = $f;
      }
      return $a;
    }

    public function obtenerPedidosMisPendientes(){
      $ide = $_SESSION['ide'];
      $idu = $_SESSION['idu'];
      $sql = "SELECT *, F.tipo tipo_factura, F.id as idf, (SELECT sum((D.pre_ven*D.tasa_ven)*D.can_ven) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda, (SELECT sum(P.monto) FROM pagos P WHERE P.id_factura=F.id) as monto, (SELECT sum((P.monto/P.tasa_pago)) FROM pagos P WHERE P.id_factura=F.id) as monto_dolar FROM ".$this->tabla." F, clientes C, estatus_pedidos E, usuarios U  WHERE F.id_est=E.id AND F.tipo=-1 AND  F.id_cliente=C.id AND F.id_usuario=U.id AND F.id_usuario=$idu AND F.id_empresa='$ide' ORDER BY F.id DESC;";
      $r= $this->orm->consultaPersonalizada($sql);
      $a =[];
      while($f = $r->fetch_assoc()){
        $f["tipo"] = $this->getEstatus($f['tipo_factura']);
        $a[] = $f;
      }
      return $a;
    }

    public function obtenerPedidosPendientes(){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, F.tipo tipo_factura, F.id as idf, (SELECT sum((D.pre_ven*D.tasa_ven)*D.can_ven) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda, (SELECT sum(P.monto) FROM pagos P WHERE P.id_factura=F.id) as monto, (SELECT sum((P.monto/P.tasa_pago)) FROM pagos P WHERE P.id_factura=F.id) as monto_dolar FROM ".$this->tabla." F, clientes C, estatus_pedidos E, usuarios U  WHERE F.id_est=E.id AND F.tipo=-1 AND  F.id_cliente=C.id AND F.id_usuario=U.id AND F.id_empresa='$ide' ORDER BY F.id DESC;";
      $r= $this->orm->consultaPersonalizada($sql);
      $a =[];
      while($f = $r->fetch_assoc()){
        $f["tipo"] = $this->getEstatus($f['tipo_factura']);
        $a[] = $f;
      }
      return $a;
    }

    public function obtenerPedidosByCliente($desde, $hasta,$idc){
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, F.tipo tipo_factura, F.id as idf, (SELECT sum(((D.pre_ven)*D.can_ven)+((D.pre_ven)*D.can_ven)*D.ivap/100)/D.tasa_ven FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda_dolar, F.id as idf, (SELECT sum(((D.pre_ven)*D.can_ven)+((D.pre_ven)*D.can_ven)*D.ivap/100) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda, (SELECT sum(P.monto) FROM pagos P WHERE P.id_factura=F.id) as monto, (SELECT sum((P.monto/P.tasa_pago)) FROM pagos P WHERE P.id_factura=F.id) as monto_dolar FROM ".$this->tabla." F, clientes C, estatus_pedidos E, usuarios U  WHERE F.id_est=E.id AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.id_cliente=C.id AND F.id_usuario=U.id AND F.id_empresa='$ide' AND F.id_cliente='$idc' ORDER BY F.id DESC;";
      $r= $this->orm->consultaPersonalizada($sql);
      $a =[];
      while($f = $r->fetch_assoc()){
        $f["tipo"] = $this->getEstatus($f['tipo_factura']);
        $a[] = $f;
      }
      return $a;
    }

    public function obtenerPedidosComision($desde, $hasta, $idv, $est){
      if($idv!=null)
        $usuario = " AND F.id_vendedor=$idv";
      if($est!=null)
        $estatus = " AND F.tipo=$est";

      $ide = $_SESSION['ide'];
      $sql = "SELECT *, F.id_vendedor as vendedor, F.tipo tipo_factura, F.id as idf,(SELECT sum((((D.pre_ven)*D.can_ven)*D.ivap/100)*D.tasa_ven) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_iva, (SELECT sum(((D.pre_ven)*D.can_ven)+((D.pre_ven)*D.can_ven)*D.ivap/100)/D.tasa_ven FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda_dolar, F.id as idf, (SELECT sum(((D.pre_ven*D.tasa_ven)*D.can_ven)+((D.pre_ven*D.tasa_ven)*D.can_ven)*D.ivap/100)/D.tasa_ven FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda, (SELECT sum(P.monto) FROM pagos P WHERE P.id_factura=F.id) as monto, (SELECT sum((P.monto/P.tasa_pago)) FROM pagos P WHERE P.id_factura=F.id) as monto_dolar FROM ".$this->tabla." F, clientes C, estatus_pedidos E, usuarios U  WHERE F.id_est=E.id AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.id_cliente=C.id AND F.id_vendedor=U.id AND F.id_empresa='$ide' $usuario $estatus ORDER BY F.id DESC;";
      $r= $this->orm->consultaPersonalizada($sql);
      $a =[];
      while($f = $r->fetch_assoc()){
        $f["tipo"] = $this->getEstatus($f['tipo_factura']);
        $a[] = $f;
      }
      return $a;
    }

    public function obtenerUtilidadesVentas($f1, $f2, $ida, $cod, $cli){
      if($ida!=""||$ida!=null)
        $sqla = "AND F.id_almacen=$ida ";

      if($cod!=""||$cod!=null)
        $sqlc="AND I.cod_inv='$cod' ";

      if($cli!=""||$cli!=null)
        $sqlcl = "AND F.id_cliente='$cli'";

      $sql = "SELECT D.*, F.id idf, DE.cos_pro pre_inv, F.cod_fac, F.fec_fac, I.cod_inv, F.descuento FROM detalle_factura D, facturas F, inventario I, detalle_entradas_salidas DE, entradas_salidas E WHERE E.id_factura=F.id AND DE.id_es=E.id AND DE.id_producto=D.id_inventario AND D.id_inventario=I.id AND D.id_factura=F.id AND F.fec_fac>='$f1 00:00:00' AND F.fec_fac<='$f2 23:59:59' AND F.tipo=1 $sqla $sqlc $sqlcl;";
      $r= $this->orm->consultaPersonalizada($sql);
      $a =[];
      while($f = $r->fetch_assoc()){
        $a[] = $f;
      }
      return $a;
    }

    public function obtenerPedidos($desde, $hasta, $idu, $est){
      if($idu!=null)
        $usuario = " AND F.id_usuario=$idu";
      if($est!=null)
        $estatus = " AND F.tipo=$est";
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, F.tipo tipo_factura, F.id as idf,(SELECT (sum((((D.pre_ven)*D.can_ven)*D.ivap/100)/D.tasa_ven)*D.tasa_ven) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_iva, (SELECT (sum((D.pre_ven*D.can_ven)+((D.pre_ven*D.can_ven)*D.ivap/100)))/D.tasa_ven FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda_dolar, F.id as idf, (SELECT (sum(((D.pre_ven/D.tasa_ven)*(D.can_ven+D.can_dev))+((D.pre_ven/D.tasa_ven)*(D.can_ven+D.can_dev))*D.ivap/100)*D.tasa_ven) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda_original, (SELECT (sum(((D.pre_ven/D.tasa_ven)*D.can_ven)+((D.pre_ven/D.tasa_ven)*D.can_ven)*D.ivap/100)*D.tasa_ven) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda, (SELECT (sum(P.monto)) FROM pagos P WHERE P.id_factura=F.id) as monto, (SELECT (sum((P.monto/P.tasa_pago))) FROM pagos P WHERE P.id_factura=F.id) as monto_dolar FROM ".$this->tabla." F, clientes C, estatus_pedidos E, usuarios U  WHERE F.id_est=E.id AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.id_cliente=C.id AND F.id_usuario=U.id AND F.id_empresa='$ide' $usuario $estatus ORDER BY F.id DESC;";
      $r= $this->orm->consultaPersonalizada($sql);
      $a =[];
      while($f = $r->fetch_assoc()){
        $f["tipo"] = $this->getEstatus($f['tipo_factura']);
        $a[] = $f;
      }
      return $a;
    }

    public function getTotalesFactura($idf){
      $sql = "SELECT F.descuento, (SELECT (sum(P.monto/P.tasa_pago)) FROM pagos P WHERE P.id_factura=$idf) as abono_dolar,(SELECT (sum(P.monto)) FROM pagos P WHERE P.id_factura=$idf) as abono_bolivar, sum(D.tasa_ven)/count(*) tasa_dolar, (SELECT (sum(((D.pre_ven)*D.can_ven)+((D.pre_ven)*D.can_ven)*D.ivap/100))/D.tasa_ven FROM detalle_factura D WHERE D.id_factura=$idf) as total_dolar,(SELECT (sum(((D.pre_ven)*D.can_ven)+((D.pre_ven)*D.can_ven)*D.ivap/100)) FROM detalle_factura D WHERE D.id_factura=$idf) as total_bolivar, (SELECT (sum((((D.pre_ven)*D.can_ven)*D.ivap/100)/D.tasa_ven)) FROM detalle_factura D WHERE D.id_factura=$idf) as iva_dolar, (SELECT (sum((((D.pre_ven)*D.can_ven)*D.ivap/100)/D.tasa_ven)*D.tasa_ven) FROM detalle_factura D WHERE D.id_factura=$idf) as iva_bolivar FROM detalle_factura D, facturas F WHERE D.id_factura=F.id AND D.id_factura=$idf;";
      $r= $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        $f = $r->fetch_assoc();

        if($f['descuento']>0){
          $f['total_dolar'] = round($f['total_dolar'] - (($f['total_dolar'] * $f['descuento'])/100), 2);
          $f['total_bolivar'] = round($f['total_bolivar'] - (($f['total_bolivar'] * $f['descuento'])/100), 2);
        }

        $f['pendiente_dolar']=  round($f['total_dolar']-$f['abono_dolar'], 2);
        $f['pendiente_bolivar']=round($f['total_bolivar']-$f['abono_bolivar'], 2);
        return $f;
      }
      else 
        return false;
    }

    public function save(){
 
      $n = count($this->data);
      $sql = "INSERT INTO ".$this->tabla ." (";
      $j=0;

      foreach($this->data as $key => $index){
        $j++;
        $sql.= "$key";
        if($j < $n){
          $sql.= ",";
        }
      }

      $sql.= ") VALUES(";
      $i = 0;

      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

    public function savePagos($id, $pagos, $id_usuario, $id_documento, $tipo_documento){
      $ide = $_SESSION['ide'];
      $n = 0;
      for($i = 0; $i < count($pagos); $i++){
        $pago = new Pago();
        $obj = $pagos[$i];


        $pago->data['id_pago'] = '';
        $pago->data['id_factura']=$id;
        $pago->data['monto']=$obj['pago'];
        $pago->data['tasa_pago']=$obj['dolar'];

        if(isset($obj['fecha'])&&$obj['fecha']!=null)
          $pago->data['fecha']=$obj['fecha'];
        else
          $pago->data['fecha']=date('Y-m-d H:i:s');

        $pago->data['id_usuario']=$id_usuario;
        $pago->data['id_moviento_cuenta']='';
        $pago->data['id_empresa']=$ide;

        if($obj['tipo']=='vuelto')
          $pago->data['nota_pago']="CAMBIO DEVUELTO";
        else
          $pago->data['nota_pago']=$obj['nota'];

        $pago->data['id_cuenta'] =$obj['cuenta'];
        $pago->data['id_metodo'] =$obj['metodo'];
        $pago->data['referencia']=$obj['ref'];

        if($id_documento==null)
          $id_documento = '';
        if($tipo_documento==null)
          $tipo_documento = '';

        $pago->data['id_documento']=$id_documento;
        $pago->data['id_tipo_documento']=$tipo_documento;
        $pago->save();

        $cuenta = new Micuenta();
        $cue = $cuenta->findById($obj['cuenta']);
        $cuenta->data['saldo_segun_sistema']=$cue['saldo_segun_sistema']+$obj['pago'];
        $cuenta->edit($obj['cuenta']);
        $n++;
      }
      return $n;
    }

    public function saveDetailsConvertir($id_nota, $id_factura){
      $nota = new NotaEntrega();
      $productos = $nota->fetchDetalles2($id_nota);
      $n = 0;

      while($obj = $productos->fetch_assoc()){
        if($obj['id_tipo_precio']==null)
          $obj['id_tipo_precio'] = 'null';
        $sql = "INSERT INTO detalle_factura VALUES(null, $id_factura, '".$obj['id_inventario']."', '".$obj['nom_pro']."', '".$obj['pre_ven']."', '". $obj['can_ven'] ."', '".$obj['tasa_ven'] ."', '".$obj['id_iva']."', '".$obj['ivap']."', '".$obj['can_des']."', 0, null, ".$obj['id_tipo_precio'].");";

        $r = $this->orm->insertarPersonalizado($sql);
        if($r)
          $n++;
      }
      return $n;
    }

    public function saveDetails($id, $prods, $id_salida, $metodo, $pos){
      $n = 0;

      for($i = 0; $i < count($prods); $i++){
        $obj = $prods[$i];
        $inv = new Inventario();
        $monto_pro = 0;
        $cos_pro =$obj['precio_bs']/$obj['dolar'];
        $id_pre = 'null';
        $fact = 'null';
        $fac = $this->findById($id);

        // SI LA FACTURA AUN ES PEDIDO, EL DESPACHO DEBE SER 0
        if($fac['tipo_factura'] == -1){
          $obj['can_des']=0;
          $SALIDA=false;
        }else if($fac['tipo_factura']>=0 && $fac['despacho']==1){
          $obj['can_des']=$obj['can'];
          $SALIDA=true;
        }else if($fac['tipo_factura']>=0 && $fac['despacho']==0){
          $obj['can_des']=0;
          $SALIDA=false;
        }

        if($obj['tipo_precio']=="")
          $obj['tipo_precio'] = 'null';

        if($obj['idp']=='detalle'){
          $rinv['nom_inv'] = $obj['nombre_detalle'];
          $obj['idp'] = 'null';
          $monto_pro = $obj['precio_bs'];
          $SALIDA=false;
        }else{
          $rinv = $inv->findById($obj['idp']);
        }

        $rinv['nom_inv'] = str_replace("'", "\'", $rinv['nom_inv']);
        $rinv['nom_inv'] = str_replace('"', "\'", $rinv['nom_inv']);

        if(isset($rinv['disponible'])&&$rinv['disponible']<=0){
          continue;
        }

        //NUEVO ARREGLAR BUG DE PRECIOS Y DESCUENTO
        if($obj['idp']!='detalle'){
          $pro = $inv->findById($obj['idp']);
          $ivasumar = 0;
          $monto_pro = 0;
          if($pro['ivap']){
            $ivasumar = ($pro['pre_ven_inv'] * $pro['ivap'])/100;
          }
          //$monto_pro = ($pro['pre_ven_inv']+$ivasumar)*$obj['dolar'];
          $monto_pro = ($pro['pre_ven_inv'])*$obj['dolar'];
          $cos_pro =$pro['pre_ven_inv'];
          $id_pre = $pro['id_presentacion'];
          $fact = $pro['can_presentacion_inv'];
          if($pro['descuento_web'] && $pro['descuento_web']>0){
            $descuen = ($monto_pro * $pro['descuento_web']) / 100;
            $monto_pro = $monto_pro - $descuen;
          }
        }
        
        $sql = "INSERT INTO detalle_factura (`id`, `id_factura`, `id_inventario`, `nom_pro`, `pre_ven`, `can_ven`, `tasa_ven`, `id_iva`, `ivap`, `can_des`, `can_dev`, `id_concepto_dev`, `id_tipo_precio`, `cos_inv`, `id_presentacion`, `factor`) VALUES(null, $id, ".$obj['idp'].", '".$rinv['nom_inv']."', '".$monto_pro."', '". $obj['can'] ."', '".$obj['dolar'] ."', '".$obj['id_iva']."', '".$obj['ivap']."', '".$obj['can_des']."', 0, null, ".$obj['tipo_precio'].", ".$cos_pro.", ".$id_pre.", ".$fact.");";
        // FIN DE LO NUEVO DEL BUG
        //$sql = "INSERT INTO detalle_factura VALUES(null, $id, ".$obj['idp'].", '".$rinv['nom_inv']."', '".$obj['precio_bs']."', '". $obj['can'] ."', '".$obj['dolar'] ."', '".$obj['id_iva']."', '".$obj['ivap']."', '".$obj['can_des']."', 0, null, ".$obj['tipo_precio'].");";
        
        $r = $this->orm->insertarPersonalizado($sql);

        if($r!=false){
          $n++;
          /*if($rinv['lotes']==1){
            $lote = new Lote();
            $lotes = $obj['lotes'];
            for($j=0;$j<count($lotes);$j++){
              $lot = $lotes[$j];
              $L = $lote->findById($lot['id']);
              $lote->dataDetalle = [];
              $lote->dataDetalle['id']='';
              $lote->dataDetalle['id_lote']=$lot['id'];
              $lote->dataDetalle['id_producto']=$obj['idp'];
              $lote->dataDetalle['id_detalle_factura']=$r->insert_id;
              $lote->dataDetalle['can_lote']=($lot['can']*-1);
              $lote->saveDetalles();
            }
          }*/
        }
        
        if($SALIDA){
          if($r != false){
            if($rinv['compuesto']==0){
              $saldo = $rinv['can_inv'] - $obj['can'];
              $can__=($obj['can']* -1);
              $almacen = new Almacen();
              $salida =new Salida();
              $salida->dataSalida = [];
              $salida->dataSalida['id'] = '';
              $salida->dataSalida['id_es'] = $id_salida;
              $salida->dataSalida['id_producto']= $obj['idp'];
              $salida->dataSalida['operacion']='0';
              $salida->dataSalida['nom_pro']=$rinv['nom_inv'];
              $salida->dataSalida['can_pro']=$can__;
              $salida->dataSalida['cos_pro']=$rinv['pre_inv'];
              $salida->dataSalida['cos_pro_bs']=($rinv['pre_inv']*$obj['dolar']);
              $salida->dataSalida['tasa_cam']=$obj['dolar'];
              $salida->dataSalida['id_iva']='';
              $salida->dataSalida['ivap']='';
              $salida->dataSalida['can_dev']='0';
              $salida->dataSalida['id_concepto_dev']='';
              $salida->dataSalida['id_almacen']=$pos['id_almacen'];
              $salida->dataSalida['id_ubicacion']='';
              $salida->dataSalida['saldo']=$saldo."";
              $salida->dataSalida['saldo_almacen']=($almacen->getStockAlmacen($pos['id_almacen'], $obj['idp']) - $obj['can'])."";
              if($salida->dataSalida['saldo_almacen']<0){
                continue;
              }

              $r = $salida->saveSalida();
              if($r!=false){
                // SI EL PRODUCTO NO ES COMPUESTO, DESCONTAMOS DE UNA
                $inv->data['can_inv'] = $rinv['can_inv']-$obj['can'];
                $inv->edit($obj['idp']);

                if($rinv['lotes']==1){
                  $lote = new Lote();
                  $lotes = $obj['lotes'];
                  for($j=0;$j<count($lotes);$j++){
                    $lot = $lotes[$j];
                    $L = $lote->findById($lot['id']);
                    $lote->data['stock_lote']=floatval($L['stock_lote'])-floatval($lot['can']);
                    $lote->edit($lot['id']);
                  }
                }


              }
            }else if($rinv['compuesto']==1){
              $ingredientes = $inv->getIngredientesById($obj['idp']);
              for($j=0;$j<count($ingredientes);$j++){

                $invx = new Inventario();
                $almacen = new Almacen();
                $pp = $ingredientes[$j];
                $pp['nom_inv'] = str_replace("'", "\'", $pp['nom_inv']);
                $pp['nom_inv'] = str_replace('"', "\'", $pp['nom_inv']);

                $saldo = $pp['can_inv'] - $pp['can_el'];
                $can__=(($pp['can_el'] * $obj['can']) * -1);

                $almacen = new Almacen();
                $salida =new Salida();
                $salida->dataSalida = [];
                $salida->dataSalida['id'] = '';
                $salida->dataSalida['id_es'] = $id_salida;
                $salida->dataSalida['id_producto']= $pp['id_elemento'];
                $salida->dataSalida['operacion']='0';
                $salida->dataSalida['nom_pro']=$pp['nom_inv'];
                $salida->dataSalida['can_pro']=$can__;
                $salida->dataSalida['cos_pro']='';
                $salida->dataSalida['cos_pro_bs']='';
                $salida->dataSalida['tasa_cam']='';
                $salida->dataSalida['id_iva']='';
                $salida->dataSalida['ivap']='';
                $salida->dataSalida['can_dev']='0';
                $salida->dataSalida['id_concepto_dev']='';
                $salida->dataSalida['id_almacen']=$pos['id_almacen'];
                $salida->dataSalida['id_ubicacion']='';
                $salida->dataSalida['saldo']=$saldo."";
                $salida->dataSalida['saldo_almacen']= ($almacen->getStockAlmacen($pos['id_almacen'], $pp['id_elemento']) - $can__)."";
                if($salida->dataSalida['saldo_almacen']<0){
                  continue;
                }
                $r = $salida->saveSalida();
                if($r){
                  $invx->data['can_inv'] = $pp['can_inv']-($pp['can_el']*$obj['can']);
                  $invx->edit($pp['id_elemento']);
                }
              }
            }
          }
        }
      }
      return $n;
    }

    public function pedidosPendientes(){
      $ide = $_SESSION['ide'];
      $fec = date('Y-m-d');
      $sql = "SELECT *, F.id as idp FROM facturas F, estatus_pedidos E WHERE F.id_est=E.id AND (F.id_est=1 OR F.id_est=2) AND F.id_empresa='$ide' AND F.fec_fac like '$fec %' ORDER BY F.id limit 3;";
      $r = $this->orm->consultaPersonalizada($sql);
      $a=[];
      while($f = $r->fetch_assoc()){
        $cli = new Cliente();
        $idf = $f["idp"];
        $f["productos"]= $this->fetchDetalles($idf);
        $idc = $f["id_cliente"];
        $f["cliente"]= $cli->findById($idc);
        $a[] = $f;
      }
      return $a;
    }

    public function ultimosEntregados(){
      $ide =$_SESSION['ide'];
      $sql = "SELECT *, F.id as idp FROM facturas F, estatus_pedidos E WHERE F.id_est=E.id AND F.id_est=3 AND F.id_empresa='$ide' ORDER BY F.id DESC limit 3;";
      $r = $this->orm->consultaPersonalizada($sql);
      $a=[];
      while($f = $r->fetch_assoc()){
        $cli = new Cliente();
        $idf = $f["idp"];
        $f["productos"]= $this->fetchDetalles($idf);
        $idc = $f["id_cliente"];
        $f["cliente"]= $cli->findById($idc);
        $a[] = $f;
      }
      return $a;
    }

    public function fetchPendientes(){
      $sql = "SELECT *, P.id as idp, (SELECT sum(D.can_pro*D.pre_pro) FROM detalles_pedido D WHERE D.id_pedido=P.id) as monto  FROM " . $this->tabla ." P, clientes C WHERE P.id_cliente=C.id AND P.visto=0;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function anodelafacturamasantigua(){
      $ide = $_SESSION['ide'];
      $sql = "SELECT DATE_FORMAT(fec_fac, '%Y') AS fec_fac FROM facturas WHERE  id_empresa='$ide' AND (tipo=1 OR tipo=0) AND fec_fac <> '0000-00-00 00:00:00' ORDER BY fec_fac ASC limit 1;";
      $r = $this->orm->consultaPersonalizada($sql);
      $a= date('Y');
      if($f = $r->fetch_assoc()){
        $a = $f["fec_fac"];
      }
      return $a;
    }

    public function obtenerPedidosEstadisticas($desde, $hasta, $est, $cat){
      if($est!=null)
        $estatus = " AND F.tipo=$est";

      $cat_wer ='';
      $cat_fro ='';
      if($cat!=null){
        $cat_wer =' AND D.id_inventario=I.id AND I.id_categoria='.$cat;
        $cat_fro =', inventario I';
      }

      $ide = $_SESSION['ide'];
      $sql = "SELECT *, F.tipo tipo_factura, F.id as idf,(SELECT (sum((((D.pre_ven)*D.can_ven)*D.ivap/100)/D.tasa_ven)*D.tasa_ven) FROM detalle_factura D $cat_fro WHERE D.id_factura=F.id $cat_wer) as monto_iva, (SELECT (sum((D.pre_ven*D.can_ven)+((D.pre_ven*D.can_ven)*D.ivap/100)))/D.tasa_ven FROM detalle_factura D $cat_fro WHERE D.id_factura=F.id $cat_wer) as monto_deuda_dolar, F.id as idf, (SELECT (sum(((D.pre_ven/D.tasa_ven)*(D.can_ven+D.can_dev))+((D.pre_ven/D.tasa_ven)*(D.can_ven+D.can_dev))*D.ivap/100)*D.tasa_ven) FROM detalle_factura D $cat_fro WHERE D.id_factura=F.id $cat_wer) as monto_deuda_original, (SELECT (sum(((D.pre_ven/D.tasa_ven)*D.can_ven)+((D.pre_ven/D.tasa_ven)*D.can_ven)*D.ivap/100)*D.tasa_ven) FROM detalle_factura D $cat_fro WHERE D.id_factura=F.id $cat_wer) as monto_deuda, (SELECT (sum(P.monto)) FROM pagos P WHERE P.id_factura=F.id) as monto, (SELECT (sum((P.monto/P.tasa_pago))) FROM pagos P WHERE P.id_factura=F.id) as monto_dolar FROM ".$this->tabla." F, clientes C, estatus_pedidos E, usuarios U  WHERE F.id_est=E.id AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.id_cliente=C.id AND F.id_usuario=U.id AND F.id_empresa='$ide' $estatus ORDER BY F.id DESC;";
      $r= $this->orm->consultaPersonalizada($sql);
      $a = 0;

      $total = 0;
      $total_dolar = 0;
      $total_descuento = 0;
      $total_descuento_dl = 0;

      while($f = $r->fetch_assoc()){
        $fila = $f;
        if($fila['estatus'] == 1){
          if($fila['monto'])
            $total = $total + $fila['monto'];
          else
            $total = $total + $fila['monto_deuda'];
          $total_dolar = $total_dolar + $fila['monto_deuda_dolar'];
          $diff = $fila['monto_dolar']-$fila['monto_deuda_dolar'];
        }else{
          $fila['monto'] = $fila['monto_deuda'];
          $fila['monto_dolar'] = $fila['monto_deuda_dolar'];
          $total_dolar = $total_dolar + $fila['monto_deuda_dolar'];
          $total = $total + $fila['monto_deuda'];
        }

        if($fila['descuento']>0){
          $total_descuento = $total_descuento + (($fila['monto_deuda_original']*$fila['descuento'])/100);
          $total_descuento_dl = $total_descuento_dl + ((($fila['monto_deuda_dolar']*$fila['descuento'])/100));
        }
      }
      $a = $total_dolar-$total_descuento_dl;
      return $a;
    }

    public function obtenerPedidosEstadisticas2($desde, $hasta, $est){
      if($est!=null)
        $estatus = " AND F.tipo=$est";

      $ide = $_SESSION['ide'];
      $sql = "SELECT *, F.tipo tipo_factura, F.id as idf,(SELECT sum(D.cos_pro*D.can_pro) FROM entradas_salidas E, detalle_entradas_salidas D WHERE D.id_es=E.id AND E.id_factura=F.id ) as monto_costo, (SELECT (sum((((D.pre_ven-((D.pre_ven*F.descuento)/100))*D.can_ven)*D.ivap/100)/D.tasa_ven)) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_iva, (SELECT (sum((D.pre_ven*D.can_ven)+((D.pre_ven*D.can_ven)*D.ivap/100)))/D.tasa_ven FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda_dolar, F.id as idf, (SELECT (sum(((D.pre_ven/D.tasa_ven)*(D.can_ven+D.can_dev))+((D.pre_ven/D.tasa_ven)*(D.can_ven+D.can_dev))*D.ivap/100)*D.tasa_ven) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda_original, (SELECT (sum(((D.pre_ven/D.tasa_ven)*D.can_ven)+((D.pre_ven/D.tasa_ven)*D.can_ven)*D.ivap/100)*D.tasa_ven) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda, (SELECT (sum(P.monto)) FROM pagos P WHERE P.id_factura=F.id) as monto, (SELECT (sum((P.monto/P.tasa_pago))) FROM pagos P WHERE P.id_factura=F.id) as monto_dolar FROM ".$this->tabla." F, clientes C, estatus_pedidos E, usuarios U  WHERE F.id_est=E.id AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.id_cliente=C.id AND F.id_usuario=U.id AND F.id_empresa='$ide' $estatus ORDER BY F.id DESC;";
      $r= $this->orm->consultaPersonalizada($sql);
      $a = [];
      $a['iva'] = 0;
      $a['costo'] = 0;
      $a['ganancia'] = 0;

      $total = 0;
      $total_dolar = 0;
      $total_descuento = 0;
      $total_descuento_dl = 0;
      $total_iva = 0;
      $total_costo = 0;
      $total_ganancia = 0;

      while($f = $r->fetch_assoc()){
        $fila = $f;
        if($fila['estatus'] == 1){
          if($fila['monto'])
            $total = $total + $fila['monto'];
          else
            $total = $total + $fila['monto_deuda'];
          $total_dolar = $total_dolar + $fila['monto_deuda_dolar'];
          $diff = $fila['monto_dolar']-$fila['monto_deuda_dolar'];
        }else{
          $fila['monto'] = $fila['monto_deuda'];
          $fila['monto_dolar'] = $fila['monto_deuda_dolar'];
          $total_dolar = $total_dolar + $fila['monto_deuda_dolar'];
          $total = $total + $fila['monto_deuda'];
        }

        if($fila['descuento']>0){
          $total_descuento = $total_descuento + (($fila['monto_deuda_original']*$fila['descuento'])/100);
          $total_descuento_dl = $total_descuento_dl + ((($fila['monto_deuda_dolar']*$fila['descuento'])/100));
        }

        $total_iva = $total_iva + $fila['monto_iva'];
        $total_costo = $total_costo + ($fila['monto_costo']*-1);

      }
      //$a = $total_dolar-$total_descuento_dl;
      $a['iva'] = $total_iva;
      $a['costo'] = $total_costo;
      $a['ganancia'] = (($total_dolar-$total_descuento_dl)-$total_iva)-$total_costo;
      return $a;
    }

    public function obtenerPedidosbyalmacen($desde, $hasta, $idu, $est, $ida){
      if($idu!=null)
        $usuario = " AND F.id_usuario=$idu";
      if($est!=null)
        $estatus = " AND F.tipo=$est";
      $almacen = '';
      if($ida!=null)
        $almacen = " AND F.id_almacen=$ida";
      $ide = $_SESSION['ide'];
      $sql = "SELECT *, F.tipo tipo_factura, F.id as idf,(SELECT (sum((((D.pre_ven)*D.can_ven)*D.ivap/100)/D.tasa_ven)*D.tasa_ven) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_iva, (SELECT (sum((D.pre_ven*D.can_ven)+((D.pre_ven*D.can_ven)*D.ivap/100)))/D.tasa_ven FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda_dolar, F.id as idf, (SELECT (sum(((D.pre_ven/D.tasa_ven)*(D.can_ven+D.can_dev))+((D.pre_ven/D.tasa_ven)*(D.can_ven+D.can_dev))*D.ivap/100)*D.tasa_ven) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda_original, (SELECT (sum(((D.pre_ven/D.tasa_ven)*D.can_ven)+((D.pre_ven/D.tasa_ven)*D.can_ven)*D.ivap/100)*D.tasa_ven) FROM detalle_factura D WHERE D.id_factura=F.id) as monto_deuda, (SELECT (sum(P.monto)) FROM pagos P WHERE P.id_factura=F.id) as monto, (SELECT (sum((P.monto/P.tasa_pago))) FROM pagos P WHERE P.id_factura=F.id) as monto_dolar FROM ".$this->tabla." F, clientes C, estatus_pedidos E, usuarios U  WHERE F.id_est=E.id AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.id_cliente=C.id AND F.id_usuario=U.id AND F.id_empresa='$ide' $usuario $estatus $almacen ORDER BY F.id DESC;";
      $r= $this->orm->consultaPersonalizada($sql);
      $a =[];
      while($f = $r->fetch_assoc()){
        $f["tipo"] = $this->getEstatus($f['tipo_factura']);
        $a[] = $f;
      }
      return $a;
    }

    public function obtenerPedidosPorMetodobyalmacen($desde, $hasta, $idu, $est, $ida){
      if($idu!=null){
        $usuario = " AND P.id_usuario=$idu";
        $usuario2 = " AND PC.id_usuario=$idu ";
      }

      if($est!=null){
        $estatus = " AND F.tipo=$est";
        $estatus2 = " AND FC.tipo=$est";
      }

      $almacen = '';$almacen2 = '';
      if($ida!=null){
        $almacen = " AND F.id_almacen=$ida";
        $almacen2 = " AND FC.id_almacen=$ida";
      }
      $ide = $_SESSION['ide'];
      $sql = "SELECT M.id id_metodo, C.id id_cuenta, M.nombre nom_met, C.nombre nom_cue, (sum(P.monto)) monto, (sum(P.monto/P.tasa_pago)) monto_dolar, (SELECT count(*) from pagos PC, facturas FC where PC.id_factura=FC.id AND PC.monto>0 AND PC.id_metodo=P.id_metodo AND PC.fecha>='$desde 00:00:00' AND PC.fecha<='$hasta 23:59:59'$usuario2 $estatus2 $almacen2) n_metodo_e, (SELECT count(*) from pagos PC, facturas FC where PC.id_factura=FC.id AND PC.monto<0 AND PC.id_metodo=P.id_metodo AND PC.fecha>='$desde 00:00:00' AND PC.fecha<='$hasta 23:59:59'$usuario2 $estatus2 $almacen2) n_metodo_s FROM pagos P, mis_cuentas C, metodos_pagos M, facturas F WHERE P.id_factura=F.id AND F.tipo<>-5 AND P.id_cuenta=C.id AND P.id_metodo=M.id AND P.id_empresa='$ide' AND P.fecha>='$desde 00:00:00' AND P.fecha<='$hasta 23:59:59' $usuario $estatus $almacen GROUP BY P.id_metodo ORDER BY C.nombre;";
      $a =[];
      $r= $this->orm->consultaPersonalizada($sql);
      while($f = $r->fetch_assoc()){
        $a[]=$f;
      }
      return $a;
    }

    public function obtenerPedidosPorCuentabyalmacen($desde, $hasta, $idu, $est, $ida){
      if($idu!=null){
        $usuario = " AND P.id_usuario=$idu";
        $usuario2= " AND PC.id_usuario=$idu ";
      }

      if($est!=null){
        $estatus = " AND F.tipo=$est";
        $estatus2 = " AND FC.tipo=$est";

      }
      $almacen = '';$almacen2 = '';
      if($ida!=null){
        $almacen = " AND F.id_almacen=$ida";
        $almacen2 = " AND FC.id_almacen=$ida";
      }
      $ide = $_SESSION['ide'];

      $sql = "SELECT C.id id_cuenta, C.nombre nom_cue, (sum(P.monto)) monto, (sum(P.monto/P.tasa_pago)) monto_dolar, (SELECT count(*) from pagos PC, facturas FC where PC.id_factura=FC.id AND PC.monto>0 AND PC.id_cuenta=P.id_cuenta AND PC.fecha>='$desde 00:00:00' AND PC.fecha<='$hasta 23:59:59'$usuario2 $estatus2 $almacen2) n_metodo_e, (SELECT count(*) from pagos PC, facturas FC where PC.id_factura=FC.id AND PC.monto<0 AND PC.id_cuenta=P.id_cuenta AND PC.fecha>='$desde 00:00:00' AND PC.fecha<='$hasta 23:59:59'$usuario2 $estatus2 $almacen2) n_metodo_s FROM pagos P, mis_cuentas C, facturas F WHERE P.id_factura=F.id AND F.tipo<>-5 AND P.id_cuenta=C.id AND P.id_empresa='$ide' AND P.fecha>='$desde 00:00:00' AND P.fecha<='$hasta 23:59:59' $usuario $estatus $almacen GROUP BY P.id_cuenta ORDER BY C.nombre;";
      $a =[];
      $r= $this->orm->consultaPersonalizada($sql);
      while($f = $r->fetch_assoc()){
        $a[]=$f;
      }
      return $a;
    }

    public function obtenerSalidasByLineasbyalmacen($desde, $hasta, $idu, $est, $ida){
      $ide = $_SESSION['ide'];
      if($est !=null)
        $est="AND F.tipo='$est'";

      if($idu!=null)
        $usu = "AND F.id_usuario=$idu";
      $almacen = '';
      if($ida!=null)
        $almacen = " AND F.id_almacen=$ida";

      $sql = "select sum(D.pre_ven) total_venta_bs, sum(D.pre_ven/D.tasa_ven) total_venta_dl, sum(D.can_ven) can_ven, L.linea from detalle_factura D, facturas F, inventario I, lineas_inventario L WHERE F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59' AND D.id_factura=F.id AND D.id_inventario=I.id AND I.id_linea=L.id AND F.id_empresa=$ide $est $usu $almacen GROUP BY I.id_linea, L.linea;";
      $a =[];
      $r= $this->orm->consultaPersonalizada($sql);
      while($f = $r->fetch_assoc()){
        $a[]=$f;
      }
      return $a;
    }

    public function obtenerSalidasDeProductosByalmacen($desde, $hasta, $idu, $est, $ida){
      $ide = $_SESSION['ide'];
      if($est !=null)
        $est="AND F.tipo='$est'";
      if($idu!=null)
        $usu = "AND F.id_usuario=$idu";
      $almacen = '';
      if($ida!=null)
        $almacen = " AND F.id_almacen=$ida";

      $sql = "SELECT D.id_producto, I.cod_inv, D.nom_pro, sum(D.can_pro) cantidad, E.fec_fac, L.linea FROM detalle_entradas_salidas D, entradas_salidas E, facturas F, inventario I, lineas_inventario L WHERE I.id_linea=L.id AND E.id_factura=F.id AND D.id_producto=I.id AND D.id_es=E.id AND E.fec_fac>='$desde 00:00:00' AND E.fec_fac<='$hasta 23:59:59' AND E.id_empresa='$ide' $est $usu $almacen GROUP BY D.id_producto ORDER BY cantidad ASC;";
      $a =[];
      $r= $this->orm->consultaPersonalizada($sql);
      while($f = $r->fetch_assoc()){
        $a[]=$f;
      }
      return $a;
    }

    public function obtenerPedidosPorProductoByalmacen($desde, $hasta, $todo, $idu, $est, $ida){
      $ide = $_SESSION['ide'];
      if($est !=null)
        $est="AND F.tipo='$est'";
      if($idu!=null)
        $esu="ANF F.id_usuario=$idu";
      $almacen = '';
      if($ida!=null)
        $almacen = " AND F.id_almacen=$ida";

      if($todo == 1)
        $sql = "select D.ivap, D.pre_ven, D.tasa_ven, D.nom_pro, D.id_inventario, D.can_ven, sum(D.can_ven) as cantidad from detalle_factura D, facturas F where D.id_factura=F.id AND (F.fec_fac>='$desde 00:00:00' AND F.id_empresa=$ide AND F.fec_fac<='$hasta 23:59:59') AND F.estatus=1 $esu $est $almacen $usu GROUP BY D.id_inventario, D.tasa_ven, D.pre_ven;";
      else
        $sql = "select D.ivap, D.pre_ven, D.tasa_ven, D.nom_pro, D.id_inventario, D.can_ven, sum(D.can_ven) as cantidad, sum(D.pre_ven*D.can_ven)/D.tasa_ven monto, sum((D.pre_ven*D.can_ven)) monto_bs from detalle_factura D, facturas F where D.id_factura=F.id AND F.id_empresa=$ide AND (F.fec_fac>='$desde 00:00:00' AND F.fec_fac<='$hasta 23:59:59') AND F.estatus=1 $esu $est $almacen GROUP BY D.id_inventario;";

      $r= $this->orm->consultaPersonalizada($sql);
      $b =[];
      while($p = $r->fetch_assoc()){
        $b[] = $p;
      }
      return array("pedidos"=>$a, "detalles"=>$b);
    }

    public function edit($id){
      $sql = "UPDATE ".$this->tabla." SET ";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        $sql.= $key."='$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }

  }
?>
