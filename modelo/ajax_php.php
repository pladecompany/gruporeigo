<?php
    error_reporting(1);
    session_start();
    header('Content-type: application/json');
   // Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}
    

    include_once("Almacen.php");
    include_once("Pedido.php");
    include_once("Empresa.php");
    include_once("Iva.php");
    
    //$file = "config.json";
    //$json = json_decode(file_get_contents($file), true);
    $empr = new Empresa();
    $empresa = $empr->findById($_SESSION['ide']);
    $dolar = $empresa["dolar"];
    $pos = $empresa;

    $modulo = $_POST['modulo'];
    $tipo =  $_POST['tipo'];

    if($modulo == 'productos'){
      include_once("Inventario.php");
      

      if($tipo == "buscarProducto"){
        $cli = new Inventario();
        $iva = new Iva();
        $idc = strtoupper($_POST['idc']);
        $r = $cli->findById($idc);
        if($r!= false){
          $r['ivas']=$iva->getIvas();
          echo json_encode(array("r"=>true, "producto"=>$r, "dolar"=>$dolar));
        }else{
          echo json_encode(array("r"=>false));
        }

      }
    }else if($modulo == 'pedidos'){

      include_once("Inventario.php");
      

      if($tipo == "nuevoPedidoCatalogo"){
        $pedido = new Pedido();

        $idc = $_POST['idc'];
        if($idc=='0'){
          include_once("Cliente.php");
          $cliente = new Cliente();
          $cliente->data["id"] = "";
          $cliente->data["cod_cli"] = $_POST['cedc'];
          $cliente->data["nom_cli"] = $_POST['nomc'];
          $cliente->data["ape_cli"] = '';
          $cliente->data["cor_cli"] = '';
          $cliente->data["tel_cli"] = $_POST['tlfc'];
          $cliente->data["inst_cli"] = '';
          $cliente->data["dir_cli"] = '';
          $cliente->data["sex_cli"] = '';
          $cliente->data["pas_cli"] = "";
          $cliente->data["naci"] = $_POST['nacc'];
          $cliente->data["tipo"] = '1';
          $cliente->data["dias_credito"] = '0';
          $cliente->data["id_vendedor"] = null;
          $cliente->data["id_tipo_precio"] = null;

          $r = $cliente->save(); 
          $idc = $r->insert_id; 
        }
        $productos = $_POST['productos'];
        $pagos = $_POST['pagos'];
        //$idu = $_SESSION['idu'];
        $nota = $_POST['nota'];
        //$ped = $_POST['pedido'];
        $estatus=$_POST['estatus'];
        $tipo_fac=$_POST["tipo_fac"];
        $pos['id_almacen'] = $_POST['id_almacen'];

        $tipo_entrega = $_POST['tipo_entrega'];

        if($nota==null||$nota==""||strlen($nota)==0)
          $nota = "";

        // momentaneo
        if(!isset($_SESSION['ide'])){
          $_SESSION['ide']=1;
          $_SESSION['correo_send'] = 'serv.reigo@gmail.com';
        }

        $pedido->data["id"] = '';
        $pedido->data["fec_fac"] = date('Y-m-d H:i:s');

        $pedido->data["id_est"] = 1;
        $factura = date('YmdHis')."-0";
        $pedido->data["id_usuario"] = null;
        $pedido->data["id_cliente"] = $idc;
        $pedido->data["cod_fac"]=$factura;
        $pedido->data["metodo"]= '';
        $pedido->data["nota"] = $nota;
        $pedido->data["estatus"]=$estatus;
        $pedido->data["tipo"]=-100;
        $pedido->data["id_empresa"]=$_SESSION['ide'];
        $pedido->data["despacho"] = $_POST['despacho'];
        //$pedido->data["id_vendedor"] = $_POST['vendedor'];
        $pedido->data["id_vendedor"] = null;
        $pedido->data["id_almacen"]='';
        $pedido->data["tipo_almacen"]=$_POST['tipo_almacen'];
        $pedido->data["descuento"]='';
        $r = $pedido->save();

        if($r!=false){
          $idfactura = $r->insert_id;
          
          $pedido = new Pedido();

          $nd = $pedido->saveDetails($idfactura, $productos, $rs->insert_id, null, $pos);
          
          if($nd>0){
            $cli = new Cliente();
            $rc = $cli->findById($idc);

            $cuerpo = 'Pedido realizado por la web, información detallada del pedido:<br>Cod pedido: '.$factura.'<br>Cliente: '.$rc['naci'].'-'.$rc['cod_cli'].' / '.$rc['nom_cli'].' / '.$rc['tel_cli'].'<br><br>Pago <br><b>BANCO</b>: '.$_POST['met'].'<br><b>REFERENCIA</b>: '.$_POST['ref'].'<br>';
            if($tipo_entrega=='pickup')
              $cuerpo.= "<b>TIPO DE ENTREGA</b>: Lo recoge en la tienda. ";
            else
              $cuerpo.= "<b>TIPO DE ENTREGA</b>: Delivery (Dirección en la nota). ";

            $cuerpo.= "<br><b>NOTA</b>:<br>".nl2br($nota);
            $cuerpo.= '<br><br><hr><table><tr><th>PRODUCTO</th><th>CANTIDAD</th>';
            $inv = new Inventario();
            for($i = 0; $i < count($productos); $i++){
              $resul = $productos[$i];
              $ri = $inv->findById($resul['idp']);

              $cuerpo .= '<tr><td>#'.$ri['cod_inv'].' - '.$ri['nom_inv'].'</td><td style="text-align:center;">'.$resul['can'].'</td></tr>';
            }
            include_once("Orm.php");
            $cuerpo .= '</table><br>Total: '.$orm->monto($_POST['totalbs']).' Bs';
            echo json_encode(array("r"=>true, "pedido"=>$fpedido,"codfac"=>$factura));
            $orm->enviarCorreo($_SESSION['correo_send'], $cuerpo, 'Pedido desde la web'); 
          }else{
            //$salida->removeById($rs->insert_id);
            $pedido->removeById($idfactura);
            echo json_encode(array("r"=>false, "msj"=>"No se pudo procesar el pedido, no registro los detalles!"));
          }
        }else{
          echo json_encode(array("r"=>false, "msj"=>"No se pudo procesar el pedido."));
        }
      }
    }else if($modulo == 'clientes'){
      include_once("Cliente.php");
      $cli = new Cliente();
      if($tipo == "buscarCliente"){
        $idc = $_POST['idc'];
        $naci = $_POST['naci'];
        $r = $cli->findByCodNac($idc,$naci);
        if($r!= false){
          //$pedido = new Pedido();
          //$filas = $pedido->obtenerPedidosACreditoByClienteSoloCredito($r['id']);
          //echo json_encode(array("r"=>true, "cliente"=>$r, "pendientes"=>$filas));
          echo json_encode(array("r"=>true, "cliente"=>$r));
        }else
          echo json_encode(array("r"=>false, "cliente"=>null));
        exit(1);
      }else if($tipo == "nuevoCliente"){
        $cod = $_POST['ced'];
        $nom = $_POST['nom'];
        $ape = $_POST['ape'];
        $cor = $_POST['cor'];
        $tel = $_POST['tel'];
        $ins = $_POST['ins'];
        $dir = $_POST['dir'];
        $sex = $_POST['sex'];
        $nac = $_POST['naci'];
        $tip = $_POST['tip'];
        $dias = $_POST['dias'];
        $ven = $_POST['vendedor'];
        if($dias =="")
          $dias = "0";

        if($tip == 1)
          $dias = "0";

        if(empty($cod)||$cod=="")
          $err = "Cédula invalida";
        else if(empty($nom)||$nom == "")
          $err = "Nombre está vacío";

        if(isset($err)){
          echo json_encode(array("r"=>false, "msj"=>$err));
          exit(1);
        }

        $cliente = new Cliente();
        $cliente->data["id"] = "";
        $cliente->data["cod_cli"] = $cod;
        $cliente->data["nom_cli"] = $nom;
        $cliente->data["ape_cli"] = '';
        $cliente->data["cor_cli"] = $cor;
        $cliente->data["tel_cli"] = $tel;
        $cliente->data["inst_cli"] = $ins;
        $cliente->data["dir_cli"] = $dir;
        $cliente->data["sex_cli"] = $sex;
        $cliente->data["pas_cli"] = "";
        $cliente->data["naci"] = $nac;
        $cliente->data["tipo"] = $tip;
        $cliente->data["dias_credito"] = $dias;
        $cliente->data["id_vendedor"] = $ven;
        $cliente->data["id_tipo_precio"] = '';

        $r = $cliente->save();
        if($r->affected_rows == 1){
          $idi = $r->insert_id;
          $cli = new Cliente();
          $cliente = $cli->findById($idi);
          $err = "¡Registró correctamente!";
          echo json_encode(array("r"=>true, "msj"=>$err, "cliente"=>$cliente));
          exit(1);
        }else{
          $err = "¡Código/Correo ya existe!";
          echo json_encode(array("r"=>false, "msj"=>$err));
          exit(1);
        }
      }
    }
    
?>
