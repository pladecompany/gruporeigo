<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Almacen{
    private $tabla = "almacenes";
    public $data = [];
    public $orm = null;

    public function Almacen(){
      $this->orm = new Orm(new Conexion());
      $tihs->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function fetchAll(){
      $ide = $_SESSION['ide'];
      $sql = "SELECT * FROM ".$this->tabla." WHERE id_empresa='$ide' ORDER BY id;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function getStockAlmacen($id_almacen, $id_producto){
      $sql = "SELECT * FROM stock_almacen WHERE id_almacen=$id_almacen AND id_producto=$id_producto;";
      $r =  $this->orm->consultaPersonalizada($sql);
      if($r->num_rows == 0){
        $sql = "INSERT INTO stock_almacen VALUES(null, $id_producto, $id_almacen, 0, null);";
        $this->orm->insertarPersonalizado($sql);
        return 0;
      }else{
        $f =  $r->fetch_assoc();
        if($f['stock_almacen']==null || $f['stock_almacen']==0)
          $f['stock_almacen'] = '0';
        return $f['stock_almacen'];
      }
    }

    public function getProductosByAlmacen($ida){
      $sql = "SELECT * FROM inventario I, stock_almacen S WHERE S.id_producto=I.id AND S.id_almacen=$ida;";
      $r = $this->orm->consultaPersonalizada($sql);
    }

    public function restarStockAlmacen($id_almacen, $id_producto, $cantidad){
      $sql = "UPDATE stock_almacen SET stock_almacen=stock_almacen-$cantidad WHERE id_almacen=$id_almacen AND id_producto=$id_producto;";
      $r = $this->orm->editarPersonalizado($sql);
      if($r->affected_rows == 1)
        return true;
      else{
        return false;
      }
    }

    public function sumarStockAlmacen($id_almacen, $id_producto, $cantidad){
      $sql = "UPDATE stock_almacen SET stock_almacen=stock_almacen+$cantidad WHERE id_almacen=$id_almacen AND id_producto=$id_producto;";
      $r = $this->orm->editarPersonalizado($sql);
      if($r->affected_rows == 1)
        return true;
      else{
        return false;
      }
    }


    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }

  }
?>
